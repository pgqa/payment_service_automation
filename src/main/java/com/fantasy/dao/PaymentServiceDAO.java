package com.fantasy.dao;

import java.math.BigDecimal;
import java.util.Properties;

import com.rummycircle.servicesjson.paymentservice.InitRedisDataService;
import com.rummycircle.servicesjson.paymentservice.ProcessPayService;
import com.rummycircle.utils.database.DatabaseManager;
import com.rummycircle.utils.reporting.extent.Logger;
import com.rummycircle.utils.testutils.PropertyReader;

public class PaymentServiceDAO {

	private static Properties prop = PropertyReader.loadCustomProperties("sql.properties");
	private static Logger Log = Logger.getLogger();

	public InitRedisDataService getGatewayTransactionTable(String userID, long amount) {
		Log.info("Inside getGatewayTransactionTable");

		String query = String.format(prop.getProperty("get.gateway.transactions"), userID, amount);
		Object[][] db = DatabaseManager.executeQuery(query);
		InitRedisDataService ds = new InitRedisDataService();

		if (db.length > 0) {
			ds.setOrderId((String) db[0][0]);
			ds.setStatus((String) db[0][1]);
			BigDecimal amt = (BigDecimal) db[0][2];
			ds.setAmount(amt.doubleValue());
			Integer channelId = (Integer) db[0][3];
			ds.setChannelId(String.valueOf(channelId.intValue()));
			Log.info("InitRedisDataService dB : " + db);
		} 
		return ds;
	}

	public ProcessPayService getProcessPayData(String order_id) {
		String query = String.format(prop.getProperty("get.proesspay.data"), order_id);
		Object[][] db = DatabaseManager.executeQuery(query);

		ProcessPayService ps = new ProcessPayService();
		if (db.length > 0) {
			Long lng = (Long) db[0][0];
			ps.setUserId(lng.intValue());
			lng = (Long) db[0][1];
			ps.setTxnId(lng.intValue());
			BigDecimal amt = (BigDecimal) db[0][2];
			ps.setAmount(amt.intValue());
			ps.setAuthStatus((String) db[0][3]);
			Integer in = (Integer) db[0][4];
			ps.setChannelId(in.intValue());
			ps.setPaymentOption((String) db[0][5]);
			ps.setPaymentMode((String) db[0][6]);
			ps.setGateway((String) db[0][7]);
			ps.setOrderId((String) db[0][8]);
		} 
		return ps;
	}
}
