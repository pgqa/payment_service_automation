package com.fantasy.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import com.rummycircle.utils.database.AbstractDAO;
import com.rummycircle.utils.database.DatabaseManager;
import com.rummycircle.utils.testutils.PropertyReader;
import java.time.LocalDate;

public class GetPlayersLimitDao extends AbstractDAO{
	
	private static Properties prop = PropertyReader
			.loadCustomProperties("sql.properties");
	private static final int index = 0;

	
	public static String getAmountDepositedToday(String userId) {
		String query = prop.getProperty("get.deposited.amount");
		query=String.format(query, userId,getDate(1),getDate(1));
		return (String) DatabaseManager.executeQuery(query)[index][index];
	}
	
	public static String getAmountWithdrawnToday(String userId) {
		String query = prop.getProperty("get.withwrawn.balance");
		query=String.format(query, userId,getDate(1),getDate(1));
		return (String) DatabaseManager.executeQuery(query)[index][index];
	}
	
	public static String getAmountWithdrawnRequestedToday(String userId) {
		String query = prop.getProperty("get.withdraw.requested");
		query=String.format(query, userId,getDate(1),getDate(1));
		return (String) DatabaseManager.executeQuery(query)[index][index];
	}
	
	public static String getAmountWithdrawnPickUpToday(String userId) {
		String query = prop.getProperty("get.withdraw.pickedup");
		query=String.format(query, userId,getDate(1),getDate(1));
		return (String) DatabaseManager.executeQuery(query)[index][index];
	}
	
	public static String getAmountWithdrawnMonthly(String userId) {
		String query = prop.getProperty("get.withwrawn.balance.monthly");
		query=String.format(query, userId,userId,getFirstDayOfMonth(1),getDate(1));
		return (String) DatabaseManager.executeQuery(query)[index][index];
	}
	
	public static String getAmountWithdrawnRequestedMonthly(String userId) {
		String query = prop.getProperty("get.withdraw.requested.monthly");
		query=String.format(query, userId,userId,getFirstDayOfMonth(1),getDate(1));
		return (String) DatabaseManager.executeQuery(query)[index][index];
	}
	
	public static String getAmountWithdrawnPickUpMonthly(String userId) {
		String query = prop.getProperty("get.withdraw.pickedup.monthly");
		query=String.format(query, userId,getFirstDayOfMonth(1),getDate(1));
		return (String) DatabaseManager.executeQuery(query)[index][index];
	}
	
	/*
	 * 0.kyc_status
	 * 1.current_limit
	 * 2.mobileNo
	 * 3.isMobileCheck
	 * 4. firstDepositor
	 * 5. accountStatus
	 * 6. maxLimit
	 * 7. monthlyAddcashLimit
	 * 8. dailyAddcashLimit
	 */
	public static Object [][] getPlayersInfo(String userId){
		String query = prop.getProperty("get.players.account.info");
		query=String.format(query, userId);
		return DatabaseManager.executeQuery(query);

	}
	public static String getDate(int days) {
		DateFormat todayFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar todaysDate = Calendar.getInstance();
		todaysDate.setTime(new Date());
		todaysDate.add(Calendar.DATE, days);
		return todayFormat.format(todaysDate.getTime());
	}
	
	public static String getFirstDayOfMonth(int days) {
		LocalDate todaydate = LocalDate.now();
		return todaydate.withDayOfMonth(1).toString();
	}

	public boolean isDailyAddCashLimitHit(String amountDepositedToday, String amountWithdrawnToday,
			String amountWithdrawnRequested, String amountWithdrawnPickUp, String dailyAddCadhLimit) {
		int mlr = Integer.parseInt(dailyAddCadhLimit)
				- (Integer.parseInt(amountDepositedToday) - (Integer.parseInt(amountWithdrawnToday)
						+ Integer.parseInt(amountWithdrawnRequested) + Integer.parseInt(amountWithdrawnPickUp)));
		if (mlr == 0)
			return true;
		else
			return false;
	}
	
	
	public static void main(String args[]) {
		//System.out.println(getAmountDepositedToday("272023"));
		//System.out.println(getAmountWithdrawnToday("272023"));
		//System.out.println(getAmountWithdrawnRequestedToday("272023"));
		//System.out.println(getAmountWithdrawnPickUpToday("272023"));
		System.out.println(getAmountWithdrawnPickUpMonthly("272023"));
		
	}
	
}
