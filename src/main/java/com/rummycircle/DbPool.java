package com.rummycircle;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

/*
 * @author Ankit Kumar
 */

public class DbPool {

	private static String JDBC_USER, JDBC_PASS, JDBC_DB_URL, JDBC_DRIVER;
	private static Connection connObj;
	private static DbPool jdbcObj;
	private static DataSource dataSource;
	private GenericObjectPool gPool = null;

	// load config details from .properties file
	static {
		Properties prop = new Properties();
		String envFileName = "config.properties";
		String envFileLoc = System.getProperty("user.dir")
				+ "/src/test/resources/" + envFileName;
		try {
			prop.load(new FileInputStream(envFileLoc));
		} catch (IOException e) {
			e.printStackTrace();
		}
		JDBC_DRIVER = prop.getProperty("jdbc.drivers");
		JDBC_DB_URL = prop.getProperty("jdbc.url");
		JDBC_USER = prop.getProperty("jdbc.username");
		JDBC_PASS = prop.getProperty("jdbc.password");
	}

	public DbPool() {

	}

	public static DbPool getInstance() {
		try {
			jdbcObj = new DbPool();
			dataSource = jdbcObj.setUpPool();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jdbcObj;
	}

	@SuppressWarnings("unused")
	public DataSource setUpPool() throws Exception {
		Class.forName(JDBC_DRIVER);

		// Creates an Instance of GenericObjectPool That Holds Our Pool of
		// Connections Object!
		gPool = new GenericObjectPool();
		gPool.setMaxActive(5);

		// Creates a ConnectionFactory Object Which Will Be Use by the Pool to
		// Create the Connection Object!
		ConnectionFactory cf = new DriverManagerConnectionFactory(JDBC_DB_URL,
				JDBC_USER, JDBC_PASS);

		// Creates a PoolableConnectionFactory That Will Wraps the Connection
		// Object Created by the ConnectionFactory to Add Object Pooling
		// Functionality!
		PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf,
				gPool, null, null, false, true);
		return new PoolingDataSource(gPool);
	}

	public GenericObjectPool getConnectionPool() {
		return gPool;
	}

	public int executeUpdate(String query) throws SQLException {

		try {
			// Making A New Connection Object For Db Transaction
			connObj = dataSource.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		int count = 0;
		PreparedStatement pstmtObj = null;
		try {
			pstmtObj = connObj.prepareStatement(query);
			count = pstmtObj.executeUpdate(query);
			// Releasing Connection Object To Pool
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				// Closing PreparedStatement Object
				if (pstmtObj != null) {
					pstmtObj.close();
				}
				// Closing Connection Object
				if (connObj != null) {
					connObj.close();
				}
			} catch (Exception sqlException) {
				sqlException.printStackTrace();
			}
		}

		return count;
	}

	public int executeQueryInt(String query) throws SQLException {

		try {
			// Making A New Connection Object For Db Transaction
			connObj = dataSource.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		int value = 0;
		ResultSet rsObj = null;
		PreparedStatement pstmtObj = null;
		try {
			pstmtObj = connObj.prepareStatement(query);
			rsObj = pstmtObj.executeQuery();
			while (rsObj.next())
				value = rsObj.getInt(1);
			// Releasing Connection Object To Pool
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				// Closing ResultSet Object
				if (rsObj != null) {
					rsObj.close();
				}
				// Closing PreparedStatement Object
				if (pstmtObj != null) {
					pstmtObj.close();
				}
				// Closing Connection Object
				if (connObj != null) {
					connObj.close();
				}
			} catch (Exception sqlException) {
				sqlException.printStackTrace();
			}
		}

		return value;
	}

	public long executeQueryLong(String query) throws SQLException {

		try {
			// Making A New Connection Object For Db Transaction
			connObj = dataSource.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		long value = 0l;
		ResultSet rsObj = null;
		PreparedStatement pstmtObj = null;
		try {
			pstmtObj = connObj.prepareStatement(query);
			rsObj = pstmtObj.executeQuery();
			while (rsObj.next()) {
				value = rsObj.getLong(1);
			}
			// Releasing Connection Object To Pool
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				// Closing ResultSet Object
				if (rsObj != null) {
					rsObj.close();
				}
				// Closing PreparedStatement Object
				if (pstmtObj != null) {
					pstmtObj.close();
				}
				// Closing Connection Object
				if (connObj != null) {
					connObj.close();
				}
			} catch (Exception sqlException) {
				sqlException.printStackTrace();
			}
		}

		return value;
	}

	public String executeQueryString(String query) throws SQLException {

		try {
			// Making A New Connection Object For Db Transaction
			connObj = dataSource.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		String resultString = new String();
		ResultSet rsObj = null;
		PreparedStatement pstmtObj = null;
		try {
			pstmtObj = connObj.prepareStatement(query);
			rsObj = pstmtObj.executeQuery();
			while (rsObj.next()) {
				resultString = rsObj.getString(1);
			}
			// Releasing Connection Object To Pool
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				// Closing ResultSet Object
				if (rsObj != null) {
					rsObj.close();
				}
				// Closing PreparedStatement Object
				if (pstmtObj != null) {
					pstmtObj.close();
				}
				// Closing Connection Object
				if (connObj != null) {
					connObj.close();
				}
			} catch (Exception sqlException) {
				sqlException.printStackTrace();
			}
		}

		// System.out.println(resultString);
		return resultString;
	}

	public double executeQueryDouble(String query) throws SQLException {

		try {
			// Making A New Connection Object For Db Transaction
			connObj = dataSource.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		double value = 0;
		ResultSet rsObj = null;
		PreparedStatement pstmtObj = null;
		try {
			pstmtObj = connObj.prepareStatement(query);
			rsObj = pstmtObj.executeQuery();
			while (rsObj.next()) {
				value = rsObj.getDouble(1);
			}
			// Releasing Connection Object To Pool
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				// Closing ResultSet Object
				if (rsObj != null) {
					rsObj.close();
				}
				// Closing PreparedStatement Object
				if (pstmtObj != null) {
					pstmtObj.close();
				}
				// Closing Connection Object
				if (connObj != null) {
					connObj.close();
				}
			} catch (Exception sqlException) {
				sqlException.printStackTrace();
			}
		}
		return value;
	}

	public List<String> selectRecordsFromDbUserTable(String query)
			throws SQLException {
		Connection connObj = null;

		try {
			// System.out.println("\n=====Making A New Connection Object For Db Transaction=====\n");
			connObj = dataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<String> L = new ArrayList<String>();
		ResultSet rsObj = null;
		PreparedStatement pstmtObj = null;
		try {
			pstmtObj = connObj.prepareStatement(query);
			rsObj = pstmtObj.executeQuery();
			while (rsObj.next())
				L.add(rsObj.getString("loginid"));
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				// Closing ResultSet Object
				if (rsObj != null) {
					rsObj.close();
				}
				// Closing PreparedStatement Object
				if (pstmtObj != null) {
					pstmtObj.close();
				}
				// Closing Connection Object
				if (connObj != null) {
					connObj.close();
				}
			} catch (Exception sqlException) {
				sqlException.printStackTrace();
			}
		}
		return L;
	}

	public List<Long> executeQueryList(String query) throws SQLException {

		try {
			// Making A New Connection Object For Db Transaction
			connObj = dataSource.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		Long value = 0l;
		ResultSet rsObj = null;
		PreparedStatement pstmtObj = null;
		List<Long> list = new ArrayList<Long>();
		try {
			pstmtObj = connObj.prepareStatement(query);
			rsObj = pstmtObj.executeQuery();
			while (rsObj.next()) {
				value = rsObj.getLong(1);
				list.add(value);
			}
			// Releasing Connection Object To Pool
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				// Closing ResultSet Object
				if (rsObj != null) {
					rsObj.close();
				}
				// Closing PreparedStatement Object
				if (pstmtObj != null) {
					pstmtObj.close();
				}
				// Closing Connection Object
				if (connObj != null) {
					connObj.close();
				}
			} catch (Exception sqlException) {
				sqlException.printStackTrace();
			}
		}
		return list;
	}

	public Object[][] executeQueryObject2DArray(String query) {
		try {
			// Making A New Connection Object For Db Transaction
			connObj = dataSource.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		ResultSet result = null;
		ResultSetMetaData rsMetaData = null;
		Object[][] finalResult = (Object[][]) null;

		try {
			Statement stat = connObj.createStatement();
			System.out.println("My query: " + query);
			result = stat.executeQuery(query);
			rsMetaData = result.getMetaData();
			int e = rsMetaData.getColumnCount();
			ArrayList<Object[]> data = new ArrayList<Object[]>();
			Object[] header = new Object[e];

			int resultLength;
			for (resultLength = 1; resultLength <= e; ++resultLength) {
				String i = rsMetaData.getColumnLabel(resultLength);
				header[resultLength - 1] = i;
			}

			int arg12;
			while (result.next()) {
				Object[] arg11 = new Object[e];

				for (arg12 = 1; arg12 <= e; ++arg12) {
					Object row = result.getObject(arg12);
					arg11[arg12 - 1] = row;
				}

				data.add(arg11);
			}

			resultLength = data.size();
			finalResult = new Object[resultLength][e];

			for (arg12 = 0; arg12 < resultLength; ++arg12) {
				Object[] arg13 = (Object[]) data.get(arg12);
				finalResult[arg12] = arg13;
			}
		} catch (SQLException arg10) {
			arg10.printStackTrace();
		}

		return finalResult;
	}

	public boolean executeQueryBoolean(String query) throws SQLException {

		try {
			// Making A New Connection Object For Db Transaction
			connObj = dataSource.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		boolean value = false;
		ResultSet rsObj = null;
		PreparedStatement pstmtObj = null;
		try {
			pstmtObj = connObj.prepareStatement(query);
			rsObj = pstmtObj.executeQuery();
			while (rsObj.next())
				value = rsObj.getBoolean(1);
			// Releasing Connection Object To Pool
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				// Closing ResultSet Object
				if (rsObj != null) {
					rsObj.close();
				}
				// Closing PreparedStatement Object
				if (pstmtObj != null) {
					pstmtObj.close();
				}
				// Closing Connection Object
				if (connObj != null) {
					connObj.close();
				}
			} catch (Exception sqlException) {
				sqlException.printStackTrace();
			}
		}

		return value;
	}

	public Map<Object, Object> executeQueryAllFieldAndValue(String query)
			throws SQLException {

		try {
			// Making A New Connection Object For Db Transaction
			connObj = dataSource.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		Object value = null;

		Map<Object, Object> map = new HashMap<Object, Object>();
		ResultSet rsObj = null;
		PreparedStatement pstmtObj = null;
		try {
			pstmtObj = connObj.prepareStatement(query);
			rsObj = pstmtObj.executeQuery();
			ResultSetMetaData rsmd = rsObj.getMetaData();
			int metaDataSize = rsmd.getColumnCount();
			if (rsObj.next()) {
				for (int i = 1; i <= metaDataSize; i++) {
					value = rsObj.getObject(i);
					map.put(rsmd.getColumnName(i), value);
				}
				value = null;
			}
			// Releasing Connection Object To Pool
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				// Closing ResultSet Object
				if (rsObj != null) {
					rsObj.close();
				}
				// Closing PreparedStatement Object
				if (pstmtObj != null) {
					pstmtObj.close();
				}
				// Closing Connection Object
				if (connObj != null) {
					connObj.close();
				}
			} catch (Exception sqlException) {
				sqlException.printStackTrace();
			}
		}

		return map;
	}
}