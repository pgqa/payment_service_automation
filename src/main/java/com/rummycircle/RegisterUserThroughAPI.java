package com.rummycircle;

import java.security.Key;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.testng.Assert;
import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.rummycircle.restclient.HTTPHeaders;
import com.rummycircle.restclient.HTTPMethod;
import com.rummycircle.restclient.HTTPParams;
import com.rummycircle.restclient.HTTPRequest;
import com.rummycircle.restclient.HTTPResponse;
import com.rummycircle.restclient.RESTAssuredClient;
import com.rummycircle.restclient.ResponseValidator;
import com.rummycircle.utils.database.DatabaseManager;
import com.rummycircle.utils.exceptions.RCException;
import com.rummycircle.utils.testutils.BaseTest;
import com.rummycircle.utils.testutils.PropertyReader;
import com.rummycircle.servicesjson.paymentservice.*;

public class RegisterUserThroughAPI extends BaseTest {

	Logger log = Logger.getLogger(RegisterUserThroughAPI.class);
	String userName = null;
	String passWord = "tester";
	String email = null;
	String gender = "Male";
	String mobile = null;
	String state = "Maharashtra";
	Properties customProp = PropertyReader
			.loadCustomProperties("custom.properties");
	String dob = null;
	String userId = null;
	private static Cipher c;
	private static final String CIPHER_TP_INSTANCE = "AES/CBC/PKCS5Padding";
	private static final String ALGO = "AES";
	private static final byte[] KEYVALUE = new byte[] { 'A', 'b', 'c', 'd',
			'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p' };
	private static Key key = generateKey();
	private Gson gson = new Gson();
	private static String addCashTokenParam = null;

	private static Key generateKey() {
		/*
		 * ClassLoader classLoader = getClass().getClassLoader();
		 * InputStreamReader ip = new
		 * InputStreamReader(classLoader.getResourceAsStream("keyfile.bin"));
		 * byte[] bFile = IOUtils.toByteArray(ip);
		 */
		Key key = new SecretKeySpec(KEYVALUE, ALGO);
		return key;
	}

	public List<String> registerUser() {

		initTestMethod();
		String registerPath = customProp.getProperty("register.protocol")
				+ "://" + customProp.getProperty("registerBAF.host") + ":"
				+ customProp.getProperty("registerBAF.port")
				+ "/registrationservice/reg/service/signup/register/";

		userName = getRandomString(9);
		email = getUniqueEmailId(userName, 8);
		List<String> userDetails = new ArrayList<String>(3);
		HTTPParams params = new HTTPParams();
		params.addParam("loginid", userName);
		params.addParam("email", email);
		params.addParam("gender", "F");
		params.addParam("password", "tester");
		params.addParam("state", "Karnataka");
		params.addParam("fromPage", "index.htm");
		params.addParam("channel_id", "0");
		HTTPRequest httpReq = new HTTPRequest(params);
		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.GET, registerPath,
				httpReq);
		Assert.assertEquals(httpResp.getStatusCode(), 200);
		Assert.assertNotNull(httpResp);
		boolean regStatus = JsonPath.read(httpResp.getBody().getBodyText(),
				"$.register");
		Assert.assertTrue(regStatus, "Registration is not success");
		userId = JsonPath.read(httpResp.getBody().getBodyText(), "$.userId");
		userDetails.add(userName);
		userDetails.add(passWord);
		userDetails.add(userId);
		return userDetails;
	}

	public enum RegistrationDetails {
		USERNAME, PASSWORD, EMAILID, USERID

	};

	public HashMap<RegistrationDetails, String> registerUserUsingPostApi(
			RESTAssuredClient rc) {
		Properties cuLocators = PropertyReader
				.loadCustomProperties("custom.properties");
		String userName = "user_".concat(getRandomString(6));
		String password = "tester@123";
		String email = getUniqueEmailId(userName, 5);

		String requestStr = "{\"loginid\":\""
				+ userName
				+ "\",\"email\":\""
				+ email
				+ "\",\"password\":\""
				+ password
				+ "\",\"nev\":false,\"channel_id\":1,\"birthYear\":\"1998\",\"gender\":\"M\",\"fromPage\":\"index.html\",\"utmParams\":{\"utm_medium\":\"FFITY\",\"utm_source\":\"0LL35\",\"gclid\":\"bgLix\"},\"register\":false,\"errorCode\":0}";
		String path = cuLocators.getProperty("register.postreq.protocol")
				.concat("://")
				+ cuLocators.getProperty("register.postreq.host")
				+ ":"
				+ cuLocators.getProperty("register.postreq.port")
				+ "/registrationservice/reg/service/signup/users";
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json");
		HTTPRequest request = new HTTPRequest(headers, requestStr);

		RESTAssuredClient rc1 = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc1.sendRequest(HTTPMethod.POST, path, request);

		Assert.assertTrue(response.getStatusCode() == 200,
				"status Code expected is 200. However Status Code received:"
						+ response.getStatusCode());
		String userId = JsonPath.read(response.getBody().getBodyText(),
				"$.userId");
		HashMap<RegistrationDetails, String> registerData = new HashMap<RegistrationDetails, String>();
		registerData.put(RegistrationDetails.USERNAME, userName);
		registerData.put(RegistrationDetails.PASSWORD, password);
		registerData.put(RegistrationDetails.USERID, userId);
		registerData.put(RegistrationDetails.EMAILID, email);

		return registerData;
	}

	public static String generateAddCashToken() {
		long seed = System.currentTimeMillis();
		Random r = new Random();
		r.setSeed(seed);
		return Long.toString(seed) + Long.toString(Math.abs(r.nextLong()));
	}

	public void doAddCash(String user_id, String loginId, int amount,
			int channelId) {
		String query = String
				.format("select if(count(user_id)=0,TRUE,FALSE) as firstDepositor from gateway_transactions where user_id=%s and status='Authorised';",
						user_id);
		boolean firstDepositor = Boolean.getBoolean(DatabaseManager
				.executeQuery(query)[0][0].toString());
		String addCashToken = generateAddCashToken();
		RedisUtility redisUtil = RedisUtility.getInstance(getRandomString(6));
		redisUtil.pushToRedis(addCashToken, addCashToken);
		System.out.println(customProp.get("ps.schema"));
		String queryInit = String
				.format("select d.merchant_name,g.name,m.name,o.name,d.processor_code from %spayment_configuration_details d,%spayment_mode_option_mapping mo, %spayment_mode m, %spayment_option o, %spayment_gateway g where d.pay_type_opt_map_id=mo.id and mo.payment_mode_id=m.id and mo.payment_option_id=o.id and d.gateway_id=g.id and d.active=1 and g.name in('BILLDESK') order by d.id desc limit 1;",
						customProp.get("ps.schema"),
						customProp.get("ps.schema"),
						customProp.get("ps.schema"),
						customProp.get("ps.schema"),
						customProp.get("ps.schema"));
		System.out.println(queryInit);
		Object[][] data = DatabaseManager.executeQuery(queryInit);
		String orderId = initPay(user_id, loginId, amount,
				data[0][0].toString(), data[0][3].toString(),
				data[0][2].toString(), data[0][4].toString(),
				data[0][1].toString(), null, channelId, "0", firstDepositor,
				addCashToken);
		log.info("Order Id: " + orderId);
		String errorMessage = customProp.getProperty("user_cancellation_msg");
		String msg = String.format(
				customProp.getProperty("processpay.billdesk.msg"), orderId,
				amount, customProp.get("processpay.billdesk.status_code"),
				userId, errorMessage).trim();

		try {
			PostAddCashPSPRequest req = new PostAddCashPSPRequest();
			req.setAmount(amount);
			req.setOrderId(orderId);
			req.setUserId(Integer.parseInt(user_id));
			req.setStatus("1");
			req.setType_id(5);

			PostAddCashPSPRequestList request = new PostAddCashPSPRequestList();
			request.setPayload(new PostAddCashPSPRequest[] { req });

			HTTPResponse res = sendPostAddCashPspReqAndGetResp(request);
			Assert.assertTrue(res.getStatusCode() == 200);
			log.info("Response Body : " + res.getBody().getBodyText());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private HTTPResponse sendPostAddCashPspReqAndGetResp(
			PostAddCashPSPRequestList inputParam) {
		HTTPResponse res = null;
		String path = customProp.getProperty("addcash.protocol") + "://"
				+ customProp.getProperty("addcash.ip") + ":"
				+ customProp.getProperty("addcash.port")
				+ customProp.getProperty("addcash.url");

		HTTPHeaders headers = setPSPPaymentServiceHeader();
		HTTPRequest req = new HTTPRequest(headers, inputParam);
		log.info("Request Params : "
				+ serializeJavaObjectToJson(inputParam,
						PostAddCashPSPRequestList.class));
		res = rc.sendRequest(HTTPMethod.POST, path, req);

		return res;
	}

	public HTTPHeaders setPSPPaymentServiceHeader() {
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type",
				customProp.getProperty("ps.header.contentType"));
		headers.addHeader("apikey",
				customProp.getProperty("ps.header.psp.apikey"));
		headers.addHeader("role", customProp.getProperty("ps.header.psp.role"));
		headers.addHeader("merchantName",
				customProp.getProperty("ps.header.merchant.name"));
		headers.addHeader("token", generatePSPToken());
		return headers;
	}

	public String generatePSPToken() {
		final long nowSec = System.currentTimeMillis() / 1000l; // SECONDS_IN_MILLISECOND;
		long startTime = nowSec - 30;
		String apiKey = customProp.getProperty("ps.header.psp.apikey");
		String secretKey = customProp.getProperty("ps.header.psp.secretKey");
		final String toHash = apiKey + secretKey + startTime;
		final String sha1 = DigestUtils.sha256Hex(toHash);
		log.info("GENERATED SHA 1 = " + sha1);
		return sha1;
	}

	public HTTPHeaders setFusionServicePaymentServiceHeader() {
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type",
				customProp.getProperty("ps.header.contentType"));
		headers.addHeader("apikey",
				customProp.getProperty("ps.header.fusion_service.apikey"));
		headers.addHeader("role",
				customProp.getProperty("ps.header.fusion_service.role"));
		headers.addHeader("merchantName",
				customProp.getProperty("ps.header.merchant.name"));
		headers.addHeader("token", generateFusionServiceToken());
		return headers;
	}

	public String generateFusionServiceToken() {
		final long nowSec = System.currentTimeMillis() / 1000l; // SECONDS_IN_MILLISECOND;
		long startTime = nowSec - 30;
		String apiKey = customProp
				.getProperty("ps.header.fusion_service.apikey");
		String secretKey = customProp
				.getProperty("ps.header.fusion_service.secretKey");
		final String toHash = apiKey + secretKey + startTime;
		final String sha1 = DigestUtils.sha256Hex(toHash);
		log.info("GENERATED SHA 1 = " + sha1);
		return sha1;
	}

	public String initPay(String userId, String loginId, int amount,
			String merchantName, String paymentOption, String paymentType,
			String bankCode, String gatewayName, String promotionalCode,
			int channelId, String debitAmount, boolean firstDepositor,
			String addCashToken) {
		String msg = customProp.getProperty("initpay.data");
		String initMsg = String.format(msg, userId, loginId, amount,
				merchantName, paymentOption, paymentType, bankCode,
				gatewayName, promotionalCode, channelId, debitAmount);
		String orderId = null;
		StringBuilder data = null;
		try {
			log.info("Input Request: " + initMsg);
			String encryptedData = encrypt(initMsg);
			String dataToSend = "{\"data\":\""
					+ encryptedData
					+ "\",\"rewardStoreItemId\":\"100\",\"bonusAmount\":\"100\",\"firstDepositor\":"
					+ firstDepositor + "}";
			data = new StringBuilder(dataToSend);
			if (channelId == 2 || channelId == 3) {
				data.replace(dataToSend.length() - 1, dataToSend.length(),
						",\"" + addCashTokenParam + "\":\"" + addCashToken
								+ "\"}");
			}
			HTTPResponse resp = initPayment(data.toString());

			orderId = JsonPath.read(resp.getBody().getBodyText(), "$.orderId");
		} catch (Exception e) {
			log.error("Error message: " + e.getMessage());
		}
		return orderId;
	}

	public HTTPHeaders setFusionProxyPaymentServiceHeader() {
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type",
				customProp.getProperty("ps.header.contentType"));
		headers.addHeader("apikey",
				customProp.getProperty("ps.header.fusion_proxy.apikey"));
		headers.addHeader("role",
				customProp.getProperty("ps.header.fusion_proxy.role"));
		headers.addHeader("merchantName",
				customProp.getProperty("ps.header.merchant.name"));
		headers.addHeader("token", generateFusionProxyToken());
		return headers;
	}

	public String generateFusionProxyToken() {
		final long nowSec = System.currentTimeMillis() / 1000l; // SECONDS_IN_MILLISECOND;
		long startTime = nowSec - 30;
		String apiKey = customProp.getProperty("ps.header.fusion_proxy.apikey");
		String secretKey = customProp
				.getProperty("ps.header.fusion_proxy.secretKey");
		final String toHash = apiKey + secretKey + startTime;
		final String sha1 = DigestUtils.sha256Hex(toHash);
		log.info("GENERATED SHA 1 = " + sha1);
		return sha1;
	}

	private HTTPResponse initPayment(String initReq) {
		String path = customProp.getProperty("init.protocol") + "://"
				+ customProp.getProperty("init.ip") + ":"
				+ customProp.getProperty("init.port")
				+ customProp.getProperty("init.url");
		HTTPHeaders headers = setFusionProxyPaymentServiceHeader();
		log.info(" Input Param formed " + initReq);
		HTTPRequest req = new HTTPRequest(headers, initReq);
		com.rummycircle.restclient.RESTAssuredClient rc = com.rummycircle.restclient.RESTAssuredClient
				.getRESTAssuredClient();
		HTTPResponse resp = rc.sendRequest(HTTPMethod.POST, path, req);

		log.info("Status Code of Response: " + resp.getStatusCode());
		log.info("Reason Phrase of Response: " + resp.getReasonPhrase());
		if (resp.getStatusCode() != 200)
			throw new RCException("Status Code id not 200.");
		boolean check = ResponseValidator.isResponseEmpty(resp);
		if (check) {
			throw new RCException("Response is EMPTY!!");
		}
		return resp;
	}

	public static String encrypt(String data) throws Exception {

		c = Cipher.getInstance(ALGO);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(data.getBytes());
		String encryptedValue = new String(Base64.getEncoder().encode(encVal));
		return encryptedValue;

	}
	
	public HashMap<RegistrationDetails, String> registerUserUsingPostApi(String... password) {
		Properties cuLocators = PropertyReader.loadCustomProperties("custom.properties");
		String userName = "user_".concat(getRandomString(6));
		String passWord = password.length > 0 ? password[0] : cuLocators.getProperty("automation.passw");
		String email = getUniqueEmailId(userName, 5);
	
		String requestStr = "{\"loginid\":\"" + userName + "\",\"email\":\"" + email + "\",\"password\":\"" + passWord
				+ "\",\"nev\":false,\"channel_id\":1,\"birthYear\":\"1998\",\"gender\":\"M\",\"fromPage\":\"index.html\",\"utmParams\":{\"utm_medium\":\"FFITY\",\"utm_source\":\"0LL35\",\"gclid\":\"bgLix\"},\"register\":false,\"errorCode\":0}";
		String path = cuLocators.getProperty("register.postreq.protocol").concat("://")
				+ cuLocators.getProperty("register.postreq.host") + ":"
				+ cuLocators.getProperty("register.postreq.port") + "/registrationservice/reg/service/signup/users";
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json");
		HTTPRequest request = new HTTPRequest(headers, requestStr);

		RESTAssuredClient rc1=RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc1.sendRequest(HTTPMethod.POST, path, request);

		Assert.assertTrue(response.getStatusCode() == 200,
				"status Code expected is 200. However Status Code received:" + response.getStatusCode());
		String userId = JsonPath.read(response.getBody().getBodyText(), "$.userId");
		HashMap<RegistrationDetails, String> registerData = new HashMap<RegistrationDetails, String>();
		registerData.put(RegistrationDetails.USERNAME, userName);
		registerData.put(RegistrationDetails.PASSWORD, passWord);
		registerData.put(RegistrationDetails.USERID, userId);
		registerData.put(RegistrationDetails.EMAILID, email);
		
		return registerData;
	}
}