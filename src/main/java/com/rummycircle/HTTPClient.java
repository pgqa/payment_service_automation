package com.rummycircle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ProtocolException;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.RedirectStrategy;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class HTTPClient {

	private CloseableHttpClient client;
	private static Logger LOGGER = Logger.getLogger(HTTPClient.class);

	public HTTPClient() {
		client = HttpClientBuilder.create().setRedirectStrategy(getRedirectStrategy())
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
	}

	public HTTPClient(final String userName, final String password) {
		client = HttpClientBuilder.create().setRedirectStrategy(getRedirectStrategy())
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
				.setDefaultCredentialsProvider(getCredentials(userName, password)).build();
	}

	public void close() {
		try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private CredentialsProvider getCredentials(final String userName, final String password) {
		CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(userName, password));
		return credentialsProvider;
	}

	private static RedirectStrategy getRedirectStrategy() {
		return new DefaultRedirectStrategy() {
			public boolean isRedirected(HttpRequest request, HttpResponse response, HttpContext context) {
				boolean isRedirect = false;
				try {
					isRedirect = super.isRedirected(request, response, context);
				} catch (ProtocolException e) {
					e.printStackTrace();
				}
				if (!isRedirect) {
					int responseCode = response.getStatusLine().getStatusCode() / 100;
					if (responseCode == 3) {
						return true;
					}
				}
				return isRedirect;
			}
		};
	}

	public HttpResponse get(final String url) throws ClientProtocolException, IOException {
		return get(url, null);
	}

	public HttpResponse get(final String url, final String authCredentials)
			throws ClientProtocolException, IOException {
		if (url == null || url.trim().isEmpty()) {
			LOGGER.error("URL cannot be null or empty");
			return null;
		}
		LOGGER.info("URL -> " + url);
		HttpGet get = new HttpGet(url.trim());
		if (!(authCredentials == null || authCredentials.trim().isEmpty())) {
			get.addHeader("Authorization", "Basic " + Base64.encodeBase64String(authCredentials.trim().getBytes()));
		}
		return getResponse(url, execute(get));
	}

	/**
	 * 
	 * @param url
	 * @param jsonBody
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public HttpResponse put(final String url, final String jsonBody) throws ClientProtocolException, IOException {
		return put(url, jsonBody, null);
	}

	/**
	 * 
	 * @param url
	 * @param jsonBody
	 * @param authCredentials
	 *            in format "USERNAME:PASSWORD"
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public HttpResponse put(final String url, final String jsonBody, final String authCredentials)
			throws ClientProtocolException, IOException {
		if (url == null || url.trim().isEmpty()) {
			LOGGER.error("URL cannot be null or empty");
			return null;
		}
		LOGGER.info("URL -> " + url);
		HttpPut put = new HttpPut(url.trim());
		put.addHeader("Content-Type", "application/json");
		put.addHeader("Accept", "application/json");
		if (!(authCredentials == null || authCredentials.trim().isEmpty())) {
			put.addHeader("Authorization", "Basic " + Base64.encodeBase64String(authCredentials.trim().getBytes()));
		}
		put.setEntity(new StringEntity(jsonBody, ContentType.APPLICATION_JSON));
		return getResponse(url, execute(put));
	}

	/**
	 * 
	 * @param url
	 * @param jsonBody
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public HttpResponse post(final String url, final String jsonBody) throws ClientProtocolException, IOException {
		return post(url, jsonBody, null);
	}

	/**
	 * 
	 * @param url
	 * @param jsonBody
	 * @param authCredentials
	 *            in format "USERNAME:PASSWORD"
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public HttpResponse post(final String url, final String jsonBody, final String authCredentials)
			throws ClientProtocolException, IOException {
		if (url == null || url.trim().isEmpty()) {
			LOGGER.error("URL cannot be null or empty");
			return null;
		}
		LOGGER.info("URL -> " + url);
		HttpPost post = new HttpPost(url.trim());
		post.addHeader("Content-Type", "application/json");
		post.addHeader("Accept", "application/json");
		if (!(authCredentials == null || authCredentials.trim().isEmpty())) {
			post.addHeader("Authorization", "Basic " + Base64.encodeBase64String(authCredentials.trim().getBytes()));
		}
		post.setEntity(new StringEntity(jsonBody, ContentType.APPLICATION_JSON));
		return getResponse(url, execute(post));
	}
	
	public HttpResponse post(final String url, final String jsonBody, final List<NameValuePair> headers, 
			final String authCredentials) throws ClientProtocolException, IOException {
		if (url == null || url.trim().isEmpty()) {
			LOGGER.error("URL cannot be null or empty");
			return null;
		}
		LOGGER.info("URL -> " + url);
		HttpPost post = new HttpPost(url.trim());
		
		if (headers != null) {
			for (int i =0; i<headers.size(); i++) {
				post.addHeader(headers.get(i).getName(), headers.get(i).getValue());
			}
		}
		
		if (!(authCredentials == null || authCredentials.trim().isEmpty())) {
			post.addHeader("Authorization", "Basic " + Base64.encodeBase64String(authCredentials.trim().getBytes()));
		}
		
		post.setEntity(new StringEntity(jsonBody, ContentType.APPLICATION_JSON));
		return getResponse(url, execute(post));
	}

	/**
	 * 
	 * @param url
	 * @param body
	 * @param authCredentials
	 *            in format "USERNAME:PASSWORD"
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public HttpResponse post(final String url, final List<NameValuePair> body)
			throws ClientProtocolException, IOException {
		return post(url, body, null);
	}

	/**
	 * 
	 * @param url
	 * @param body
	 * @param authCredentials
	 *            in format "USERNAME:PASSWORD"
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public HttpResponse post(final String url, final List<NameValuePair> body, final String authCredentials)
			throws ClientProtocolException, IOException {
		if (url == null || url.trim().isEmpty()) {
			LOGGER.error("URL cannot be null or empty");
			return null;
		}
		LOGGER.info("URL -> " + url);
		HttpPost post = new HttpPost(url.trim());
		post.addHeader("Content-Type", "application/x-www-form-urlencoded");
		if (!(authCredentials == null || authCredentials.trim().isEmpty())) {
			post.addHeader("Authorization", "Basic " + Base64.encodeBase64String(authCredentials.trim().getBytes()));
		}
		post.setEntity(new UrlEncodedFormEntity(body));
		return getResponse(url, execute(post));
	}

	private HttpResponse getResponse(final String url, final HttpResponse response) {
		if (response == null) {
			LOGGER.error("Response is null...");
			return null;
		}
		int responseCode = response.getStatusLine().getStatusCode();
		switch (responseCode / 100) {
		case 2:
			return response;
		case 4:
		case 5:
			LOGGER.error("URL -> " + url + " \t Status Code -> " + responseCode + "          Status Message -> "
					+ response.getStatusLine().getReasonPhrase());
			return null;
		}
		return null;
	}

	private synchronized HttpResponse execute(final HttpUriRequest request)
			throws ClientProtocolException, IOException {
		if (request == null) {
			LOGGER.error("Request cannot be NULL");
			return null;
		}
		return client.execute(request);
	}
}
