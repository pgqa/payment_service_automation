package com.rummycircle;

public interface ServicesEndPoint {

	public static final java.lang.String RUMMYSTARS_WRITE_TESTIMONIAL = "/fusion.rummycircle.com/fusion/service/testimonial/writeTestimonial";
	public static final java.lang.String RUMMYSTARS_GET_USER_TESTIMONIAL = "/fusion.rummycircle.com/fusion/service/testimonial/getUserTestimonial";
	public static final java.lang.String RUMMYSTARS_IS_ELIGIBLE_TESTIMONIAL = "/fusion.rummycircle.com/fusion/service/testimonial/eligible";
	public static final String RUMMYSTARS_ANALYTICS_BONUS = "/fusion.rummycircle.com/fusion/service/testimonialAnalytics/bonus";
	public static final java.lang.String BAF_ACCESS_BRING_A_FRIEND_PAGE = "/bafservice.rummycircle.com/baf/service/bringFriend/serveBAFPage/";
	public static final java.lang.String BAF_ACCESS_SEND_PERSONAL_INVITE_PAGE = "/bafservice.rummycircle.com/baf/service/bringFriend/sendPersonalInvite/";
	public static final java.lang.String BAF_ACCESS_FILTER_CONTACT = "/bafservice.rummycircle.com/baf/service/bringFriend/filterContacts/";
	public static final java.lang.String BAF_INVITE_TO_JOIN = "/bafservice.rummycircle.com/baf/service/bringFriend/inviteToJoin/";
	public static final String BAF_TRACK_YOUR_BONUS = "/bafservice.rummycircle.com/baf/service/bringFriend/trackBonus/";
	public static final String BAF_SEND_REMINDER = "/bafservice.rummycircle.com/baf/service/bringFriend/sendReminder/";
	public static final String BAF_GET_OVERLAY_INFO = "/bafservice.rummycircle.com/baf/service/bringFriend/getOverlayInformation";
	public static final java.lang.String VIEW_TRANSACTIONS_ALL_TRANSACTIONS_PAGE = "/fusion.rummycircle.com/fusion/service/transactions/getTransactions";
	public static final java.lang.String VIEW_TRANSACTIONS_TABLE_TRANSACTIONS = "/fusion.rummycircle.com/fusion/service/transactions/table-transactions";
	public static final java.lang.String VIEW_TRANSACTIONS_DEAL_TRANSACTIONS = "/fusion.rummycircle.com/fusion/service/transactions/deal-transactions";
	public static final java.lang.String VIEW_TRANSACTIONS_MTT_GAME_TRANSACTIONS = "/fusion.rummycircle.com/fusion/service/transactions/mtt-game-transactions";
	public static final java.lang.String PIXEL_SERVICE_ALL_PIXELS = "/player/pixel/getpixel.html";
	public static final String LOBBY_TD = "/tlistTemplate.do";
	public static final String LOBBY_ADMIN = "/api/lobbyab";
	public static final String LOBBY_TD_SORT_WITH_PREFERENCE = "/fusion.rummycircle.com/fusion/service/tlist/saveSortingOrder";
	public static final String LOBBY_GET_TD_SORT = "/fusion.rummycircle.com/fusion/service/tlist/getSortingOrder";
	public static final String LOBBY_TD_FILTER_WITH_PREFERENCE = "/fusion.rummycircle.com/fusion/service/tlist/saveTListFilter";
	public static final String LOBBY_GET_TD_FILTER = "/fusion.rummycircle.com/fusion/service/tlist/getTListFilter";

	public static final String VALIDATE_SEND_EMAIL = "/fusion.rummycircle.com/fusion/service/forgotPassword/validateAndSendEmail";
	public static final String SAVE_NEW_PASSWORD = "/fusion.rummycircle.com/fusion/service/forgotPassword/saveNewPassword";
	public static final String CHANGE_PASSWORD = "/fusion.rummycircle.com/fusion/service/forgotPassword/changePassword";
	public static final String ShowChange_Email = "/fusion.rummycircle.com/fusion/service/emailChange/showChangeEmailPage";
	public static final String Change_Email = "/fusion.rummycircle.com/fusion/service/emailChange/changeEmailAddress";
	public static final String ShowEmail_Confirmation = "/fusion.rummycircle.com/fusion/service/emailChange/showEmailConfirmation";
	public static final String verify_acount = "/fusion.rummycircle.com/fusion/service/emailChange/verifyAccount";
	public static final String WITHDRAW_CASH_SERVEPAGE = "/fusion.rummycircle.com/fusion/service/withdraw/serveWithDrawPage";
	public static final String WITHDRAW_CASH_PLACEREQ = "/fusion.rummycircle.com/fusion/service/withdraw/placeWithdrawRequest";
	public static final String WITHDRAW_CASH_CANCEL = "/fusion.rummycircle.com/fusion/service/withdraw/cancelWithdrawal";
	public static final String WITHDRAW_CASH_LIST = "/fusion.rummycircle.com/fusion/service/withdraw/getWithdrawList";
	public static final String WITHDRAW_CASH_DETAILS = "/fusion.rummycircle.com/fusion/service/withdraw/getWithdrawDetails";

	public static final String Resend_Email_Confirmation = "/fusion.rummycircle.com/fusion/service/emailChange/resendEmailConfirmation";
	public static final String Get_Login_Attempts = "/fusion.rummycircle.com/fusion/service/emailChange/getLoginAttemptsLeft";
	public static final String Show_Change_User_Name = "/fusion.rummycircle.com/fusion/service/userName/showChangeUserNamePage";
	public static final String Get_User_Name_Suggestions = "/fusion.rummycircle.com/fusion/service/userName/getUserNameSuggestions";
	public static final String Change_User_Name = "/fusion.rummycircle.com/fusion/service/userName/changeUserName";
	public static final String REGISTER_USER = "/player/register/register.html";
	public static final String UPDATE_DEPOSIT_LIMIT = "/limitservice/limitdata/update-deposit-limit";
	public static final String UPDATE_DEPOSIT_DEPOSIT = "/fusion.rummycircle.com/fusion/service/addcash/updateMonthlyLimit";
	public static final String UPDATE_MOBILE_NUMBER = "/fusion.rummycircle.com/fusion/service/addcash/updateMobileNumber";
	public static final String LOGIN_USER = "/signup/login";
	public static final String CONTACT_US_PAGE_INTERNAL = "/fusion.rummycircle.com/fusion/service/support/setcontactuspage";
	public static final String CONTACT_US_SEND_MAIL = "/mailer/sendMail";
	public static final String CONTACT_US_PAGE = "/support/contact-us.html";

	public static final String GET_REWARDSTORE_TILES = "/rewardstoreservice/rewardstore/service/rewardStore/getRewardStoreTiles";
	public static final String GET_REWARDSTORE_BONUS_ITEMS = "/rewardstoreservice/rewardstore/service/rewardStore/getRewardStoreBonusItems";
	public static final String GET_RELATIONSHIP_MANAGER = "/rewardstoreservice/rewardstore/service/rewardStore/getRelationshipManager";
	public static final String GET_TILE_STATUS = "/rewardstoreservice/rewardstore/service/rewardStore/getRewardTileStatus";
	public static final String GET_TILE_ATTRIBUTE = "/rewardstoreservice/rewardstore/service/rewardStore/getRewardTileAttributes";
	public static final String UPDATE_TILE_ATTRIBUTE = "/rewardstoreservice/rewardstore/service/rewardStore/updateRewardTileAttributes";
	public static final String UPDATE_TILE_STATUS = "/rewardstoreservice/rewardstore/service/rewardStore/updateRewardTileStatus";
	public static final String REDEEM_REWARD_STORE_ITEM = "/rewardstoreservice/rewardstore/service/rewardStore/redeemRewardStoreItem";

	public static final String GET_TICKET_LIST = "/fusion.rummycircle.com/fusion/service/playerAccount/getTickets";
	public static final String GET_BONUS_LIST = "/fusion.rummycircle.com/fusion/service/playerAccount/getBonusDetails";
	public static final String GET_ACC_OVERVIEW = "/fusion.rummycircle.com/fusion/service/playerAccount/overview";
	public static final String GET_REFILL_CHIPS = "/fusion.rummycircle.com/fusion/service/playerAccount/refillChips";
	public static final String GET_REWARD_POINT_LIST = "/fusion.rummycircle.com/fusion/service/playerAccount/getRewardPointsInfo";

	public static final String SEND_FTD_OTP = "/fusion.rummycircle.com/fusion/service/mobileVerification/sendOtpForFTD";
	public static final String SEND_RD_OTP = "/fusion.rummycircle.com/fusion/service/mobileVerification/checkAndSendOtpForRD";
	public static final String RESEND_OTP = "/fusion.rummycircle.com/fusion/service/mobileVerification/resendOtp";
	public static final String IS_TOP_BAR_SHOWN = "/fusion.rummycircle.com/fusion/service/mobileVerification/isTopBarToBeShown";
	public static final String SAVE_MOB_TEMP = "/fusion.rummycircle.com/fusion/service/mobileVerification/saveMobileTemporarily";
	public static final String VERIFY_OTP = "/fusion.rummycircle.com/fusion/service/mobileVerification/verifyOtp";
	public static final String IS_PROFILE_BAR_SHOWN = "/fusion.rummycircle.com/fusion/service/mobileVerification/isProfileBarToBeShown";
	public static final String MANAGE_MOBILE = "/fusion.rummycircle.com/fusion/service/mobileVerification/manageMobile";
	public static final String IS_PROFILE_OTP_SHOWN = "/fusion.rummycircle.com/fusion/service/mobileVerification/isProfileOTPSectionToBeShown";
	public static final String Daily_LoyaltyInfo = "/fusion.rummycircle.com/fusion/service/playerAccount/getDailyLoyaltyInfo";
	public static final String Get_nftdetails = "/nfs/api/nft/nftdetails/";
	public static final String Get_LoyaltyInfo = "/fusion.rummycircle.com/fusion/service/playerAccount/getLoyaltyInfo";

	public static final String Get_Reward_Points_Info = "/fusion.rummycircle.com/fusion/service/playerAccount/getDailyRewardInfo";

	public static final String GET_ERLS_MSG = "/fusion.rummycircle.com/fusion/service/player/erlsMessage";
	public static final String GET_CASHBACK = "/fusion.rummycircle.com/fusion/service/cashback/getCashBackDetails";
	public static final String GET_REWARDSTORE_ITEM_DETAILS = "/rewardstoreservice/rewardstore/service/rewardStore/rewardItem/";

	public static final String GET_ELASTICSEARCH = "/iplookupservice/_search";

	public static final String GET_ADD_CASH_PAGE = "/fusion.rummycircle.com/fusion/service/addcash/getAddcashPage";
	public static final String GET_USER_ADD_CASH_LIMIT = "/fusion.rummycircle.com/fusion/service/addcash/getUserAddCashLimits";
	public static final String UPDATE_MAX_MONTHLY_LIMIT = "/limitservice/limitdata/updateMaxMonthlyLimit";
	public static final String UPDATE_MAX_MONTHLY_LIMIT_MOBILE = "/limitservice/limitdata/updateMaxMonthlyLimitWithMobile";
	public static final String UPDATE_MONTHLY_LIMIT = "/limitservice/userdetail/updateUserMonthlyLimit";

	public static final String GET_LOBBY_TLIST_CASH = "/lobby/tlist/cash";
	public static final String GET_LOBBY_TLIST_PRACTICE = "/lobby/tlist/practice";
	public static final String GET_LOBBY_TLIST_MTT = "/lobby/tlist/mtt";
	public static final String GET_LOBBY_TLIST_FMG = "/lobby/tlist/fmg";
	public static final String GET_LOBBY_MYGAMES = "/lobby/mygames";

	public static final String SEARCH_ORDER = "/payment-service/player/search-order";
	public static final String PG_CONFIG = "";
	public static final String PAY_MODES = "/payment-service/pg/pay-modes";
	public static final String PAYMENT_MODES_ADD = "/payment-service/paymentmode/add-update-pm";
	public static final String DELETE_PAYMENT_MODE = "/payment-service/paymentmode/delete";
	public static final String INIT_PAYMENT = "/payment-service/add-cash/init-pay";
	public static final String GENERATE_KEY_SECURE = "/payment-service/authentication/generate-key-secure";
	public static final String CLIENT_ROLE = "/payment-service/authentication/client-role";
	public static final String ACTIVE_CLIENT = "/payment-service/authentication/active-client";
	public static final String MAP_PAYMENT_MODE = "/payment-service/pg/map-payment-mode-option";
	public static final String ADD_UPDATE_MASTER_TABLE = "/payment-service/payment-master/add-update";
	public static final String GET_DATA_FROM_MASTER_TABLE = "/payment-service/payment-master/get-pay-master/";
	public static final String GET_WALLET_DETAILS = "/payment-service/wallet/details";
	public static final String POST_ADD_CASH = "/fusion.rummycircle.com/fusion/service/payment/process-transaction";

	public static final String GET_DEFAULT_PAYMENT_MODE = "/payment-service/pg/default-pay-mode";
	public static final String ALL_PAYMENT_DATA = "/payment-service/pg/all-payment-data";
	public static final String PROCESS_PAY = "/payment-service/process-pay/process-pay-response";
	public static final String FREECHARGE_WALLET_LINK = "/payment-service/wallet/link";

	public static final String NAE_DOWNLOAD = "/download";
	public static final String NAE_DATA = "/data";
	public static final String DUMMY_GET_WEBHOOK = "/dummy-webhook-tool/getWebhook";
	public static final String DUMMY_GET_ALL_WEBHOOK = "/dummy-webhook-tool/getAllWebhook";

	public static final String NAE_CONFIG = "/admin/config";
	public static final String NAE_HOOK = "/hook/";
	public static final String BROWSER_ENDPOIT = "/player/register/register.html";

	public static final String GET_REWARD_STORE_ITEM_DETAILS_FROM_PROMOCODE = "/rewardstoreservice/rewardstore/service/rewardStore/promocode/rewardItem";
	public static final String FACEBOOK_REGISTRATION_SERVICE = "/registrationservice/reg/service/signup/socialusers";
	public static final String BAF_FILTER_EMAILS_PHONE = "/bafservice.rummycircle.com/baf/service/bringFriend/filterEmailsAndPhoneNumbers";

	public static final String KYC_VERIFICATION = "/fusion.rummycircle.com/fusion/service/player/isIdProofSubmitted";

	public static final String GET_EXPERIMENT_AFFILIATE = "/fusion.rummycircle.com/fusion/service/abcontroller/getExperimentDetails";
	public static final String CREATE_EXPERIMENT_AFFILIATE = "/fusion.rummycircle.com/fusion/service/abcontroller/createExperiment";
	public static final String UPDATE_EXPERIMENT_AFFILIATE = "/fusion.rummycircle.com/fusion/service/abcontroller/updateExperiment";

	public static final String Affiliates_signup = "/signup/register";

	public static final String KYC_GET_DOC_DETAILS = "/limitservice/kycdetails/getdocdetails";
	public static final String KYC_UPDATE_DOC_DETAILS = "/limitservice/kycdetails/updatedocdetails";

	public static final String KYC_GET_BASE_USER_ID = "/limitservice/userdetail/baseuserid";

	public static final String KYC_CUMULATIVE_DEPOSIT = "/limitservice/userdetail/cumulativelimits";

	public static final String KYC_ADD_CASH_LIMIT = "/limitservice/userdetail/addcashlimits";

	public static final String KYC_GET_ABSTATUS = "/fusion.rummycircle.com/fusion/service/abcontroller/getABStatus";

	public static final String KYC_GET_MAX_USER_ID = "/limitservice/userdetail/maxUserId";

	public static final String STATE_PINCODE_DETAILS = "/fusion.rummycircle.com/fusion/service/addressverification/getPostalCodeDetails";

	public static final String FUSIONPROXY_CHANGE_PASSWORD = "/player/account/change-password";

	public static final String HMS_GETHELPCONFIG = "/hms/getHelpConfig";

	public static final String FUSIONPROXY_CHANGE_PASSWORDPAGE = "/player/account/change-password.html";

	public static final String FUSIONPROXY_LOGIN = "/signup/login";

	public static final String FUSIONPROXY_CREATECAPTCHA = "/create";

	public static final String FUSIONPROXY_RESETEMAIL = "/player/account/forgot-password/validateemailandsendresetemail";

	public static final String FUSIONPROXY_RESETPASSWORD = "/player/account/forgot-password/reset-password";

	public static final String KYC_STATUS = "/fusion.rummycircle.com/fusion/service/idverification/getKycStatus";

	public static final String UPLOAD_HISTORY = "/fusion.rummycircle.com/fusion/service/idverification/getUploadHistory";

	public static final String LATEST_UPLOAD_HISTORY = "/fusion.rummycircle.com/fusion/service/idverification/getLatestUploadedHistory";

	public static final String ID_STATUS = "/fusion.rummycircle.com/fusion/service/idverification/getIdStatus";

	public static final String ADD_CASH_GET_EMAILID = "/fusion.rummycircle.com/fusion/service/addcash/getEmail";
	public static final String ADD_CASH_GET_PAN_DETAILS = "/fusion.rummycircle.com/fusion/service/addcash/getPanDetails";
	public static final String ADD_CASH_CLOSED_BLOCKED_PLAYER = "/fusion.rummycircle.com/fusion/service/addcash/checkIfBlocked";
	public static final String ADD_CASH_GET_USER_LIMITS = "/fusion.rummycircle.com/fusion/service/addcash/getUserLimits";
	public static final String ADD_CASH_GET_USER_CHANGE_LIMITS_DATA = "/fusion.rummycircle.com/fusion/service/addcash/getChangeLimitData";

	public static final String INSERT_UPLOAD_DATA = "/fusion.rummycircle.com/fusion/service/idverification/insertUserUploadData";

	public static final String CREATE_TEMPLATE = "/gpc/api/templates";

	public static final String CREATE_SCHEDULE = "/gpc/api/schedules";

	public static final String CREATE_LS = "/gpc/api/templates/liquiditysplit";

	public static final String CONCLUDE_LS = "/gpc/api/templates/liquiditysplit/conclude";

	public static final String LS_USERS = "/gpc/api/lobby/templates/liquiditysplit/users";

	public static final String ADD_CASH_LOAD_RESPONSE_PAGE_DETAILS = "/fusion.rummycircle.com/fusion/service/addcash/loadResponsePage";
	public static final String SHOW_ADDRESS_PAGE = "/fusion.rummycircle.com/fusion/service/addressverification/showAddressPage";

	public static final String GET_BAF_DATA = "/bafservice.rummycircle.com/baf/service/bringFriend/getBafDetails";
	public static final String UPDATE_BAF_DETAILS = "/bafservice.rummycircle.com/baf/service/bringFriend/updateBafDetails";
	public static final String POST_REGN_UPDATE_BAF_DATA = "/bafservice.rummycircle.com/baf/service/bringFriend/postRegUpdate";
	public static final String POST_LANDING_UPDATE_BAF_DATA = "/bafservice.rummycircle.com/baf/service/bringFriend/postLandingUpdate";

	public static final String LOGIN_SIGN_IN = "/player/signin";
	public static final String REGISTRATION_SIGN_UP = "/player/signup";
	public static final String ZK_CONFIG = "/mobileapp/iosconfig.json";
	public static final String USER_REGISTER = "/registrationservice/reg/service/signup/users";
	public static final String SET_TICKET_ADD_CASH_INFO="/fusion.rummycircle.com/fusion/service/addcash/setTicketAddCashInfo";

}
