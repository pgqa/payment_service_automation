package com.rummycircle;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import com.rummycircle.utils.reporting.extent.Logger;

public class VerificationCheck {

    String maxapiReponsetime = "500";
    private static Logger Log = new Logger();
   
    public Map<String, String> verifyResults(Map<String, String> actualdata, Map<String, String> expecteddata) {
        boolean responsetimeVerification = false;

        String keysVerified = "";
        Map<String, String> returnmap = new HashMap<String, String>();
        try {
            //Below loop will be executed only if expected is not having response time
            for (Map.Entry<String, String> actual : actualdata.entrySet()) {

                if (actual.getKey().contains("responsetime") && !(expecteddata.containsKey("responsetime"))) {
                    // Response time verification needs to be done only for API response
                    expecteddata.put(actual.getKey(), maxapiReponsetime);
                    responsetimeVerification = true;
                }
            }
			
            Map<String, String> map1 = Util.convertKeystoLowerCase(actualdata);
            Map<String, String> map2 = Util.convertKeystoLowerCase(expecteddata);
			
            for (Map.Entry<String, String> expected : map2.entrySet()) {
                if (map1.containsKey(expected.getKey())) {

                    if (expected.getValue() == null || map1.get(expected.getKey()) == null) {
                        map2.put(expected.getKey(), "null");
                        map1.put(expected.getKey(), "null");
                    }
                    if (expected.getKey().contains("responsetime")) {
                        keysVerified = keysVerified + "|" + expected.getKey();
//                        Log.info("keysVerified"+keysVerified);
                        if (Integer.valueOf(map1.get(expected.getKey())) < Integer.valueOf(map2.get(expected.getKey()))) {
                            Assert.assertTrue(true);
                        } else {
                           Log.warn("WARNING RESPONSE TIME Expected API response time should be less than " + map2.get(expected.getKey()) +"but found" + map1.get(expected.getKey()));
                        }

                    } else {
                        Assert.assertEquals(map1.get(expected.getKey()), map2.get(expected.getKey()));
                        keysVerified = keysVerified + "|" + expected.getKey();
                    }
                }

                else {
                    returnmap.put(expected.getKey(), "KEY_NOT_FOUND");
                }
            }
           if (responsetimeVerification && !keysVerified.contains("responsetime")) {
                Log.warn("Response Time -- Not verified ");
            }
            if (!returnmap.isEmpty()) {
                Log.info("Verification Done and Keys Verified = " + keysVerified);
            }

        } catch (Exception e) {
           Log.info("Verification-Exception" +e.getMessage());
        }
        return returnmap;
    }

}
