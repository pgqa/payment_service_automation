package com.rummycircle;

import java.util.Properties;

import com.jayway.jsonpath.JsonPath;
import com.rummycircle.utils.config.ConfigManager;
import com.rummycircle.utils.config.ConfigProperties;
import com.rummycircle.utils.database.ZookeeperManager;
import com.rummycircle.utils.reporting.extent.Logger;
import com.rummycircle.utils.testutils.PropertyReader;

public class ZooKeeperUtils {

	private static Properties custProp= PropertyReader.loadCustomProperties("custom.properties");
	private static Logger Log = Logger.getLogger();
	
	public static String getZkGatwayInfo(String key,String zkPath) {
		ConfigManager config = ConfigManager.getInstance();
		try {
			if (config.getString(ConfigProperties.IS_ZK_ENABLED.getKey()).equals("yes")) {
				ZookeeperManager test = new ZookeeperManager();
				String zkTechProcess = null;

				try {
					test.setZkHost(custProp.getProperty("acl.AB.Zk.ips"));
					zkTechProcess = test.getZookeperData(zkPath, key);
				} catch (IllegalStateException e) {
					Log.info(e.getMessage());
				}
				return zkTechProcess;
			}
		} catch (Exception e) {
			Log.error("Error message: " + e.getMessage());
		}
		return null;
	}

	public static String getTechProcessIV() {
		String zkPath = custProp.getProperty("ps.initPay.ZkPath.reverie");
		String data = getZkGatwayInfo(custProp.getProperty("zk.gateway.tp"),zkPath);
		return data == null ? null : JsonPath.read(data, "$.request.TECHPROCESS_IV");
	}
	

	public static String getTechProcessKey() {
		String zkPath = custProp.getProperty("ps.initPay.ZkPath.reverie");
		String data = getZkGatwayInfo(custProp.getProperty("zk.gateway.tp"),zkPath);
		return data == null ? null : JsonPath.read(data, "$.request.TECHPROCESS_KEY");
	}

}

