package com.rummycircle;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class MongoDbHelper {

	public static BasicDBObject createQuery(BasicDBObject queryObject, String query,
			String value) {
		if (queryObject == null) {
			queryObject = new BasicDBObject(query, value);
		} else {
			queryObject.append(query, value);
		}

		return queryObject;
	}

	public static BasicDBObject createQuery(BasicDBObject queryObject, String query,
			int value) {
		if (queryObject == null) {
			queryObject = new BasicDBObject(query, value);
		} else {
			queryObject.append(query, value);
		}

		return queryObject;
	}

	public static String executeQuery(DBCollection m_collection, BasicDBObject query) {
		DBCursor cursor = m_collection.find(query);

		if (cursor.hasNext()) {
			return cursor.next().toString();
		}

		return null;
	}
}
