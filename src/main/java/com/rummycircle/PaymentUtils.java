package com.rummycircle;

import java.util.Base64;
import java.util.Properties;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.rummycircle.servicesjson.paymentservice.ProcessPayService;
import com.rummycircle.utils.database.DatabaseManager;
import com.rummycircle.utils.reporting.extent.Logger;
import com.rummycircle.utils.testutils.PropertyReader;

public class PaymentUtils {

	private static Logger Log = Logger.getLogger();
	private static Properties custProp  = PropertyReader.loadCustomProperties("custom.properties");
	private static final String CIPHER_TP_INSTANCE = "AES/CBC/PKCS5Padding";
	private static final String ALGO = "AES";
	
	public static String encrypt(String text, byte[] iv, byte[] key) throws Exception {
		Cipher cipher = Cipher.getInstance(CIPHER_TP_INSTANCE);

		SecretKeySpec keySpec = new SecretKeySpec(key, ALGO);
		IvParameterSpec ivSpec = new IvParameterSpec(iv);

		cipher.init(1, keySpec, ivSpec);
		byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
		return new String(Base64.getEncoder().encode(results));
	}

	public static Object[][] getDBWithCardInitData(String gateway, int channelId) {
		String schema = custProp.getProperty("ps.schema");
		String query = String.format(
				"select p.merchant_name as merchantName,p.processor_code as bankCode,m.name as modeName,o.name as optionName, p.channel_id as channelId, g.name as gateway from %spayment_configuration_details p, %spayment_mode m, %spayment_option o, %spayment_mode_option_mapping map, %spayment_gateway g where p.active=1 and p.pay_type_opt_map_id=map.id and map.payment_mode_id=m.id and map.payment_option_id=o.id and p.detail_required=1 and m.id in(14,15) and g.id=p.gateway_id and g.id=(select id from %spayment_gateway where name='%s') and p.channel_id=%d limit 1;",
				schema, schema, schema, schema, schema, schema, gateway, channelId);
		Log.info("SQL Query: " + query);
		return DatabaseManager.executeQuery(query);
	}
	
	public static Object[][] getDBInitData(String gateway, int channelId) {
		String schema = custProp.getProperty("ps.schema");
		String query = String.format(
				"select p.merchant_name as merchantName,p.processor_code as bankCode,m.name as modeName,o.name as optionName, p.channel_id as channelId, g.name as gateway from %spayment_configuration_details p, %spayment_mode m, %spayment_option o, %spayment_mode_option_mapping map, %spayment_gateway g where p.active=1 and p.pay_type_opt_map_id=map.id and map.payment_mode_id=m.id and map.payment_option_id=o.id and p.detail_required=0 and g.id=p.gateway_id and g.id=(select id from %spayment_gateway where name='%s') and p.channel_id=%d limit 1;",
				schema, schema, schema, schema, schema, schema, gateway, channelId);
		Log.info("SQL Query: " + query);
		return DatabaseManager.executeQuery(query);
	}

	public String getUserIdForRD(String gateway) {
		String query = String.format("select distinct up.user_id from gateway_transactions gt,payment_tracking pt, "
				+ "user_preference up where gt.txn_id=pt.txn_id and pt.payment_method='%s' "
				+ "and gt.user_id=up.user_id and gt.status='Authorised' and "
				+ "up.real_account_creation_date is not null order by rand() limit 1;", gateway);
		return DatabaseManager.executeQuery(query)[0][0].toString();
	}
	

	public static Object[][] getInitNonWalletData(String gateway, int channelId,String paymenModeName, int cardDetailRequired) {
		String schema = custProp.getProperty("ps.schema");
		String query = String.format(
				"select p.merchant_name as merchantName,p.processor_code as bankCode,m.name as modeName,o.name as optionName, p.channel_id as channelId, g.name as gateway from %spayment_configuration_details p, %spayment_mode m, %spayment_option o, %spayment_mode_option_mapping map, %spayment_gateway g where p.active=1 and p.pay_type_opt_map_id=map.id and map.payment_mode_id=m.id and map.payment_option_id=o.id and p.detail_required=%d and m.id =((SELECT id FROM %spayment_mode m  where name='%s' limit 1)) and g.id=p.gateway_id and g.id=(select id from %spayment_gateway where name='%s') and p.channel_id=%d ;",
				schema, schema, schema, schema, schema, cardDetailRequired, schema,paymenModeName, schema,gateway, channelId);
		Log.info("SQL Query: " + query);
		return DatabaseManager.executeQuery(query);
	}

	public static Object[][] getInitWalletData(String gateway, int channelId,String paymenModeName) {
		String schema = custProp.getProperty("ps.schema");
		String query = String.format(
				"select p.merchant_name as merchantName,p.processor_code as bankCode,m.name as modeName,o.name as optionName, p.channel_id as channelId, g.name as gateway from %spayment_configuration_details p, %spayment_mode m, %spayment_option o, %spayment_mode_option_mapping map, %spayment_gateway g where p.active=1 and p.pay_type_opt_map_id=map.id and map.payment_mode_id=m.id and map.payment_option_id=o.id and m.id =((SELECT id FROM %spayment_mode m  where name='%s' limit 1)) and g.id=p.gateway_id and g.id=(select id from %spayment_gateway where name='%s') and p.channel_id=%d ;",
				schema, schema, schema, schema, schema, schema,paymenModeName, schema,gateway, channelId);
		Log.info("SQL Query: " + query);
		return DatabaseManager.executeQuery(query);
	}
	
	public static ProcessPayService getProcessPayData(String order_id) {
		String query = String.format(
				"select gt.user_id as user_id,gt.txn_id as txn_id,gt.amount as amount,gt.status as authStatus,gt.channel_id as channel_id,pt.payment_options as paymentOption,pt.payment_type as paymentMode, pt.payment_method as gateway,gt.order_id as order_id "
						+ "from gateway_transactions gt,payment_tracking pt where gt.txn_id=pt.txn_id and gt.order_id='%s'",
				order_id);
		Object[][] db=DatabaseManager.executeQuery(query);
		ProcessPayService ppService = new ProcessPayService();
		ppService.setUserId((Integer)db[0][0]);
		Double amount = (double)db[0][1];
		ppService.setAmount((Integer)db[0][2]);
		ppService.setAuthStatus((String)db[0][3]);
		ppService.setTxnId(((Integer)db[0][3]));
		ppService.setGateway((String)db[0][3]);
		ppService.setChannelId((Integer)db[0][3]);
		ppService.setPaymentMode((String)db[0][3]);
		ppService.setPaymentOption((String)db[0][3]);
		ppService.setOrderId((String)db[0][3]);
		return ppService;
	}
	

	 

}

