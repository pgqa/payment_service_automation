package com.rummycircle.paymentservice;

import java.util.Properties;

import com.rummycircle.restclient.HTTPHeaders;
import com.rummycircle.restclient.HTTPMethod;
import com.rummycircle.restclient.HTTPRequest;
import com.rummycircle.restclient.HTTPResponse;
import com.rummycircle.restclient.RESTAssuredClient;
import com.rummycircle.utils.reporting.extent.Logger;
import com.rummycircle.utils.testutils.PropertyReader;

public class PaymentServiceUtil {

	
	private static final Properties configProp = PropertyReader.loadCustomProperties(System.getProperty("configfile", "config.properties"));
	private static final Logger logs = Logger.getLogger();
	private static final RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
	private static String hostPort = "http://10.14.25.135:9000/";
	static {
		hostPort = configProp.getProperty("protocol") + "://" + configProp.getProperty("host") + ":" + configProp.getProperty("port")+"/";
	}
	

	public static HTTPResponse searchOrder(String requestBody) {
		HTTPHeaders header=new HTTPHeaders();
		header.addHeader("Content-Type", "application/json");
		HTTPRequest requestSpec = new HTTPRequest(header, requestBody);
		String url = hostPort + "payment-service/player/search-order";
		return rc.sendRequest(HTTPMethod.POST, url, requestSpec);
	}
	
	public static HTTPResponse manualProcessTransaction(String requestBody) {
		HTTPHeaders header=new HTTPHeaders();
		header.addHeader("Content-Type", "application/json");
		HTTPRequest requestSpec = new HTTPRequest(header, requestBody);
		String url = hostPort + "payment-service/manual/processTransaction";
		return rc.sendRequest(HTTPMethod.POST, url, requestSpec);
	}
	public static HTTPResponse createProcessTransaction(String requestBody) {
		HTTPHeaders header=new HTTPHeaders();
		header.addHeader("Content-Type", "application/json");
		HTTPRequest requestSpec = new HTTPRequest(header, requestBody);
		String url = hostPort + "payment-service/manual/createTransaction";
		return rc.sendRequest(HTTPMethod.POST, url, requestSpec);
	}
	
	public static void main(String[] args) {
		String s="{\"userId\":17335}";
		HTTPResponse rs=searchOrder(s);
		System.out.println(rs.getStatusCode());
		System.out.println(rs.getBody().getBodyText());
	}
	
}
