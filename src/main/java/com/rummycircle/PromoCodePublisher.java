package com.rummycircle;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rummycircle.utils.reporting.extent.Logger;
import com.rummycircle.utils.testutils.PropertyReader;

public class PromoCodePublisher {

	public static Logger log = Logger.getLogger();
	public static Properties customProp = PropertyReader
			.loadCustomProperties("custom.properties");


    public  static void publishMsgForPromoCode(String promoCode,String userId) throws IOException, TimeoutException, InterruptedException {
        Connection connection = createConnection();
        Channel channel = connection.createChannel();
        String msg = String.format(customProp.getProperty("mq.msg.promocode"), promoCode,userId);
        log.info("msg : "+msg);
        channel.exchangeDeclare("EdsPromocodeMap", "fanout", false);
        channel.basicPublish("EdsPromocodeMap", "", false, null, msg.getBytes(StandardCharsets.UTF_8));
        log.info("SUCCESS");

       
    }
   
    public static Connection createConnection() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(customProp.getProperty("rmq.host"));
		factory.setPort(Integer.parseInt(customProp.getProperty("rmq.port")));
		factory.setUsername(customProp.getProperty("rmq.username"));
		factory.setPassword(customProp.getProperty("rmq.password"));
        log.info("rmq.host : "+customProp.getProperty("rmq.host"));
        log.info("rmq.port : "+customProp.getProperty("rmq.port"));
        log.info("rmq.username : "+customProp.getProperty("rmq.username"));
        log.info("rmq.password : "+customProp.getProperty("rmq.password"));

        Connection connection = factory.newConnection();
        return connection;
    }
    
    public static void main(String args[]) throws IOException, TimeoutException, InterruptedException {
    			publishMsgForPromoCode("RP_NEW_1000","523758");
    }


}
