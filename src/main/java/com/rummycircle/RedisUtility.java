package com.rummycircle;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

public class RedisUtility {

	private Logger logger = LoggerFactory.getLogger(RedisUtility.class);

	private static RedisUtility redisUtility = null;
	public JedisCluster jedisCluster = null;
	private static Properties serverProps;
	private static final String AMOUNT = "Amount";
	private static final Integer sessionTimeOut = 1800000;

	public static RedisUtility getInstance(String k) {
		System.out.println("REDISSSSS:::::");

		if (redisUtility == null) {
			System.out.println("Initializing");
			redisUtility = new RedisUtility(k);
		}
		return redisUtility;
	}

	/***
	 * Initializes the Jedis Framework
	 */
	private RedisUtility(String reGdisIp) {
		// getSystemProperties();
		String IP1 = "10.14.24.175";
		String IP2 = "10.14.24.176";
		int port1 = 7000;
		int port2 = 7001;
		int port3 = 7002;
		int port4 = 7003;
		int port5 = 7004;
		int port6 = 7005;

		logger.info("Initializing Redis....");
		if (jedisCluster == null) {
			JedisPoolConfig config = new JedisPoolConfig();
			config.setTestOnBorrow(true);
			config.setMaxTotal(128);
			config.setTestWhileIdle(true);
			Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();

			jedisClusterNodes.add(new HostAndPort(IP1, port1));
			jedisClusterNodes.add(new HostAndPort(IP1, port2));
			jedisClusterNodes.add(new HostAndPort(IP1, port3));
			jedisClusterNodes.add(new HostAndPort(IP2, port4));
			jedisClusterNodes.add(new HostAndPort(IP2, port5));
			jedisClusterNodes.add(new HostAndPort(IP2, port6));

			jedisCluster = new JedisCluster(jedisClusterNodes, 10 * 1000, config);

		}

	}

	/****
	 * Pushes the key value pair in redis
	 * 
	 * @param key
	 *            : Unique key.
	 * @param field
	 *            : Attribute for every key
	 * @param value
	 *            : Value for every attribute
	 */
	public void pushToRedis(String key, String field, String value) {
		if (jedisCluster != null) {
			try {
				jedisCluster.hset(key, field, value);
				jedisCluster.expire(key, sessionTimeOut);
			} catch (Exception e) {
				// Auto-generated catch block
				e.printStackTrace();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		} else {
			System.out.println("JEDIS Cluster NOt Initialized.");
		}

	}

	public void pushToRedis(String key, String value) {
		if (jedisCluster != null) {
			try {
				jedisCluster.lpush(key, value);
				jedisCluster.expire(key, sessionTimeOut);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		} else {
			System.out.println("JEDIS Cluster NOt Initialized.");
		}

	}

	public void setToRedis(String key, String value) {
		if (jedisCluster != null) {
			try {
				jedisCluster.set(key, value);
				jedisCluster.expire(key, sessionTimeOut);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		} else {
			System.out.println("JEDIS Cluster NOt Initialized.");
		}

	}

	public String getFromRedis(String key)

	{
		String value = null;
		if (jedisCluster != null) {
			try {
				value = jedisCluster.get(key);
				jedisCluster.expire(key, sessionTimeOut);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		} else {
			System.out.println("JEDIS Cluster NOt Initialized.");
		}
		return value;

	}

	public String getValues(String key, String field) {
		String value = "";
		try {
			if (jedisCluster.exists(key)) {

				value = (jedisCluster.hget(key, field));
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("Redis Unavaliable");
			return value;
		}
		return value;
	}

	public String getValues(String key) {
		String value = "";
		try {
			if (jedisCluster.exists(key)) {

				value = (jedisCluster.get(key));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Redis Unavaliable");
			return value;
		}
		return value;
	}

	/****
	 * Checks IF a key is present with all its attributes,.
	 * 
	 * @param keyToCheck
	 *            : Key to be verified.
	 * @param field
	 *            : Attribute present for any key.
	 * @param valueToCheck
	 *            : Value for every attribute
	 * @return
	 */
	public boolean pullFromRedis(String keyToCheck, String field, String valueToCheck) {
		boolean flag = false;
		logger.debug("JEDIS:Retrieving  Key " + keyToCheck);

		try {
			if (jedisCluster.exists(keyToCheck)) {
				String value = (jedisCluster.hget(keyToCheck, field));
				// if(value.equalsIgnoreCase( valueToCheck ))
				if (field.equalsIgnoreCase(AMOUNT) && (Double.parseDouble(valueToCheck) == Double.parseDouble(value))) {
					flag = true;
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return flag;
		}
		return flag;
	}

	public void deleteFromRedis(String key) {
		try {
			if (jedisCluster.exists(key)) {
				jedisCluster.del(key);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Redis Unavailable");
		}

	}

	public Map<String, String> getAllFieldValues(String key) {
		Map<String, String> value = null;
		try {
			if (jedisCluster.exists(key)) {
				value = jedisCluster.hgetAll(key);
				System.out.println("inside if");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Redis Unavailable");
			return value;
		}

		return value;
	}

	public long removeFieldValues(String key, String field) {
		long deleted = 0;
		try {
			if (jedisCluster.exists(key)) {
				deleted = jedisCluster.hdel(key, field);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Redis Unavailable");
			return deleted;
		}

		return deleted;
	}

	public void pushToRedisTxnFailure(String key, String field, String value, int timeout) {
		System.out.println("Pushing:::::::" + field + ":::::::" + value);
		// Jedis jedis = null;
		try {
			// jedis = jedisPool.getResource();
			jedisCluster.hset(key, field, value);
			jedisCluster.expire(key, timeout);
			logger.debug("JEDIS:Stored Key" + key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("REDIS UNAVAILABLE");
		}

	}

	public void getSystemProperties() {

		serverProps = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("/home/dineshb/redis.props");
			serverProps.load(input);
			System.out.println("Server Ip for this application " + serverProps.getProperty("server.Ip"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/****
	 * 
	 * @param key
	 *            : The element that needs to be retrieved from the file
	 * @return: The value of the key
	 */
	public static String getServerProps(String key) {
		System.out.println("serverProps.getProperty( " + key + " ) " + serverProps.getProperty(key));
		return serverProps.getProperty(key);
	}

	public void subscribe(final JedisPubSub subscriber, final String channelName) {
		final Map<String, JedisPool> clusterMap = jedisCluster.getClusterNodes();
		new Thread(() -> {
			try {
				for (JedisPool pool : clusterMap.values()) {
					System.out.println("Value = " + pool);
					Jedis subscriberJedis = pool.getResource();
					logger.info("Subscribing to \"portal.admin.configdatachannel\". This thread will be blocked.");
					subscriberJedis.subscribe(subscriber, channelName);
					logger.info("Subscription ended.");
				}
			} catch (Exception e) {
				logger.error("Subscribing failed.", e);
				e.printStackTrace();
			}
		}).start();
	}
}