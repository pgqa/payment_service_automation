package com.rummycircle;

import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONObject;
import org.testng.Assert;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.rummycircle.restclient.HTTPHeaders;
import com.rummycircle.restclient.HTTPMethod;
import com.rummycircle.restclient.HTTPParams;
import com.rummycircle.restclient.HTTPRequest;
import com.rummycircle.restclient.HTTPResponse;
import com.rummycircle.utils.reporting.extent.Logger;
import com.rummycircle.utils.testutils.BaseTest;

public class Util extends BaseTest {

	private static Logger logger = Logger.getLogger();
	DbPool db = DbPool.getInstance();
	private Random random = new Random();

	public static Map jsonToMapConverter(String jsonData) {

		HashMap<String, String> dataMap = new HashMap<String, String>();
		String jsondata = jsonData;
		System.out.println(jsondata);
		if (jsonData.contains("|")) {
			String testdata[] = jsonData.split("\\|");

			for (int i = 0; i < testdata.length; i++) {
				// String temp[];
				if (testdata[i].contains("Verification=")) {
					String temp[] = testdata[i].split("Verification=");
					dataMap.put("Verification", temp[1]);
				} else if (testdata[i].contains("BODY=")) {
					String temp[] = testdata[i].split("BODY=");
					dataMap.put("BODY", temp.length > 1 ? temp[1] : "");
				}
			}
			jsondata = dataMap.get("Verification");
			dataMap.remove("Verification");
		}

		try {
			Gson gson = new Gson();
			Map inspectionObj = gson.fromJson(jsondata, Map.class);
			logger.info("map:--" + inspectionObj);

			// put non json data in map
			Iterator datalocal = dataMap.entrySet().iterator();
			while (datalocal.hasNext()) {
				Map.Entry extradatamap = (Map.Entry) datalocal.next();
				inspectionObj.put(extradatamap.getKey(), extradatamap.getValue());
			}
			return inspectionObj;
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
			logger.fail("Exception in stringToJsonConverter For test Data " + jsonData + "\n" + e);
		}
		return null;
	}

	/**
	 * @Description: This method returns "value" from the "Test Data sheet" based on
	 *               the Key passed.
	 * @return String.
	 * @param Map ,String.
	 */

	public static String getTestDataStringValue(Map testDataHere, String keyName) {
		try {
			String ret = testDataHere.get(keyName).toString();
			return ret;
		} catch (Exception e) {
			logger.info(keyName + "is not present in test data = " + testDataHere);
			// return "false";
		}
		return "Key Not Found Error";
	}

	/*
	 * Trim Double data to 3 decimal to solve comparison problem like 3.000 & 3.0
	 */
	public static Map<String, String> convertDoubleValues(Map<String, String> originalMapData) {

		Map<String, String> finalReturnMap = new HashMap<String, String>();
		Iterator it = originalMapData.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry originalData = (Map.Entry) it.next();

			DecimalFormat df = new DecimalFormat("#.###");
			df.setRoundingMode(RoundingMode.CEILING);

			try {
				if (originalData.getKey().equals("referredDate")) {
					finalReturnMap.put(String.valueOf(originalData.getKey()),
							dateStringToLong(String.valueOf(originalData.getValue())));
				} else {
					Double x = Double.valueOf(String.valueOf(originalData.getValue()));
					finalReturnMap.put(String.valueOf(originalData.getKey()), String.valueOf(x));
				}
			} catch (NumberFormatException e) {
				finalReturnMap.put(String.valueOf(originalData.getKey()), String.valueOf(originalData.getValue()));
			}
		}
		return finalReturnMap;
	}

	public static int randomNumber(int min, int max) {
		Random rn = new Random();
		int range = max - min + 1;
		return rn.nextInt(range) + min;

	}

	/* Convert generic MAP data to Map<String, String> type */

	public static Map<String, String> convertObjMapToStringMap(Map temp) {
		Map<String, String> retTemp = new HashMap<String, String>();
		Iterator it = temp.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry expectedData = (Map.Entry) it.next();
			retTemp.put(String.valueOf(expectedData.getKey()), String.valueOf(expectedData.getValue()));
		}
		return retTemp;
	}

	public static String dateStringToLong(String originalDate) {
		try {
			// String today = "2016-11-18 16:39:17.0";

			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			Date date = formatter.parse(originalDate);
			long dateInLong = date.getTime();

			return String.valueOf(Double.valueOf(dateInLong));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return originalDate;
	}

	public static Map<String, String> convertKeystoLowerCase(Map<String, String> inputMap) {

		Map<String, String> resultMap = new HashMap<String, String>();

		Set<String> x1 = inputMap.keySet();
		for (Iterator iterator = x1.iterator(); iterator.hasNext();) {

			String key = (String) iterator.next();
			resultMap.put(key.toLowerCase(), String.valueOf(inputMap.get(key)));
		}

		return resultMap;
	}

	public String getUserIdFromExcelData(String data) {
		String userId = new String("");
		try {
			JSONObject createObj = new JSONObject(data);

			/*
			 * For entering the wrong userIds which cannot be found from the database and
			 * are supposed to be validations
			 */
			userId = createObj.getString("userId");

			if (createObj.has("DBQuery")) {
				JSONObject json = new JSONObject(data);
				String dbQuery = json.getString("DBQuery");
				userId = db.executeQueryString(dbQuery);
			}
			logger.info("UserId for the given test is : " + userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userId;
	}

	public String getHttpRequestType(String data) {

		String requestType = new String("");
		try {
			JSONObject json = new JSONObject(data);
			requestType = json.getString("method");
		} catch (Exception e) {
			e.printStackTrace();
			logger.fail("Exception caught in getting http request type.");
		}
		logger.info("Request type for the given test is : " + requestType);
		return requestType;
	}

	public String appendUserIdInTheUrl(String data, String userId) {

		String url = new String("");
		try {
			JSONObject json = new JSONObject(data);
			url = json.getString("url") + userId;
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Url request for the given test is : " + url);
		return url;
	}

	public String getRequestBody(String data) {

		JSONObject json = new JSONObject(data);
		JSONObject obj = json.getJSONObject("body");
		data = obj.toString();
		logger.info("Http Request body : " + obj.toString());
		return data;
	}

	public int getActualStatusCode(HTTPResponse response) {
		int statusCode = ((HTTPResponse) response).getStatusCode();
		logger.info("Actual Status Code is : " + statusCode);
		return statusCode;
	}

	public HTTPResponse getHttpResponse(String data, HTTPParams param) {
		JSONObject json = new JSONObject(data);
		String requestBody = new String("");

		String url = json.getString("url");
		String methodType = getHttpRequestType(data);

		// HTTP REQUEST'S REQUIRED PARAMS
		HTTPHeaders headers = new HTTPHeaders();
		HTTPResponse response = null;
		HTTPRequest requestSpec = null;
		headers.addHeader("Content-Type", "application/json");

		try {
			switch (methodType) {

			case "GET":
				requestSpec = new HTTPRequest(headers, param);
				logger.info("Requested url is : " + url);
				response = rc.sendRequest(HTTPMethod.GET, url, requestSpec);
				logger.info("Response : " + response.getBody().getBodyText().toString());
				break;

			case "POST":
				requestBody = getRequestBody(data);
				requestSpec = new HTTPRequest(headers, (Object) requestBody);
				response = rc.sendRequest(HTTPMethod.POST, url, requestSpec);
				break;

			case "PUT":
				requestBody = getRequestBody(data);
				requestSpec = new HTTPRequest(headers, (Object) requestBody);
				response = rc.sendRequest(HTTPMethod.PUT, url, requestSpec);
				break;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (response != null)
			logger.pass("Response message recieved : " + response.getBody().getBodyText().toString());

		return response;
	}

	public int getExpectedStatusCode(String data) {

		JSONObject json = new JSONObject(data);
		int statusCode = json.getInt("ExpectedStatusCode");
		logger.info("Expected status code is : " + statusCode);
		return statusCode;
	}

	public String getExpectedErrorMessageJson(String data) {
		JSONObject json = new JSONObject(data);
		String errorMessage = json.getJSONObject("ExpectedErrorMessage").toString();
		logger.info("Expected error message is : " + errorMessage);
		return errorMessage;
	}

	public String getExpectedResponseMessage(String data) {
		JSONObject json = new JSONObject(data);
		String responseMessage = json.getJSONObject("ExpectedResponse").toString();
		logger.info("Expected response message is : " + responseMessage);
		return responseMessage;
	}

	public String getExpectedErrorMessageString(String data) {
		JSONObject json = new JSONObject(data);
		String errorMessage = json.getString("ExpectedErrorMessage");
		logger.info("Expected error message is : " + errorMessage);
		return errorMessage;
	}

	private String getRandomStrings(int length) {
		String randomString = RandomStringUtils.random(length, true, true);
		return randomString;
	}

	public int getValidYob() {
		int yob = random.nextInt((2000 - 1940) + 1) + 1940;
		return yob;
	}

	public void compareHashMaps(Map<Object, Object> responseValueMap, Map<Object, Object> dbValuesMap) {
		Iterator<?> it = responseValueMap.entrySet().iterator();
		dbValuesMap.entrySet().iterator();
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			Entry<Object, Object> entry = (Entry<Object, Object>) it.next();
			if (dbValuesMap.containsKey(entry.getKey()) && responseValueMap.containsKey(entry.getKey())) {

				String key = entry.getKey().toString();
				String responseValue = responseValueMap.get(key).toString();
				String dbValue = dbValuesMap.get(key).toString();

				List<String> updatedValues = checkAndUpdateValuesAccordingly(responseValue, dbValue);

				responseValue = updatedValues.get(0);
				dbValue = updatedValues.get(1);

				if (responseValue.equals(dbValue)) {
					logger.pass(key + " is " + responseValue + " in response and " + dbValue + " in db.");
				} else {
					logger.fail(key + " is " + responseValue + " in response and " + dbValue + " in db.");
				}

			}
		}
	}

	private List<String> checkAndUpdateValuesAccordingly(String responseValue, String dbValue) {
		if (responseValue.contains(".0"))
			responseValue = Util.removeDoubleValue(responseValue);
		if (dbValue.contains(".0"))
			dbValue = Util.removeDoubleValue(dbValue);

		if (responseValue.equals("false"))
			responseValue = "0";
		if (responseValue.equals("true"))
			responseValue = "1";

		if (dbValue.equals("false"))
			dbValue = "0";
		if (dbValue.equals("true"))
			dbValue = "1";

		List<String> list = new ArrayList<String>();
		list.add(responseValue);
		list.add(dbValue);

		return list;
	}

	public static String removeDoubleValue(String keyValue) {
		try {
			Double doubleVal = Double.parseDouble(keyValue);
			return Integer.toString(doubleVal.intValue());
		} catch (Exception e) {
			return "false";
		}
	}

	public String getReasonCode(String expectedOrActual, String responnse) {
		JSONObject json = new JSONObject(responnse);
		int a = json.getInt("reasonCode");
		String reasonCode = String.valueOf(a);
		logger.info(expectedOrActual + " reasonCode is : + " + reasonCode);
		return reasonCode;
	}

	public void assertOnStatusCode(int expectedStatusCode, int actualStatusCode) {

		if (expectedStatusCode == actualStatusCode) {
			logger.pass("Expected status code(" + expectedStatusCode + ") and actual status code(" + actualStatusCode
					+ ") is same.");
		} else {
			logger.fail("Expected status code(" + expectedStatusCode + ") and actual status code(" + actualStatusCode
					+ ") is same.");
		}
	}

	public void assertOnErrorMessage(String expectedErrorMessage, String actualErrorMessage) {

		if (expectedErrorMessage.equalsIgnoreCase(actualErrorMessage)) {
			logger.pass("Expected error message and actual error message are same.");
		} else {
			logger.fail("Expected error message and actual error message are same.");
		}
	}

	public void checkAndAssertIfErrorMessageIsPresent(String testData, HTTPResponse response) {
		JSONObject requestJson = new JSONObject(testData);
		JSONObject responseJson = new JSONObject(response.getBody().getBodyText().toString());

		if (requestJson.has("ExpectedErrorMessage")) {
			// Getting expected response message
			String expectedErrorMessage = getExpectedErrorMessageString(testData);

			String actualErrorMessage = responseJson.getString("ErrorMessage");
			// Comparing expected and actual error message
			assertOnErrorMessage(expectedErrorMessage, actualErrorMessage);
		}
	}

	public int getRandomNumberWithBaseValue(int base) {
		int randomNumber = random.nextInt(base) + base;
		return randomNumber;
	}

	public void updatePlayerBalannceAsZeroInAllTables(String userId) {
		try {
			db.executeUpdate("update player_account set deposit = 0  where user_id = " + userId);
			db.executeUpdate("update player_account set withdrawable = 0 where user_id = " + userId);

			db.executeUpdate("update player_transaction set amount = 0  where user_id = " + userId);
			db.executeUpdate("update player_transaction set updated_withdrawal_bal = 0 where user_id = " + userId);

			db.executeUpdate("update cash_accounts set deposit = 0  where user_id = " + userId);
			db.executeUpdate("update cash_accounts set withdrawable = 0 where user_id = " + userId);
			logger.pass("Successfuly updated player_account/player_transaction/cash_accounts with amount as 0Rs.");
		} catch (SQLException e) {
			logger.fail("Failed to update player_account/player_transaction/cash_accounts with amount as 0Rs.");
			e.printStackTrace();
		}
	}

	public void assertErrorMessageAndStatusCode(String testData, HTTPResponse response) {
		JSONObject data = new JSONObject(testData);
		String ExpectedErrorMessage = null;
		int actualStatusCode = response.getStatusCode();
		int expectedStatusCode = getExpectedStatusCode(testData);
		Assert.assertEquals(expectedStatusCode, actualStatusCode);
		logger.info("Actual status code is : " + actualStatusCode);

		if (data.has("ExpectedErrorMessage")) {
			String ActualErrorMessage = response.getBody().getBodyText().toString();
			logger.info("Actual error message is : " + ActualErrorMessage);
			ExpectedErrorMessage = getExpectedErrorMessageJson(testData);

			boolean result = JSONComparator.comparator(ActualErrorMessage, ExpectedErrorMessage);

			if (result == true)
				logger.pass("Expected and actual error messages are same.");
			else
				logger.fail("Expected and actual error messages are same.");
		}

	}

	public void dbVerificationForPostAddCash(String userId, double amountInRequest) {
		double amount1 = 0, amount2 = 0, amount3 = 0;
		int txnId = 0, txnType = 0;
		String query1 = "select deposit from player_account where user_id = " + userId + " order by id desc limit 1";
		String query2 = "select amount from player_transaction where user_id = " + userId + " order by id desc limit 1";
		String query3 = "select deposit from cash_accounts where user_id = " + userId
				+ " order by account_id desc limit 1";
		String query4 = "select txn_id from player_transaction where user_id = " + userId + " order by id desc limit 1";
		try {
			amount1 = db.executeQueryDouble(query1);
			amount2 = db.executeQueryDouble(query2);
			amount3 = db.executeQueryDouble(query3);
			txnId = db.executeQueryInt(query4);
			txnType = db.executeQueryInt("select txn_id from global_transaction where id =  " + txnId);
		} catch (SQLException e) {
			logger.fail("Failed to get deposit amount from player_account/player_transaction/cash_accounts");
			e.printStackTrace();
		}
		if (amount1 == amount2 && amount2 == amount3 && amount3 == amountInRequest)
			logger.pass("Amount is " + amount1 + " in player_account and " + amount2 + " in player_transaction and "
					+ amount3 + " in cash_accounts");
		else
			logger.fail("Amount is " + amount1 + " in player_account and " + amount2 + " in player_transaction and "
					+ amount3 + " in cash_accounts");
		if (txnType == 1)
			logger.pass("TxnId is " + txnId + " in global transaction table and txnType is " + txnType
					+ " in txn_type table i.e. AddCash");
		else
			logger.fail("TxnId is " + txnId + " in global transaction table and txnType is " + txnType
					+ " in txn_type table i.e. AddCash");
	}

	public void AssertOnResponse(String testData, HTTPResponse response) {
		JSONObject data = new JSONObject(testData);
		String ExpectedResponse = null;
		int actualStatusCode = response.getStatusCode();
		int expectedStatusCode = getExpectedStatusCode(testData);

		if (data.has("ExpectedResponse")) {
			String ActualResponse = response.getBody().getBodyText().toString();
			ExpectedResponse = getExpectedResponseMessage(testData);
			logger.info("Actual response is : " + ActualResponse);
			Assert.assertEquals(ExpectedResponse, ActualResponse);
		}

		logger.info("Actual status code is : " + actualStatusCode);
		Assert.assertEquals(expectedStatusCode, actualStatusCode);
	}

	public void dbVerificationForKycAutomation(String testData, HTTPResponse response, String userId) {
		try {} catch (Exception e) {
			e.printStackTrace();
			logger.fail("Failed to put db assert in class : " + this.getClass().getName());
		}
		try {
			String query1 = "select * user_kyc_data from  where userId = " + userId;
			String query2 = "select * id_kyc_status from  where userId = " + userId;
			String query3 = "select * user_upload_log from  where userId = " + userId;

			Map<Object, Object> map1 = new HashMap<Object, Object>();
			Map<Object, Object> map2 = new HashMap<Object, Object>();
			Map<Object, Object> map3 = new HashMap<Object, Object>();
			Map<Object, Object> map4 = new HashMap<Object, Object>();
			Map<Object, Object> responseMap = new HashMap<Object, Object>();

			responseMap = jsonToMapConverter(response.getBody().getBodyText().toString());

			map1 = db.executeQueryAllFieldAndValue(query1);
			map2 = db.executeQueryAllFieldAndValue(query2);
			map3 = db.executeQueryAllFieldAndValue(query3);

			compareHashMaps(responseMap, map1);
			compareHashMaps(responseMap, map2);
			compareHashMaps(responseMap, map3);
			compareHashMaps(responseMap, map4);

		} catch (Exception e) {
			logger.fail("Failed to assert on db values");
			e.printStackTrace();
		}
	}
}