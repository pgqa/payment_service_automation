package com.rummycircle.addcash;

public class GetUserLimitsResponse {

	private String grossDepositLimit;
	private String cumulativeAddedCash;
	private String currentLimit;
	private String dailyLimit;

	public String getGrossDepositLimit() {
		return grossDepositLimit;
	}

	public void setGrossDepositLimit(String grossDepositLimit) {
		this.grossDepositLimit = grossDepositLimit;
	}

	public String getCumulativeAddedCash() {
		return cumulativeAddedCash;
	}

	public void setCumulativeAddedCash(String cumulativeAddedCash) {
		this.cumulativeAddedCash = cumulativeAddedCash;
	}

	public String getCurrentLimit() {
		return currentLimit;
	}

	public void setCurrentLimit(String currentLimit) {
		this.currentLimit = currentLimit;
	}

	public String getDailyLimit() {
		return dailyLimit;
	}

	public void setDailyLimit(String dailyLimit) {
		this.dailyLimit = dailyLimit;
	}
}
