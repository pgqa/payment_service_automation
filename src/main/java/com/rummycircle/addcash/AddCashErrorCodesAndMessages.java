package com.rummycircle.addcash;

public interface AddCashErrorCodesAndMessages {

	/*
	 * Created an interface for all the different set of error codes and error
	 * messages.At present only template is ready
	 * 
	 * TODO : The developer has not developed the code to handle negative
	 * scenarios hence this is not finished yet.Once they will update us with
	 * error codes and their respective messages for negative scenarios then
	 * will update this interface accordingly.
	 */

	// Generic Error Codes
	public static final String ERR_CODE_INVALID_USER = "";

	// Generic Error Messages
	public static final String ERR_MSG_INVALID_USER = "";
}
