package com.rummycircle.addcash;

public class GetPANDetailsResponse {

	private String mobile;
	private String strongPwd;
	private String state;
	private String email;
	private String loginid;
	private String _synchronizerToken;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getStrongPwd() {
		return strongPwd;
	}

	public void setStrongPwd(String strongPwd) {
		this.strongPwd = strongPwd;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLoginid() {
		return loginid;
	}

	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}

	public String get_synchronizerToken() {
		return _synchronizerToken;
	}

	public void set_synchronizerToken(String _synchronizerToken) {
		this._synchronizerToken = _synchronizerToken;
	}
}
