package com.rummycircle.addcash;

public class GetChangeLimitDataResponse {

	private String limitHit48hr;
	private String firstTxnDateInLast48Hours;
	private String last48HoursLimitChangedDate;
	private String maxLimit;
	private String cashAddedThisMonth;
	private String monthlyAddCashLimit;
	private String linkEnableOrNot;
	private String whichLimit;

	public String getLimitHit48hr() {
		return limitHit48hr;
	}

	public void setLimitHit48hr(String limitHit48hr) {
		this.limitHit48hr = limitHit48hr;
	}

	public String getFirstTxnDateInLast48Hours() {
		return firstTxnDateInLast48Hours;
	}

	public void setFirstTxnDateInLast48Hours(String firstTxnDateInLast48Hours) {
		this.firstTxnDateInLast48Hours = firstTxnDateInLast48Hours;
	}

	public String getLast48HoursLimitChangedDate() {
		return last48HoursLimitChangedDate;
	}

	public void setLast48HoursLimitChangedDate(String last48HoursLimitChangedDate) {
		this.last48HoursLimitChangedDate = last48HoursLimitChangedDate;
	}

	public String getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(String maxLimit) {
		this.maxLimit = maxLimit;
	}

	public String getCashAddedThisMonth() {
		return cashAddedThisMonth;
	}

	public void setCashAddedThisMonth(String cashAddedThisMonth) {
		this.cashAddedThisMonth = cashAddedThisMonth;
	}

	public String getMonthlyAddCashLimit() {
		return monthlyAddCashLimit;
	}

	public void setMonthlyAddCashLimit(String monthlyAddCashLimit) {
		this.monthlyAddCashLimit = monthlyAddCashLimit;
	}

	public String getLinkEnableOrNot() {
		return linkEnableOrNot;
	}

	public void setLinkEnableOrNot(String linkEnableOrNot) {
		this.linkEnableOrNot = linkEnableOrNot;
	}

	public String getWhichLimit() {
		return whichLimit;
	}

	public void setWhichLimit(String whichLimit) {
		this.whichLimit = whichLimit;
	}

}
