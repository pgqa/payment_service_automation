package com.rummycircle;

public enum LocalConfigProperties {

	FUSION_PROXY_PROTOCOL("fusionProxy.protocol"), FUSION_PROXY_HOST("fusionProxy.host"), FUSION_PROXY_PORT(
			"fusionProxy.port"),FUSION_PROXY_MOBILE_HOST("fusionProxy.host.mobile"),LOGIN_PROTOCOL("login.protocol"),LOGIN_HOST("login.host"),LOGIN_PORT("login.port"),
	CREATECAPTCHA_PROTOCOL("createCaptcha.protocol"),CREATECAPTCHA_HOST("createCaptcha.host"),CREATECAPTCHA_PORT("createCaptcha.port"),FUSIONPROXYSSETUP_PROTOCOL("fusionProxySetup.protocol"),FUSIONPROXYSETUP_HOST("fusionProxySetup.host"),
	REDIS_IP("redis.ip");

	private String value;

	LocalConfigProperties(String key) {
		this.value = key;
	}

	public String getValue() {
		return value;
	}

}
