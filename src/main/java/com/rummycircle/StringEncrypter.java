package com.rummycircle;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The following example implements a class for encrypting and decrypting
 * strings using several Cipher algorithms. The class is created with a key and
 * can be used repeatedly to encrypt and decrypt strings using that key. Some of
 * the more popular algorithms are: Blowfish DES DESede PBEWithMD5AndDES
 * PBEWithMD5AndTripleDES TripleDES
 */

public class StringEncrypter {
	private static Logger logger = LoggerFactory.getLogger(StringEncrypter.class);
	Cipher ecipher;
	Cipher dcipher;

	/**
	 * Constructor used to create this object. Responsible for setting and
	 * initializing this object's encrypter and decrypter Chipher instances
	 * given a Secret Key and algorithm.
	 * 
	 * @param key
	 *            Secret Key used to initialize both the encrypter and decrypter
	 *            instances.
	 * @param algorithm
	 *            Which algorithm to use for creating the encrypter and
	 *            decrypter instances.
	 */
	public StringEncrypter(SecretKey key, String algorithm) {
		try {
			ecipher = Cipher.getInstance(algorithm);
			dcipher = Cipher.getInstance(algorithm);
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			dcipher.init(Cipher.DECRYPT_MODE, key);
		} catch (NoSuchPaddingException e) {
			logger.trace("EXCEPTION: NoSuchPaddingException");
		} catch (NoSuchAlgorithmException e) {
			logger.trace("EXCEPTION: NoSuchAlgorithmException");
		} catch (InvalidKeyException e) {
			logger.trace("EXCEPTION: InvalidKeyException");
		}
	}

	/**
	 * Constructor used to create this object. Responsible for setting and
	 * initializing this object's encrypter and decrypter Chipher instances
	 * given a Pass Phrase and algorithm.
	 * 
	 * @param passPhrase
	 *            Pass Phrase used to initialize both the encrypter and
	 *            decrypter instances.
	 */
	public StringEncrypter(String passPhrase) {

		// 8-bytes Salt
		byte[] salt = { (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32, (byte) 0x56, (byte) 0x34, (byte) 0xE3,
				(byte) 0x03 };

		// Iteration count
		int iterationCount = 19;

		try {

			KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
			SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

			ecipher = Cipher.getInstance(key.getAlgorithm());
			dcipher = Cipher.getInstance(key.getAlgorithm());

			// Prepare the parameters to the cipthers
			AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

			ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
			dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

		} catch (InvalidAlgorithmParameterException e) {
			logger.trace("EXCEPTION: InvalidAlgorithmParameterException");
		} catch (InvalidKeySpecException e) {
			logger.trace("EXCEPTION: InvalidKeySpecException");
		} catch (NoSuchPaddingException e) {
			logger.trace("EXCEPTION: NoSuchPaddingException");
		} catch (NoSuchAlgorithmException e) {
			logger.trace("EXCEPTION: NoSuchAlgorithmException");
		} catch (InvalidKeyException e) {
			logger.trace("EXCEPTION: InvalidKeyException");
		}
	}

	/**
	 * Takes a single String as an argument and returns an Encrypted version of
	 * that String.
	 * 
	 * @param str
	 *            String to be encrypted
	 * @return <code>String</code> Encrypted version of the provided String
	 */
	public String encrypt(String str) {
		try {
			// Encode the string into bytes using utf-8
			byte[] utf8 = str.getBytes("UTF8");

			// Encrypt
			byte[] enc = ecipher.doFinal(utf8);

			// Encode bytes to base64 to get a string
			return new String(Base64.getEncoder().encode(enc));

		} catch (BadPaddingException e) {
		} catch (IllegalBlockSizeException e) {
		} catch (UnsupportedEncodingException e) {
		}
		return null;
	}

	/**
	 * Takes a encrypted String as an argument, decrypts and returns the
	 * decrypted String.
	 * 
	 * @param str
	 *            Encrypted String to be decrypted
	 * @return <code>String</code> Decrypted version of the provided String
	 */
	public String decrypt(String str) {

		try {

			// Decode base64 to get bytes
			byte[] dec = Base64.getDecoder().decode(str);

			// Decrypt
			byte[] utf8 = dcipher.doFinal(dec);

			// Decode using utf-8
			return new String(utf8, "UTF8");

		} catch (BadPaddingException e) {
		} catch (IllegalBlockSizeException e) {
		} catch (UnsupportedEncodingException e) {
		}
		return null;
	}

	/**
	 * The following method is used for testing the String Encrypter class. This
	 * method is responsible for encrypting and decrypting a sample String using
	 * several symmetric temporary Secret Keys.
	 */
	public static void testUsingSecretKey() {
		try {
			logger.trace("+----------------------------------------+");
			logger.trace("|  -- Test Using Secret Key Method --    |");
			logger.trace("+----------------------------------------+");

			String secretString = "Attack at dawn!";

			// Generate a temporary key for this example. In practice, you would
			// save this key somewhere. Keep in mind that you can also use a
			// Pass Phrase.
			SecretKey desKey = KeyGenerator.getInstance("DES").generateKey();
			SecretKey blowfishKey = KeyGenerator.getInstance("Blowfish").generateKey();
			SecretKey desedeKey = KeyGenerator.getInstance("DESede").generateKey();

			// Create encrypter/decrypter class
			StringEncrypter desEncrypter = new StringEncrypter(desKey, desKey.getAlgorithm());
			StringEncrypter blowfishEncrypter = new StringEncrypter(blowfishKey, blowfishKey.getAlgorithm());
			StringEncrypter desedeEncrypter = new StringEncrypter(desedeKey, desedeKey.getAlgorithm());

			// Encrypt the string
			String desEncrypted = desEncrypter.encrypt(secretString);
			String blowfishEncrypted = blowfishEncrypter.encrypt(secretString);
			String desedeEncrypted = desedeEncrypter.encrypt(secretString);

			// Decrypt the string
			String desDecrypted = desEncrypter.decrypt(desEncrypted);
			String blowfishDecrypted = blowfishEncrypter.decrypt(blowfishEncrypted);
			String desedeDecrypted = desedeEncrypter.decrypt(desedeEncrypted);

			// Print out values
			logger.trace(desKey.getAlgorithm() + " Encryption algorithm");
			logger.trace("    Original String  : " + secretString);
			logger.trace("    Encrypted String : " + desEncrypted);
			logger.trace("    Decrypted String : " + desDecrypted);

			logger.trace(blowfishKey.getAlgorithm() + " Encryption algorithm");
			logger.trace("    Original String  : " + secretString);
			logger.trace("    Encrypted String : " + blowfishEncrypted);
			logger.trace("    Decrypted String : " + blowfishDecrypted);

			logger.trace(desedeKey.getAlgorithm() + " Encryption algorithm");
			logger.trace("    Original String  : " + secretString);
			logger.trace("    Encrypted String : " + desedeEncrypted);
			logger.trace("    Decrypted String : " + desedeDecrypted);

		} catch (NoSuchAlgorithmException e) {
		}
	}

	/**
	 * The following method is used for testing the String Encrypter class. This
	 * method is responsible for encrypting and decrypting a sample String using
	 * using a Pass Phrase.
	 */
	public static void testUsingPassPhrase() {

		logger.trace("+----------------------------------------+");
		logger.trace("|  -- Test Using Pass Phrase Method --   |");
		logger.trace("+----------------------------------------+");

		String secretString = "Attack at dawn!";
		String passPhrase = "My Pass Phrase";

		// Create encrypter/decrypter class
		StringEncrypter desEncrypter = new StringEncrypter(passPhrase);

		// Encrypt the string
		String desEncrypted = desEncrypter.encrypt(secretString);

		// Decrypt the string
		String desDecrypted = desEncrypter.decrypt(desEncrypted);

		// Print out values
		logger.trace("PBEWithMD5AndDES Encryption algorithm");
		logger.trace("    Original String  : " + secretString);
		logger.trace("    Encrypted String : " + desEncrypted);
		logger.trace("    Decrypted String : " + desDecrypted);

	}

	public static String replaceSpecial(String str) {
		str = str.replace("&", "abcdefghij1");
		str = str.replace("+", "abcdefghij2");
		str = str.replace("/", "abcdefghij3");
		str = str.replace("%", "abcdefghij4");
		str = str.replace("<", "abcdefghij5");
		str = str.replace(">", "abcdefghij6");
		str = str.replace("\"\"", "abcdefghij7");
		str = str.replace("=", "abcdefghij8");
		str = str.replace("`", "abcdefghij9");
		str = str.replace("~", "abcdefghij1k0");
		str = str.replace("!", "abcdefghij1k1");
		str = str.replace("@", "abcdefghij1k2");
		str = str.replace("#", "abcdefghij1k3");
		str = str.replace("$", "abcdefghij1k4");
		str = str.replace("^", "abcdefghij1k6");
		str = str.replace("*", "abcdefghij1k7");
		str = str.replace("(", "abcdefghij1k8");
		str = str.replace(")", "abcdefghij1k9");
		str = str.replace("-", "abcdefghij2k0");
		str = str.replace("_", "abcdefghij2k1");
		str = str.replace("{", "abcdefghij2k2");
		str = str.replace("}", "abcdefghij2k3");
		str = str.replace("[", "abcdefghij2k4");
		str = str.replace("]", "abcdefghij2k5");
		str = str.replace("|", "abcdefghij2k6");
		str = str.replace(":", "abcdefghij2k7");
		str = str.replace(";", "abcdefghij2k8");
		str = str.replace(",", "abcdefghij2k9");
		str = str.replace(".", "abcdefghij3k0");

		return str;
	}

	public static String replaceSpecialAgain(String str) {
		str = str.replace("abcdefghij1", "&");
		str = str.replace("abcdefghij2", "+");
		str = str.replace("abcdefghij3", "/");
		str = str.replace("abcdefghij4", "%");
		str = str.replace("abcdefghij5", "<");
		str = str.replace("abcdefghij6", ">");
		str = str.replace("abcdefghij7", "\"\"");
		str = str.replace("abcdefghij8", "=");

		str = str.replace("abcdefghij9", "`");
		str = str.replace("abcdefghij1k0", "~");
		str = str.replace("abcdefghij1k1", "!");
		str = str.replace("abcdefghij1k2", "@");
		str = str.replace("abcdefghij1k3", "#");
		str = str.replace("abcdefghij1k4", "$");
		str = str.replace("abcdefghij1k6", "^");
		str = str.replace("abcdefghij1k7", "*");
		str = str.replace("abcdefghij1k8", "(");
		str = str.replace("abcdefghij1k9", ")");
		str = str.replace("abcdefghij2k0", "-");
		str = str.replace("abcdefghij2k1", "_");
		str = str.replace("abcdefghij2k2", "{");
		str = str.replace("abcdefghij2k3", "}");
		str = str.replace("abcdefghij2k4", "[");
		str = str.replace("abcdefghij2k5", "]");
		str = str.replace("abcdefghij2k6", "|");
		str = str.replace("abcdefghij2k7", ":");
		str = str.replace("abcdefghij2k8", ";");
		str = str.replace("abcdefghij2k9", ",");
		str = str.replace("abcdefghij3k0", ".");
		return str;
	}

	/**
	 * Sole entry point to the class and application used for testing the String
	 * Encrypter class.
	 * 
	 * @param args
	 *            Array of String arguments.
	 */
	public static void main(String[] args) {
		testUsingSecretKey();
		testUsingPassPhrase();
	}

}