package com.rummycircle;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.rummycircle.utils.reporting.extent.Logger;

public class JSONUtil {
	public static List<String> keyValueList = new ArrayList<>();
	public static final String REPLACE_DOUBLE_QUOTES_FROM_STARTANDEND = "^\"(.*)\"$";
	public static Logger LOGS = Logger.getLogger();
	public static final String SEMICOlON_SEPARATOR = "\\s*;\\s*";
	private static final Configuration configuration = Configuration.builder()
			.jsonProvider(new JacksonJsonNodeJsonProvider()).mappingProvider(new JacksonMappingProvider()).build();
	private static List<String> pathList = new ArrayList<>();

	public static String pathType(String path) {

		if (!path.contains(".") && !isDigit(path)) {
			return "key";
		} else {
			return "path";
		}
	}

	// index to do testing
	public static boolean isDigit(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (Exception e) {
			// LOGS.debug("ERROR:" + e.getMessage());
			return false;
		}
	}

	public static boolean verifyValueByIndex(String inputJson, int index, String value) {
		boolean flag = false;
		Map<String, String> val = JsonPath.read(inputJson, "$.[" + index + "]");
		for (String s : val.values()) {
			if (s.equals(value)) {
				flag = true;
				break;
			}
		}

		return flag;

	}

	public static boolean verifyKeypathValue(String input, String path, String fieldNewValue) throws Exception {
		String pathType = pathType(path);
		boolean flag = false;
		try {
			switch (pathType) {
			case "path":
				if (null != JsonPath.read(input, path) && JsonPath.read(input, path).toString().equals(fieldNewValue)) {
					flag = true;
				}
				break;
			case "index":
				int index = Integer.parseInt(path);
				Map<String, String> val = JsonPath.read(input, "$.[" + index + "]");
				if (verifyValueByIndex(input, index, fieldNewValue)) {
					flag = true;
				}
				break;
			case "key":
				if (findByKeyValue(input, path, fieldNewValue)) {
					flag = true;
				}
				break;

			default:
			}

			return flag;
		} catch (Exception e) {
			LOGS.error("ERROR:" + e.getMessage());
			return false;
		}

	}

	public static boolean findByKeyValue(String inputJson, String fieldName, String fieldValue) throws IOException {
		Boolean searchFlag = false;
		try {
			inputJson = jsonArrayTojson(inputJson);
			ObjectNode root = (ObjectNode) new ObjectMapper().readTree(inputJson);
			Iterator<Map.Entry<String, JsonNode>> fields = root.fields();
			int index = -1;
			String keyName = fieldName;
			if (fieldName.contains("[")) {
				index = Integer.parseInt(fieldName.substring(fieldName.indexOf('[') + 1, fieldName.indexOf(']')));
				keyName = fieldName.substring(0, fieldName.indexOf('['));

			}
			searchFlag = deepSearch(fields, keyName, fieldValue, false, index);

		} catch (FileNotFoundException e) {
			LOGS.error("ERROR:" + e.getMessage());
		}
		return searchFlag;

	}

	private static boolean deepSearch(Iterator<Map.Entry<String, JsonNode>> fields, String fieldParam,
			String fieldOldvalue, boolean previousFuncRet, int index) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode recNode = mapper.createObjectNode();
		Boolean searchFlag = false;
		if (previousFuncRet)
			searchFlag = true;
		while (fields.hasNext()) {
			Map.Entry<String, JsonNode> field = fields.next();
			recNode = field.getValue();
			String fieldName = field.getKey();
			String fieldValue = recNode.textValue();
			if (recNode.isNumber()) {
				Number n = recNode.numberValue();
				fieldValue = n.toString();
			} else if (recNode.isBoolean()) {
				Boolean b = recNode.booleanValue();
				fieldValue = b.toString();
			}
			if (fieldName.equals(fieldParam)) {
				if (null != fieldValue && fieldValue.equals(fieldOldvalue)) {
					searchFlag = true;
					return searchFlag;
				} else if (recNode.isArray() && (index != -1) && recNode.get(index).toString()
						.replaceAll(REPLACE_DOUBLE_QUOTES_FROM_STARTANDEND, "$1").equals(fieldOldvalue)) {
					searchFlag = true;
					return searchFlag;
				}

			} else {
				if (recNode.isObject()) {
					Iterator<Map.Entry<String, JsonNode>> derivedField = recNode.fields();

					searchFlag = deepSearch(derivedField, fieldParam, fieldOldvalue, searchFlag, index);
				} else if (recNode.isArray()) {
					Iterator<JsonNode> arrayItr = recNode.elements();
					while (arrayItr.hasNext()) {
						JsonNode arrayNode = arrayItr.next();
						Iterator<Map.Entry<String, JsonNode>> arrayField = arrayNode.fields();
						searchFlag = deepSearch(arrayField, fieldParam, fieldOldvalue, searchFlag, index);
					}
				}

			}
		}
		return searchFlag;
	}

	public static boolean checkKeyPathExist(String input, String path) throws Exception {

		try {
			boolean flag = false;
			String pathType = pathType(path);

			if ((pathType.equals("path") && JsonPath.read(input, path) != null)
					|| (pathType.equals("key") && isJSonKeyExist(input, path))) {

				flag = true;
			}
			return flag;

		} catch (PathNotFoundException e) {
			LOGS.error("ERROR:" + e.getMessage());
			return false;
		}
	}

	public static boolean isJSonKeyExist(String inputJson, String key) throws IOException {

		Boolean searchFlag = false;
		try {
			inputJson = jsonArrayTojson(inputJson);
			ObjectNode root = (ObjectNode) new ObjectMapper().readTree(inputJson);
			Iterator<Map.Entry<String, JsonNode>> fields = root.fields();

			searchFlag = deepSearchKeyExist(fields, key, false);

		} catch (FileNotFoundException e) {
			LOGS.error("ERROR:" + e.getMessage());
		}
		return searchFlag;

	}

	private static boolean deepSearchKeyExist(Iterator<Map.Entry<String, JsonNode>> fields, String fieldParam,
			boolean previousFuncRet) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode recNode = mapper.createObjectNode();
		Boolean searchFlag = false;
		if (previousFuncRet)
			searchFlag = true;
		while (fields.hasNext()) {
			Map.Entry<String, JsonNode> field = fields.next();
			recNode = field.getValue();
			String fieldName = field.getKey();

			if (fieldName.equals(fieldParam)) {
				searchFlag = true;
				return searchFlag;
			} else {
				if (recNode.isObject()) {
					Iterator<Map.Entry<String, JsonNode>> derivedField = recNode.fields();
					searchFlag = deepSearchKeyExist(derivedField, fieldParam, searchFlag);
				} else if (recNode.isArray()) {
					Iterator<JsonNode> arrayItr = recNode.elements();
					while (arrayItr.hasNext()) {
						JsonNode arrayNode = arrayItr.next();
						Iterator<Map.Entry<String, JsonNode>> arrayField = arrayNode.fields();
						searchFlag = deepSearchKeyExist(arrayField, fieldParam, searchFlag);
					}
				}

			}
		}
		return searchFlag;
	}

	public static String getKeyOrPathValues(String input, String path) {
		try {
			String pathType = pathType(path);
			String value = null;
			switch (pathType) {
			case "path":
				if (null != JsonPath.read(input, path)) {
					String datatype = getTheDataType(input, path);
					if (datatype != null && datatype.equals("MAP")) {
						JSONObject jsonobject = new JSONObject(JsonPath.read(input, path));
						value = jsonobject.toJSONString();
					} else {
						value = JsonPath.read(input, path).toString();
					}

				}
				break;
			case "key":
				getKeyValue(input, path);
				if (!keyValueList.isEmpty()) {
					value = keyValueList.toString();
				} else {
					System.out.println("empty list");
				}
				break;
			case "index":
				int keyindex = Integer.parseInt(path);
				value = getValueByIndex(input, keyindex);
				break;
			default:

			}
			return value;

		} catch (Exception e) {
			LOGS.error("ERROR:" + e.getMessage());
			return null;
		}
	}

	public static String getTheDataType(String input, String path) {
		try {
			String dataType = null;
			Object object = JsonPath.read(input, path);
			if (object != null) {
				if (object instanceof String) {
					dataType = "STRING";
				} else if (object instanceof Integer) {
					dataType = "INTEGER";
				} else if (object instanceof Float) {
					dataType = "Float";
				} else if (object instanceof Double) {
					dataType = "DOUBLE";
				} else if (object.toString().startsWith("[")) {
					dataType = "ARRAY";
				} else if (object.toString().startsWith("{")) {
					dataType = "MAP";
				}

			} else {
				dataType = "NULL";
			}
			return dataType;
		} catch (Exception e) {
			LOGS.error("ERROR:" + e.getMessage());
			return null;
		}

	}

	public static void getKeyValue(String inputJson, String fieldName) {

		try {
			keyValueList.clear();
			inputJson = jsonArrayTojson(inputJson);
			ObjectNode root = (ObjectNode) new ObjectMapper().readTree(inputJson);
			Iterator<Map.Entry<String, JsonNode>> fields = root.fields();
			int index = -1;
			String keyName = fieldName;
			if (fieldName.contains("[")) {
				index = Integer.parseInt(fieldName.substring(fieldName.indexOf('[') + 1, fieldName.indexOf(']')));
				keyName = fieldName.substring(0, fieldName.indexOf('['));

			}
			getVlaueBydeepSearch(fields, keyName, index);

		} catch (Exception e) {
			e.printStackTrace();
			// LOGS.error("ERROR:" + e.getMessage());
		}

	}

	private static void getVlaueBydeepSearch(Iterator<Map.Entry<String, JsonNode>> fields, String fieldParam, int index)
			throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode recNode = mapper.createObjectNode();
		while (fields.hasNext()) {
			Map.Entry<String, JsonNode> field = fields.next();
			recNode = field.getValue();
			String fieldName = field.getKey();

			if (fieldName.equals(fieldParam)) {

				if (recNode.isArray() && (index != -1)) {
					keyValueList.add(
							recNode.get(index).toString().replaceAll(REPLACE_DOUBLE_QUOTES_FROM_STARTANDEND, "$1"));
				} else {
					keyValueList.add(recNode.asText());
				}

			} else {
				if (recNode.isObject()) {
					Iterator<Map.Entry<String, JsonNode>> derivedField = recNode.fields();
					getVlaueBydeepSearch(derivedField, fieldParam, index);
				} else if (recNode.isArray()) {
					Iterator<JsonNode> arrayItr = recNode.elements();
					while (arrayItr.hasNext()) {
						JsonNode arrayNode = arrayItr.next();
						Iterator<Map.Entry<String, JsonNode>> arrayField = arrayNode.fields();
						getVlaueBydeepSearch(arrayField, fieldParam, index);
					}
				}

			}
		}
	}

	public static String getValueByIndex(String inputJson, int index) {
		Map<String, String> val = JsonPath.read(inputJson, "$.[" + index + "]");
		if (val != null) {

			JSONObject jsonobject = new JSONObject(val);
			return jsonobject.toJSONString();
		} else
			return null;
	}

	public static String jsonArrayTojson(String input) {

		if (input.startsWith("[") && input.endsWith("]")) {
			input = "{\"dummy\":" + input + "}";
		}
		return input;

	}

	public static int getSizeJsonArray(String input, String path) {
		int count = -1;
		try {
			if (null != JsonPath.read(input, path)) {
				String value = JsonPath.read(input, path).toString();
				JSONParser parser = new JSONParser();
				JSONArray json = (JSONArray) parser.parse(value);
				count = json.size();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return count;
	}

	public static boolean verifyMatchInJSON(JSONObject responseJson, String keyToMatch, String valueToMatch) {
		String value = JSONUtil.getKeyOrPathValues(responseJson.toJSONString(), keyToMatch);
		if (value != null && value.contains(valueToMatch))
			return true;
		else
			return false;
	}

	public static int verifyMatchInJSON(JSONArray responseJsonArray, String keyToMatch, String valueToMatch) {
		for (int i = 0; i < responseJsonArray.size(); i++) {
			if (responseJsonArray.get(i) instanceof JSONObject) {
				if (verifyMatchInJSON((JSONObject) responseJsonArray.get(i), keyToMatch, valueToMatch)) {
					return i;
				}
			} else if (responseJsonArray.get(i) instanceof JSONArray)
				return verifyMatchInJSON((JSONArray) responseJsonArray.get(i), keyToMatch, valueToMatch);
		}
		return -1;
	}

	public static String replaceNodeValues(String input, String nodeSequence, String fieldNewValue) {
		try {
			String newJsonStirng = input;
			String[] aryNodeSequence = nodeSequence.split(SEMICOlON_SEPARATOR);
			String[] aryFieldValue = fieldNewValue.split(SEMICOlON_SEPARATOR);
			if (aryNodeSequence.length == aryFieldValue.length || aryFieldValue.length == 1) {
				boolean onlyOneValue = Boolean.FALSE;
				if (aryFieldValue.length == 1)
					onlyOneValue = Boolean.TRUE;
				for (int i = 0; i < aryNodeSequence.length; i++) {

					String pathType = pathType(aryNodeSequence[i]);
					switch (pathType) {
					case "path":
						if (onlyOneValue) {
							newJsonStirng = replaceByPath(newJsonStirng, aryNodeSequence[i], aryFieldValue[0]);
						} else {
							newJsonStirng = replaceByPath(newJsonStirng, aryNodeSequence[i], aryFieldValue[i]);
						}

						break;
					case "key":

						List<String> rootToKeyPaths = getALLPahtForKey(input, aryNodeSequence[i]);
						for (String rootTokeyPath : rootToKeyPaths) {
							if (onlyOneValue) {
								newJsonStirng = replaceByPath(newJsonStirng, rootTokeyPath, aryFieldValue[0]);
							} else {
								newJsonStirng = replaceByPath(newJsonStirng, rootTokeyPath, aryFieldValue[i]);
							}

						}

						break;
					case "index":
						Integer index = Integer.parseInt(aryNodeSequence[i]);
						String indexPath = "$.[" + index + "]";
						if (onlyOneValue) {
							newJsonStirng = replaceByPath(newJsonStirng, indexPath, aryFieldValue[0]);
						} else {
							newJsonStirng = replaceByPath(newJsonStirng, indexPath, aryFieldValue[i]);
						}

						break;
					default:

					}

				}
			} else {
				LOGS.info("Count of new values and nodepaths are not same. Please provide valid input");
			}

			return newJsonStirng;
		} catch (Exception e) {
			;
			LOGS.error("error in replcing data" + e);
			return null;
		}

	}

	public static String replaceByPath(String input, String path, String newValue) {
		return JsonPath.using(configuration).parse(input).set(path, newValue).json().toString();
	}

	public static List<String> getALLPahtForKey(String input, String key) {
		setALLJsonPaths(input);
		List<String> rootTokeyPaths = new ArrayList<>();
		for (String path : pathList) {
			if (path.endsWith(key)) {
				rootTokeyPaths.add(path);
			}
		}
		return rootTokeyPaths;

	}

	public static void setALLJsonPaths(String json) {
		try {
			pathList.clear();
			JSONParser parser = new JSONParser();
			String jsonPath = "$";
			if (json.startsWith("[")) {
				JSONArray json1 = (JSONArray) parser.parse(json);
				readArray(json1, jsonPath);
			} else {
				JSONObject object = (JSONObject) parser.parse(json);
				readObject(object, jsonPath);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void readArray(JSONArray array, String jsonPath) {
		String parentPath = jsonPath;
		for (int i = 0; i < array.size(); i++) {
			Object value = array.get(i);
			jsonPath = parentPath + "[" + i + "]";

			if (value instanceof JSONArray) {
				readArray((JSONArray) value, jsonPath);
			} else if (value instanceof JSONObject) {
				readObject((JSONObject) value, jsonPath);
			} else { // is a value
				pathList.add(jsonPath);
			}
		}
	}

	public static void readObject(JSONObject object, String jsonPath) {
		Iterator<String> keysItr = object.keySet().iterator();
		String parentPath = jsonPath;
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);
			jsonPath = parentPath + "." + key;

			if (value instanceof JSONArray) {
				readArray((JSONArray) value, jsonPath);
			} else if (value instanceof JSONObject) {
				readObject((JSONObject) value, jsonPath);
			} else { // is a value
				pathList.add(jsonPath);
			}
		}
	}

}
