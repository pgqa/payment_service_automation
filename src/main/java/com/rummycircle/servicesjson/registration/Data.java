package com.rummycircle.servicesjson.registration;

public class Data {

	private boolean register;
	private String pathCUserRef;
	private String userId;
	private String loginId;

	public boolean isRegister() {
		return register;
	}

	public void setRegister(boolean register) {
		this.register = register;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPathCUserRef() {
		return pathCUserRef;
	}

	public void setPathCUserRef(String pathCUserRef) {
		this.pathCUserRef = pathCUserRef;
	}
}
