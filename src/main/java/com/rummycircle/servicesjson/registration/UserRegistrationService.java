package com.rummycircle.servicesjson.registration;

public class UserRegistrationService {
	private String loginid;
	private String loginId;
	private String email;
	private String password;
	private boolean nev;
	private int channel_id;
	private String birthYear;
	private String gender;
	private String fromPage;
	private UTMParams utmParams;
	private boolean register;
	private String pathCUserRef;
	private String userId;
	private int errorCode; 

	public String getLoginid() {
		return loginid;
	}

	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isNev() {
		return nev;
	}

	public void setNev(boolean nev) {
		this.nev = nev;
	}

	public int getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(int channel_id) {
		this.channel_id = channel_id;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public UTMParams getUtmParams() {
		return utmParams;
	}

	public void setUtmParams(UTMParams utmParams) {
		this.utmParams = utmParams;
	}

	@Override
	public String toString() {
		return "UserRegistrationService [loginid=" + loginid + ", loginId=" + loginId + ", email=" + email
				+ ", password=" + password + ", nev=" + nev + ", channel_id=" + channel_id + ", birthYear=" + birthYear
				+ ", gender=" + gender + ", fromPage=" + fromPage + ", utmParams=" + utmParams + ", register="
				+ register + ", pathCUserRef=" + pathCUserRef + ", userId=" + userId + ", errorCode=" + errorCode + "]";
	}

	public boolean isRegister() {
		return register;
	}

	public void setRegister(boolean register) {
		this.register = register;
	}

	public String getPathCUserRef() {
		return pathCUserRef;
	}

	public void setPathCUserRef(String pathCUserRef) {
		this.pathCUserRef = pathCUserRef;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
