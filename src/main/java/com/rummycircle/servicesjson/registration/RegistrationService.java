package com.rummycircle.servicesjson.registration;

public class RegistrationService {
	
	private String loginId;
	//For UTM parameters
	private String loginid;
	
	private String email;
	private String gender;
	private String password;
	private String birthYear;
	private String state;
	private String fromPage;
	private String channel_id;
	private String promoCode;
	private String j_captcha_response;
	private boolean register;
	private String pathCUserRef;
	private String userId;
	private String cookieName;
	private String CookieValue;
	private int errorCode;
	private String mobile;
	private boolean emailSplOffer;
	private String utmMedium;

	//For UTM parameters
	public String getLoginid() {
	    return loginid;
	}
	
	public void setLoginid(String loginid) {
	    this.loginid = loginid;
	}
	
	public boolean isEmailSplOffer() {
	    return emailSplOffer;
	}
	
	public void setEmailSplOffer(boolean emailSplOffer) {
	    this.emailSplOffer = emailSplOffer;
	}
	
	public String getUtmMedium() {
	    return utmMedium;
	}
	
	public void setUtmMedium(String utmMedium) {
	    this.utmMedium = utmMedium;
	}
	
	
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the birthYear
	 */
	public String getBirthYear() {
		return birthYear;
	}
	/**
	 * @param birthYear the birthYear to set
	 */
	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the fromPage
	 */
	public String getFromPage() {
		return fromPage;
	}
	/**
	 * @param fromPage the fromPage to set
	 */
	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}
	/**
	 * @return the channel_id
	 */
	public String getChannel_id() {
		return channel_id;
	}
	/**
	 * @param channel_id the channel_id to set
	 */
	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}
	/**
	 * @return the promoCode
	 */
	public String getPromoCode() {
		return promoCode;
	}
	/**
	 * @param promoCode the promoCode to set
	 */
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	/**
	 * @return the j_captcha_response
	 */
	public String getJ_captcha_response() {
		return j_captcha_response;
	}
	/**
	 * @param j_captcha_response the j_captcha_response to set
	 */
	public void setJ_captcha_response(String j_captcha_response) {
		this.j_captcha_response = j_captcha_response;
	}
	/**
	 * @return the register
	 */
	public boolean isRegister() {
		return register;
	}
	/**
	 * @param register the register to set
	 */
	public void setRegister(boolean register) {
		this.register = register;
	}
	/**
	 * @return the pathCUserRef
	 */
	public String getPathCUserRef() {
		return pathCUserRef;
	}
	/**
	 * @param pathCUserRef the pathCUserRef to set
	 */
	public void setPathCUserRef(String pathCUserRef) {
		this.pathCUserRef = pathCUserRef;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the loginId
	 */
	public String getLoginId() {
		return loginId;
	}
	/**
	 * @param loginId the loginId to set
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	/**
	 * @return the cookieName
	 */
	public String getCookieName() {
		return cookieName;
	}
	/**
	 * @param cookieName the cookieName to set
	 */
	public void setCookieName(String cookieName) {
		this.cookieName = cookieName;
	}
	/**
	 * @return the cookieValue
	 */
	public String getCookieValue() {
		return CookieValue;
	}
	/**
	 * @param cookieValue the cookieValue to set
	 */
	public void setCookieValue(String cookieValue) {
		CookieValue = cookieValue;
	}
	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
}