package com.rummycircle.servicesjson.registration;

public class UTMParams {

	private String utm_medium;
	private String utm_source;
	private String gclid;

	public String getUtm_medium() {
		return utm_medium;
	}

	public void setUtm_medium(String utm_medium) {
		this.utm_medium = utm_medium;
	}

	public String getUtm_source() {
		return utm_source;
	}

	public void setUtm_source(String utm_source) {
		this.utm_source = utm_source;
	}

	public String getGclid() {
		return gclid;
	}

	public void setGclid(String gclid) {
		this.gclid = gclid;
	}

	@Override
	public String toString() {
		return "UTMParams [utm_medium=" + utm_medium + ", utm_source=" + utm_source + ", gclid=" + gclid + "]";
	}

}
