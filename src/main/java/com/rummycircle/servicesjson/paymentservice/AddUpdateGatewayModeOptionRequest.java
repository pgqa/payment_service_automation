package com.rummycircle.servicesjson.paymentservice;

public class AddUpdateGatewayModeOptionRequest {

	private String id;
	private String name;
	private String paymentMasterType;
	private String operationType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPaymentMasterType() {
		return paymentMasterType;
	}

	public void setPaymentMasterType(String paymentMasterType) {
		this.paymentMasterType = paymentMasterType;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

}
