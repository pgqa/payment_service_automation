package com.rummycircle.servicesjson.paymentservice;

import java.util.List;

public class PayModesService {

	private String channelId;
	private String clientId;
	private String ErrorCode;
	private String ErrorMessage;
	private List<PaymentTypeDetails> paymentTypes;
	private List<CreditCardDetails> CREDITCARD;
	private List<CreditCardDetails> DEBITCARD;
	private List<CreditCardDetails> NETBANKING;
	private List<CreditCardDetails> PAYTMWALLET;
	private List<CreditCardDetails> CASHCARD;
	private List<CreditCardDetails> FREECHARGE;
	private List<CreditCardDetails> MOBILEPAYMENT;
	private List<CreditCardDetails> PAYUMONEY;
	private List<CreditCardDetails> MOBIKWIKWALLET;

	public List<CreditCardDetails> getCASHCARD() {
		return CASHCARD;
	}

	public void setCASHCARD(List<CreditCardDetails> cASHCARD) {
		CASHCARD = cASHCARD;
	}

	public List<CreditCardDetails> getFREECHARGE() {
		return FREECHARGE;
	}

	public void setFREECHARGE(List<CreditCardDetails> fREECHARGE) {
		FREECHARGE = fREECHARGE;
	}

	public List<CreditCardDetails> getMOBILEPAYMENT() {
		return MOBILEPAYMENT;
	}

	public void setMOBILEPAYMENT(List<CreditCardDetails> mOBILEPAYMENT) {
		MOBILEPAYMENT = mOBILEPAYMENT;
	}

	public List<CreditCardDetails> getPAYUMONEY() {
		return PAYUMONEY;
	}

	public void setPAYUMONEY(List<CreditCardDetails> pAYUMONEY) {
		PAYUMONEY = pAYUMONEY;
	}

	public List<CreditCardDetails> getMOBIKWIKWALLET() {
		return MOBIKWIKWALLET;
	}

	public void setMOBIKWIKWALLET(List<CreditCardDetails> mOBIKWIKWALLET) {
		MOBIKWIKWALLET = mOBIKWIKWALLET;
	}

	public List<CreditCardDetails> getCREDITCARD() {
		return CREDITCARD;
	}

	public void setCREDITCARD(List<CreditCardDetails> cREDITCARD) {
		CREDITCARD = cREDITCARD;
	}

	public List<CreditCardDetails> getDEBITCARD() {
		return DEBITCARD;
	}

	public void setDEBITCARD(List<CreditCardDetails> dEBITCARD) {
		DEBITCARD = dEBITCARD;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public List<CreditCardDetails> getPAYTMWALLET() {
		return PAYTMWALLET;
	}

	public void setPAYTMWALLET(List<CreditCardDetails> pAYTMWALLET) {
		PAYTMWALLET = pAYTMWALLET;
	}

	public List<CreditCardDetails> getNETBANKING() {
		return NETBANKING;
	}

	public void setNETBANKING(List<CreditCardDetails> nETBANKING) {
		NETBANKING = nETBANKING;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public List<PaymentTypeDetails> getPaymentTypes() {
		return paymentTypes;
	}

	public void setPaymentTypes(List<PaymentTypeDetails> paymentTypes) {
		this.paymentTypes = paymentTypes;
	}

}
