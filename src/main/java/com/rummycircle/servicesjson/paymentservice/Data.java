package com.rummycircle.servicesjson.paymentservice;

import java.util.List;

public class Data {

	public List<InitResponseService> NETBANKING;
	public List<InitResponseService> CREDITCARD;
	public List<InitResponseService> DEBITCARD;

	public List<InitResponseService> getNETBANKING() {
		return NETBANKING;
	}

	public void setNETBANKING(List<InitResponseService> nETBANKING) {
		NETBANKING = nETBANKING;
	}

	public List<InitResponseService> getCREDITCARD() {
		return CREDITCARD;
	}

	public List<InitResponseService> getDEBITCARD() {
		return DEBITCARD;
	}

	public void setCREDITCARD(List<InitResponseService> cREDITCARD) {
		CREDITCARD = cREDITCARD;
	}

	public void setDEBITCARD(List<InitResponseService> dEBITCARD) {
		DEBITCARD = dEBITCARD;
	}
}
