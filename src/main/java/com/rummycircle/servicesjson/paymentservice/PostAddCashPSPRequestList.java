package com.rummycircle.servicesjson.paymentservice;

public class PostAddCashPSPRequestList {
	private PostAddCashPSPRequest[] payload;

	public PostAddCashPSPRequest[] getPayload() {
		return payload;
	}

	public void setPayload(PostAddCashPSPRequest[] payload) {
		this.payload = payload;
	}
}
