package com.rummycircle.servicesjson.paymentservice;

public class UpdatePayloadDetails {

	private String gateway;
	private String payTypeOptId;
	private String channelId;
	private int percent;
	private int[] auditId;

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getPayTypeOptId() {
		return payTypeOptId;
	}

	public void setPayTypeOptId(String payTypeOptId) {
		this.payTypeOptId = payTypeOptId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public int getPercent() {
		return percent;
	}

	public void setPercent(int percent) {
		this.percent = percent;
	}

	public int[] getAuditId() {
		return auditId;
	}

	public void setAuditId(int[] auditId) {
		this.auditId = auditId;
	}

	public UpdatePayloadDetails(String gateway, String payTypeOptId,
			String channelId, int percent, int[] auditId) {

		this.gateway = gateway;
		this.payTypeOptId = payTypeOptId;
		this.channelId = channelId;
		this.percent = percent;
		this.auditId = auditId;

	}

}
