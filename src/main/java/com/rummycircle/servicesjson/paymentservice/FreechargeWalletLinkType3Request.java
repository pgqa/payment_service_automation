package com.rummycircle.servicesjson.paymentservice;

public class FreechargeWalletLinkType3Request {

	private String type;
	private String otpId;
	private String otp;
	private String accountNumber;
	private String userId;
	private String userMachineIdentifier;
	private String walletId;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOtpId() {
		return otpId;
	}

	public void setOtpId(String otpId) {
		this.otpId = otpId;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserMachineIdentifier() {
		return userMachineIdentifier;
	}

	public void setUserMachineIdentifier(String userMachineIdentifier) {
		this.userMachineIdentifier = userMachineIdentifier;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
}
