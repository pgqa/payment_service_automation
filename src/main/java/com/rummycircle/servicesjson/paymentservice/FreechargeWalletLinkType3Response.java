package com.rummycircle.servicesjson.paymentservice;

public class FreechargeWalletLinkType3Response {

	private String accessToken;
	private String accessTokenExpiry;
	private String checksum;
	private String refreshToken;
	private String refreshTokenExpiry;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAccessTokenExpiry() {
		return accessTokenExpiry;
	}

	public void setAccessTokenExpiry(String accessTokenExpiry) {
		this.accessTokenExpiry = accessTokenExpiry;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getRefreshTokenExpiry() {
		return refreshTokenExpiry;
	}

	public void setRefreshTokenExpiry(String refreshTokenExpiry) {
		this.refreshTokenExpiry = refreshTokenExpiry;
	}

}
