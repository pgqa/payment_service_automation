package com.rummycircle.servicesjson.paymentservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProcessPayServicePayTM {
	private String source;
	private int type_id;
	private String ORDERID;
	private int TXNAMOUNT;
	private String RESPCODE;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	@JsonProperty("ORDERID")
	public String getORDERID() {
		return ORDERID;
	}

	@JsonProperty("ORDERID")
	public void setORDERID(String oRDERID) {
		ORDERID = oRDERID;
	}

	@JsonProperty("TXNAMOUNT")
	public int getTXNAMOUNT() {
		return TXNAMOUNT;
	}

	@JsonProperty("TXNAMOUNT")
	public void setTXNAMOUNT(int tXNAMOUNT) {
		TXNAMOUNT = tXNAMOUNT;
	}

	@JsonProperty("RESPCODE")
	public String getRESPCODE() {
		return RESPCODE;
	}

	@JsonProperty("RESPCODE")
	public void setRESPCODE(String rESPCODE) {
		RESPCODE = rESPCODE;
	}

	@Override
	public String toString() {
		return "ProcessPayServicePayTM [source=" + source + ", type_id="
				+ type_id + ", ORDERID=" + ORDERID + ", TXNAMOUNT=" + TXNAMOUNT
				+ ", RESPCODE=" + RESPCODE + "]";
	}
}
