package com.rummycircle.servicesjson.paymentservice;

public class AuthorisationInputParamService {
	
	public String clientName;
	public boolean edsRequired;
	public boolean auditRequired;
	public String merchantName;
	public String schemaName;
	

	public boolean isEdsRequired() {
		return edsRequired;
	}
	public boolean isAuditRequired() {
		return auditRequired;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public String getSchemaName() {
		return schemaName;
	}

	public void setEdsRequired(boolean edsRequired) {
		this.edsRequired = edsRequired;
	}
	public void setAuditRequired(boolean auditRequired) {
		this.auditRequired = auditRequired;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	@Override
	public String toString() {
		return "AuthorisationInputParamService [clientName=" + clientName
				+ ", edsRequired=" + edsRequired + ", auditRequired="
				+ auditRequired + ", merchantName=" + merchantName
				+ ", schemaName=" + schemaName + "]";
	}

}
