package com.rummycircle.servicesjson.paymentservice;

public class InitRedisDataService {

	private double amount;
	private String promocode;
	private String channelId;
	private String userId;
	private String paymentMode;
	private String paymentOption;
	private String orderId;
	private String status;
	private int rewardStoreItemId;
	private String gateway;
	private int gatewayId;
	private int paymentModeId;
	private boolean firstDepositor;

	public String getPromocode() {
		return promocode;
	}

	public String getChannelId() {
		return channelId;
	}

	public String getUserId() {
		return userId;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getRewardStoreItemId() {
		return rewardStoreItemId;
	}

	public String getGateway() {
		return gateway;
	}

	public int getGatewayId() {
		return gatewayId;
	}

	public int getPaymentModeId() {
		return paymentModeId;
	}

	public boolean isFirstDepositor() {
		return firstDepositor;
	}

	public void setRewardStoreItemId(int rewardStoreItemId) {
		this.rewardStoreItemId = rewardStoreItemId;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public void setGatewayId(int gatewayId) {
		this.gatewayId = gatewayId;
	}

	public void setPaymentModeId(int paymentModeId) {
		this.paymentModeId = paymentModeId;
	}

	public void setFirstDepositor(boolean firstDepositor) {
		this.firstDepositor = firstDepositor;
	}

	@Override
	public String toString() {
		return "InitRedisDataService [amount=" + amount + ", promocode="
				+ promocode + ", channelId=" + channelId + ", userId=" + userId
				+ ", paymentMode=" + paymentMode + ", paymentOption="
				+ paymentOption + ", orderId=" + orderId + ", status=" + status
				+ ", rewardStoreItemId=" + rewardStoreItemId + ", gateway="
				+ gateway + ", gatewayId=" + gatewayId + ", paymentModeId="
				+ paymentModeId + ", firstDepositor=" + firstDepositor + "]";
	}

}
