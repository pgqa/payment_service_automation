package com.rummycircle.servicesjson.paymentservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProcessPayServiceCCAvenue {
	private String source;
	private String Order_Id;
	private int Amount;
	private String AuthDesc;
	private String Merchant_Param;
	private int type_id;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@JsonProperty("Order_Id")
	public String getOrder_Id() {
		return Order_Id;
	}

	@JsonProperty("Order_Id")
	public void setOrder_Id(String order_Id) {
		Order_Id = order_Id;
	}

	@JsonProperty("Amount")
	public int getAmount() {
		return Amount;
	}

	@JsonProperty("Amount")
	public void setAmount(int amount) {
		Amount = amount;
	}

	@JsonProperty("AuthDesc")
	public String getAuthDesc() {
		return AuthDesc;
	}

	@JsonProperty("AuthDesc")
	public void setAuthDesc(String authDesc) {
		AuthDesc = authDesc;
	}

	@JsonProperty("Merchant_Param")
	public String getMerchant_Param() {
		return Merchant_Param;
	}

	@JsonProperty("Merchant_Param")
	public void setMerchant_Param(String merchant_Param) {
		Merchant_Param = merchant_Param;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	@Override
	public String toString() {
		return "ProcessPayServiceCCAvenue [source=" + source + ", Order_Id="
				+ Order_Id + ", Amount=" + Amount + ", AuthDesc=" + AuthDesc
				+ ", Merchant_Param=" + Merchant_Param + ", type_id=" + type_id
				+ "]";
	}
}
