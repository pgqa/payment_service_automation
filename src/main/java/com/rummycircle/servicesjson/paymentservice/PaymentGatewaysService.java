package com.rummycircle.servicesjson.paymentservice;

public class PaymentGatewaysService {
	private int id;
	private String name;
	private long updateTime;
	private String processorCode;
	private boolean detailRequired;
	private int priority;
	private long updateDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	public String getProcessorCode() {
		return processorCode;
	}

	public void setProcessorCode(String processorCode) {
		this.processorCode = processorCode;
	}

	public boolean isDetailRequired() {
		return detailRequired;
	}

	public void setDetailRequired(boolean detailRequired) {
		this.detailRequired = detailRequired;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(long updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "PaymentGatewaysService [id=" + id + ", name=" + name
				+ ", updateTime=" + updateTime + ", processorCode="
				+ processorCode + ", detailRequired=" + detailRequired
				+ ", priority=" + priority + ", updateDate=" + updateDate + "]";
	}
}
