package com.rummycircle.servicesjson.paymentservice;

public class AlternatePayloadDetails {

	private String gateway;
	private String payTypeOptId;
	private String priority;
	private int[] auditId;

	public AlternatePayloadDetails(String gateway, String payTypeOptId,
			String priority, int[] auditId) {

		this.gateway = gateway;
		this.payTypeOptId = payTypeOptId;
		this.priority = priority;
		this.auditId = auditId;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getPayTypeOptId() {
		return payTypeOptId;
	}

	public void setPayTypeOptId(String payTypeOptId) {
		this.payTypeOptId = payTypeOptId;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public int[] getAuditId() {
		return auditId;
	}

	public void setAuditId(int[] auditId) {
		this.auditId = auditId;
	}

}
