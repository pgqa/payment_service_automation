package com.rummycircle.servicesjson.paymentservice;

public class PaymentMetadataService {
	private int id;
	private String paymentOption;
	private int percentage;
	private long maxUserId;
	private String processorCode;
	private String paymentGateway;
	private int gatewayId;
	private long minUserId;
	private int active;
	private boolean detailRequired;
	private int channelId;
	private int payTypeOptId;
	private String lastUpdatedAt;
	private String lastUpdatedBy;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public String getProcessorCode() {
		return processorCode;
	}

	public void setProcessorCode(String processorCode) {
		this.processorCode = processorCode;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public int getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(int gatewayId) {
		this.gatewayId = gatewayId;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public boolean isDetailRequired() {
		return detailRequired;
	}

	public void setDetailRequired(boolean detailRequired) {
		this.detailRequired = detailRequired;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public int getPayTypeOptId() {
		return payTypeOptId;
	}

	public void setPayTypeOptId(int payTypeOptId) {
		this.payTypeOptId = payTypeOptId;
	}

	public String getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public void setLastUpdatedAt(String lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Override
	public String toString() {
		return "PaymentMetadataService [id=" + id + ", paymentOption="
				+ paymentOption + ", percentage=" + percentage + ", maxUserId="
				+ maxUserId + ", processorCode=" + processorCode
				+ ", paymentGateway=" + paymentGateway + ", gatewayId="
				+ gatewayId + ", minUserId=" + minUserId + ", active=" + active
				+ ", detailRequired=" + detailRequired + ", channelId="
				+ channelId + ", payTypeOptId=" + payTypeOptId
				+ ", lastUpdatedAt=" + lastUpdatedAt + ", lastUpdatedBy="
				+ lastUpdatedBy + "]";
	}

	public void setMaxUserId(long maxUserId) {
		this.maxUserId = maxUserId;
	}

	public void setMinUserId(long minUserId) {
		this.minUserId = minUserId;
	}

	public long getMaxUserId() {
		return maxUserId;
	}

	public long getMinUserId() {
		return minUserId;
	}
}
