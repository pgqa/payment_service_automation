package com.rummycircle.servicesjson.paymentservice;

public class InitResponseService {
	
	private String name;
	private String id;
	private String value;
	private String DEBITCARD;
	private String NETBANKING;
	private String CREDITCARD;
	
	
	public String getName() {
		return name;
	}
	public String getId() {
		return id;
	}
	public String getValue() {
		return value;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDEBITCARD() {
		return DEBITCARD;
	}
	public String getNETBANKING() {
		return NETBANKING;
	}
	public String getCREDITCARD() {
		return CREDITCARD;
	}
	public void setDEBITCARD(String dEBITCARD) {
		DEBITCARD = dEBITCARD;
	}
	public void setNETBANKING(String nETBANKING) {
		NETBANKING = nETBANKING;
	}
	public void setCREDITCARD(String cREDITCARD) {
		CREDITCARD = cREDITCARD;
	}
	@Override
	public String toString() {
		return "InitResponseService [name=" + name + ", id=" + id + ", value="
				+ value + ", DEBITCARD=" + DEBITCARD + ", NETBANKING="
				+ NETBANKING + ", CREDITCARD=" + CREDITCARD + "]";
	}

}
