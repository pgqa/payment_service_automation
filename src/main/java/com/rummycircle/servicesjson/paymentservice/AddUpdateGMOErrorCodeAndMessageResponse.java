package com.rummycircle.servicesjson.paymentservice;

public class AddUpdateGMOErrorCodeAndMessageResponse {

	private String ErrorCode;
	private String ErrorMessage;

	public String getCode() {
		return ErrorCode;
	}

	public void setCode(String code) {
		this.ErrorCode = code;
	}

	public String getMessage() {
		return ErrorMessage;
	}

	public void setMessage(String message) {
		this.ErrorMessage = message;
	}
}
