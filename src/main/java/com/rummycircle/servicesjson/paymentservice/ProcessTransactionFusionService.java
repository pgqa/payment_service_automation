package com.rummycircle.servicesjson.paymentservice;

import java.util.List;

public class ProcessTransactionFusionService {
	private List<Object> payload;

	public List<Object> getPayload() {
		return payload;
	}

	public void setPayload(List<Object> payload) {
		this.payload = payload;
	}

	/*
	 * Overloaded toString()
	 */
	@Override
	public String toString() {
		return "ProcessTransactionFusionService [payload=" + payload + "]";
	}
}
