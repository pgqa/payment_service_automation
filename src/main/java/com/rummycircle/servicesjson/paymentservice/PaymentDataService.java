package com.rummycircle.servicesjson.paymentservice;

import java.util.List;

public class PaymentDataService {
	private String paymentMode;
	private List<PaymentMetadataService> metadata;

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public List<PaymentMetadataService> getMetadata() {
		return metadata;
	}

	public void setMetadata(List<PaymentMetadataService> metadata) {
		this.metadata = metadata;
	}

	@Override
	public String toString() {
		return "PaymentDataService [paymentMode=" + paymentMode + ", metadata="
				+ metadata + "]";
	}
}
