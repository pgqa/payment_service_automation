package com.rummycircle.servicesjson.paymentservice;

public class PGConfigService {

	private String paymentType;
	private int channel;
	private String gatewayType;
	private Boolean success;
	

	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public int getChannel() {
		return channel;
	}
	public void setChannel(int channel) {
		this.channel = channel;
	}
	public String getGatewayType() {
		return gatewayType;
	}
	public void setGatewayType(String gatewayType) {
		this.gatewayType = gatewayType;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean sucess) {
		this.success = sucess;
	}
	
	
}
