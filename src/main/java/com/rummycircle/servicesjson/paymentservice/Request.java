package com.rummycircle.servicesjson.paymentservice;

public class Request {
	
	private int minUserId;
	private String requestType;
	private String merchantName;	
	private String agentName;

	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public int getMinUserId() {
		return minUserId;
	}
	public void setMinUserId(int minUserId) {
		this.minUserId = minUserId;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

}
