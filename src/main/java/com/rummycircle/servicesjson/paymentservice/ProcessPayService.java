package com.rummycircle.servicesjson.paymentservice;

public class ProcessPayService {
	private int amount;
	private String orderId;
	private int userId;
	private int status;
	private String checksums;
	private String gateway;
	private String authStatus;
	private int txnId;
	private String paymentOption;
	private String paymentType;
	private int channelId;
	private String paymentMode;
	private String promoCode;
	private int rewardStoreItemId;
	private double bonusAmount;
	private int gatewayId;
	private int paymentModeId;
	private String errorCode;
	private boolean firstDepositor;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getChecksums() {
		return checksums;
	}

	public void setChecksums(String checksums) {
		this.checksums = checksums;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getAuthStatus() {
		return authStatus;
	}

	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}

	public int getTxnId() {
		return txnId;
	}

	public void setTxnId(int txnId) {
		this.txnId = txnId;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public int getRewardStoreItemId() {
		return rewardStoreItemId;
	}

	public void setRewardStoreItemId(int rewardStoreItemId) {
		this.rewardStoreItemId = rewardStoreItemId;
	}

	public double getBonusAmount() {
		return bonusAmount;
	}

	public void setBonusAmount(double bonusAmount) {
		this.bonusAmount = bonusAmount;
	}

	public int getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(int gatewayId) {
		this.gatewayId = gatewayId;
	}

	public int getPaymentModeId() {
		return paymentModeId;
	}

	public void setPaymentModeId(int paymentModeId) {
		this.paymentModeId = paymentModeId;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public boolean isFirstDepositor() {
		return firstDepositor;
	}

	public void setFirstDepositor(boolean firstDepositor) {
		this.firstDepositor = firstDepositor;
	}

	@Override
	public String toString() {
		return "ProcessPayService [amount=" + amount + ", orderId=" + orderId + ", userId=" + userId + ", status="
				+ status + ", checksums=" + checksums + ", gateway=" + gateway + ", authStatus=" + authStatus
				+ ", txnId=" + txnId + ", paymentOption=" + paymentOption + ", paymentType=" + paymentType
				+ ", channelId=" + channelId + ", paymentMode=" + paymentMode + ", promoCode=" + promoCode
				+ ", rewardStoreItemId=" + rewardStoreItemId + ", bonusAmount=" + bonusAmount + ", gatewayId="
				+ gatewayId + ", paymentModeId=" + paymentModeId + ", errorCode=" + errorCode + ", firstDepositor="
				+ firstDepositor + "]";
	}
}