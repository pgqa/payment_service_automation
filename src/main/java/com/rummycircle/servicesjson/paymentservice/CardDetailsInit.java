package com.rummycircle.servicesjson.paymentservice;

public class CardDetailsInit {
	
	public int C1;
	public int C2;
	public int C3;
	public int C4;
	public int month;
	public int year;
	public int cvv;
	public String name;
	
	
	public int getMonth() {
		return month;
	}
	public int getYear() {
		return year;
	}
	public int getCvv() {
		return cvv;
	}
	public String getName() {
		return name;
	}
	
	public void setMonth(int month) {
		this.month = month;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public void setCvv(int cvv) {
		this.cvv = cvv;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getC1() {
		return C1;
	}
	public int getC2() {
		return C2;
	}
	public int getC3() {
		return C3;
	}
	public int getC4() {
		return C4;
	}
	public void setC1(int c1) {
		C1 = c1;
	}
	public void setC2(int c2) {
		C2 = c2;
	}
	public void setC3(int c3) {
		C3 = c3;
	}
	public void setC4(int c4) {
		C4 = c4;
	}
	
	@Override
	public String toString() {
		return "CardDetailsInit [C1=" + C1 + ", C2=" + C2 + ", C3=" + C3
				+ ", C4=" + C4 + ", month=" + month + ", year=" + year
				+ ", cvv=" + cvv + ", name=" + name + "]";
	}

	

}
