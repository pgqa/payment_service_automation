package com.rummycircle.servicesjson.paymentservice;

public class WalletDetailsService {
	
	private String walletBalance;
	private String id;
	private String accessToken;
	private String accountNumber;
	private String status;
	private String ErrorCode;
	private String ErrorMessage;
	
	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public String getWalletBalance() {
		return walletBalance;
	}
	
	public void setWalletBalance(String walletBalance) {
		this.walletBalance = walletBalance;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getAccessToken() {
		return accessToken;
	}
	
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

}
