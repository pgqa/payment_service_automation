package com.rummycircle.servicesjson.paymentservice;

public class PaymentServiceConfigPOJO {

	String payTypeOptId;
	String channelId;
	String merchantName;
	int minUserId;
	String status;
	int percent;
	int priority;
	
	
	
	public String getPayTypeOptId() {
		return payTypeOptId;
	}
	public void setPayTypeOptId(String payTypeOptId) {
		this.payTypeOptId = payTypeOptId;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public int getMinUserId() {
		return minUserId;
	}
	public void setMinUserId(int minUserId) {
		this.minUserId = minUserId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getPercent() {
		return percent;
	}
	public void setPercent(int percent) {
		this.percent = percent;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}

}
