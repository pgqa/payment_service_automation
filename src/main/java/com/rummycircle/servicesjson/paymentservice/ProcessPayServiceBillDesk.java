package com.rummycircle.servicesjson.paymentservice;

public class ProcessPayServiceBillDesk {
	private String source;
	private String msg;
	private int type_id;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	@Override
	public String toString() {
		return "BillDeskService [source=" + source + ", msg=" + msg
				+ ", type_id=" + type_id + "]";
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
