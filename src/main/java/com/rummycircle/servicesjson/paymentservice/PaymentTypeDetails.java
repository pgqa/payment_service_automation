package com.rummycircle.servicesjson.paymentservice;

public class PaymentTypeDetails {

	String type;
	
	String label;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}


}
