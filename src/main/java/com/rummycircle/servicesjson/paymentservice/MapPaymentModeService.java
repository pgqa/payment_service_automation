package com.rummycircle.servicesjson.paymentservice;

public class MapPaymentModeService {
	
	private String paymentModeId;
	private String paymentOptionId;
	private String label;
	private String status;
	private String ErrorCode;
	private String ErrorMessage;
	
	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentModeId() {
		return paymentModeId;
	}
	
	public void setPaymentModeId(String paymentModeId) {
		this.paymentModeId = paymentModeId;
	}
	
	public String getPaymentOptionId() {
		return paymentOptionId;
	}
	
	public void setPaymentOptionId(String paymentOptionId) {
		this.paymentOptionId = paymentOptionId;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	

}
