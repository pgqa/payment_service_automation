package com.rummycircle.servicesjson.paymentservice;

public class FreechargeWalletLinkType1_2Response {

	private String checksum;
	private String isIvrEnabled;
	private String otpId;
	private String redirectUrl;
	private String signUp;
	private String status;

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public String getIsIvrEnabled() {
		return isIvrEnabled;
	}

	public void setIsIvrEnabled(String isIvrEnabled) {
		this.isIvrEnabled = isIvrEnabled;
	}

	public String getOtpId() {
		return otpId;
	}

	public void setOtpId(String otpId) {
		this.otpId = otpId;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getSignUp() {
		return signUp;
	}

	public void setSignUp(String signUp) {
		this.signUp = signUp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
