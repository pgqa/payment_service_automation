package com.rummycircle.servicesjson.paymentservice;

public class ProcessPayServicePayUMoney {
	private String source;
	private int type_id;
	private String txnid;
	private int amount;
	private String status;
	private String userId;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ProcessPayServicePayUMoney [source=" + source + ", type_id="
				+ type_id + ", txnid=" + txnid + ", amount=" + amount
				+ ", status=" + status + "]";
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
