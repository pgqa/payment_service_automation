package com.rummycircle.servicesjson.paymentservice;

public class ProcessPayServiceTechProcess {
	private String source;
	private int type_id;
	private String msg;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "ProcessPayServiceTechProcess [source=" + source + ", type_id="
				+ type_id + ", msg=" + msg + "]";
	}
}
