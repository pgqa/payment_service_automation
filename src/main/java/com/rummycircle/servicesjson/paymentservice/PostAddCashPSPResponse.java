package com.rummycircle.servicesjson.paymentservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostAddCashPSPResponse {
	private String status;
	private int channelFlag;
	private String gatewayName;
	private String paymentMode;
	private float withdrawable;
	private String successPage;
	private String orderId;
	private int amount;
	private String authStatus;
	private int rewardStoreItemId;
	private float bonusAmount;
	private boolean firstDeposit;
	private String ErrorCode;
	private String errorMessage;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String success) {
		this.status = success;
	}

	public int getChannelFlag() {
		return channelFlag;
	}

	public void setChannelFlag(int channelFlag) {
		this.channelFlag = channelFlag;
	}

	public String getGatewayName() {
		return gatewayName;
	}

	public void setGatewayName(String gatewayName) {
		this.gatewayName = gatewayName;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getSuccessPage() {
		return successPage;
	}

	public void setSuccessPage(String successPage) {
		this.successPage = successPage;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getAuthStatus() {
		return authStatus;
	}

	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}

	public int getRewardStoreItemId() {
		return rewardStoreItemId;
	}

	public void setRewardStoreItemId(int rewardStoreItemId) {
		this.rewardStoreItemId = rewardStoreItemId;
	}

	public float getBonusAmount() {
		return bonusAmount;
	}

	public void setBonusAmount(float bonusAmount) {
		this.bonusAmount = bonusAmount;
	}

	public boolean isFirstDeposit() {
		return firstDeposit;
	}

	public void setFirstDeposit(boolean firstDeposit) {
		this.firstDeposit = firstDeposit;
	}
	
	@JsonProperty("errorMessage")
	public String getErrorMessage() {
		return errorMessage;
	}

	@JsonProperty("errorMessage")
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@JsonProperty("ErrorCode")
	public String getErrorCode() {
		return ErrorCode;
	}

	@JsonProperty("ErrorCode")
	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	@Override
	public String toString() {
		return "PostAddCashPSPResponse [status=" + status + ", channelFlag=" + channelFlag + ", gatewayName="
				+ gatewayName + ", paymentMode=" + paymentMode + ", withdrawable=" + withdrawable + ", successPage="
				+ successPage + ", orderId=" + orderId + ", amount=" + amount + ", authStatus=" + authStatus
				+ ", rewardStoreItemId=" + rewardStoreItemId + ", bonusAmount=" + bonusAmount + ", firstDeposit="
				+ firstDeposit + "]";
	}

	public float getWithdrawable() {
		return withdrawable;
	}

	public void setWithdrawable(float userBalance) {
		this.withdrawable = userBalance;
	}	
	
}