package com.rummycircle.servicesjson.paymentservice;

public interface PaymentServicesErrors {
	// Generic Error Messages
	public static final String ERR_MSG_SQL_EXCEPTION = "Sql Exception has occured";
	public static final String ERR_MSG_INVALID_TYPE_PARAM = "Unhandled type id ";
	public static final String ERR_MSG_INVALID_OR_MISSING_PARAM = "Parameters are invalid or missing ";

	public static final String ERR_MSG_INAVLID_STATUS = "The status is invalid";
	public static final String ERR_MSG_NO_RECORDS = "No Records Found";
	public static final String ERR_MSG_REQUEST_EMPTY_OR_INVALID = "The request is empty or invalid";

	// Generic Error Codes
	public static final String ERR_CODE_SQL_EXCEPTION = "PSE0015";
	public static final String ERR_CODE_INVALID_TYPE_PARAM = "PSE0016";
	public static final String ERR_CODE_INVALID_OR_MISSING_PARAM = "PSE0017";

	public static final String ERR_CODE_INAVLID_STATUS = "PSE0001";
	public static final String ERR_CODE_NO_RECORDS = "PSE0007";
	public static final String ERR_CODE_REQUEST_EMPTY_OR_INVALID = "PSE0011";

	// Error Codes for PaymentGatewayResourceErrors
	public static final String ERR_CODE_LABEL = "PSE0803";
	public static final String ERR_CODE_PAYMENTMODE = "PSE0801";
	public static final String ERR_CODE_PAYMENTOPTION = "PSE0802";

	// Error Messages for PaymentGatewayResourceErrors
	public static final String ERR_MSG_LABEL = "Label is EITHER NULL OR INCORRECT OR MISSING";
	public static final String ERR_MSG_PAYMENTMODE = "paymentModeId EITHER NULL OR INCORRECT OR MISSING";
	public static final String ERR_MSG_PAYMENTOPTION = "paymentOptionId EITHER NULL OR INCORRECT OR MISSING";

	// Error Codes for FreechargeWalletLinkErrors
	public static final String ERR_CODE_INVALID_MOBILE_NO = "E631";
	public static final String ERR_CODE_APPLICATION_ERROR_OCCURED = "E018";
	public static final String ERR_CODE_INVALID_OTP_ID = "E701";
	public static final String ERR_CODE_INVALID_CHECKSUM = "E005";
	public static final String ERR_CODE_INVALID_OTP = "E702";

	// Error Messages for FreechargeWalletLinkErrors
	public static final String ERR_MSG_INVALID_MOBILE_NO = "Invalid Mobile Number";
	public static final String ERR_MSG_APPLICATION_ERROR_OCCURED = "Application error occured";
	public static final String ERR_MSG_INVALID_OTP_ID = "Invalid OTP id";
	public static final String ERR_MSG_INVALID_CHECKSUM = "Invalid checksum/Checksum mismatch";
	public static final String ERR_MSG_INVALID_OTP = "Invalid OTP";
	
	// InitPay Error Codes
	public static final String ERR_CODE_INVALID_USER = "PSE0003";
	public static final String ERR_CODE_PAYMENT_CONF_INACTIVE = "PSE0103";
	public static final String ERR_CODE_INVALID_OR_MISSING_DATA = "PSE0017";
	public static final String ERR_CODE_INVALID_AMOUNT = "PSE0018";
	public static final String ERR_CODE_MERCHANT_NAME_MISSING = "PSE0013";
	public static final String ERR_CODE_NO_GATEWAY_CONFIGURED = "PSE0102";

	// InitPay Error Messages
	public static final String ERR_MSG_INVALID_USER = "User is invalid";
	public static final String ERR_MSG_PAYMENT_CONF_INACTIVE = "Payment gateway configuration is not active";
	public static final String ERR_MSG_INVALID_OR_MISSING_DATA = "Parameters are invalid or missing ";
	public static final String ERR_MSG_INVALID_AMOUNT = "Invalid Amount ";
	public static final String ERR_MSG_MERCHANT_NAME_MISSING = "Merchant Name is incorrect or missing";
	public static final String ERR_MSG_NO_GATEWAY_CONFIGURED = "No Gateway Id Configured for this Gateway";

	// SearchOrder Error Codes
	public static final String ERR_CODE_MISSING_CREATION_TODATE = "PSE0401";
	public static final String ERR_CODE_MISSING_CREATION_FROMDATE = "PSE0402";
	public static final String ERR_CODE_MISSING_UPDATE_TODATE = "PSE0403";
	public static final String ERR_CODE_MISSING_UPDATE_FROMDATE = "PSE0404";
	public static final String ERR_CODE_NON_INTEGER_USERID = "PSE0405";
	public static final String ERR_CODE_NON_INTEGER_AMOUNT = "PSE0406";
	public static final String ERR_CODE_BLANK_ORDERID = "PSE0407";
	public static final String ERR_CODE_ONLY_STATUS_SELECTION = "PSE0408";

	// Search Order Error Messages
	public static final String ERR_MSG_MISSING_CREATION_TODATE = "Please Enter Creation To dates";
	public static final String ERR_MSG_MISSING_CREATION_FROMDATE = "Please Enter Creation To dates";
	public static final String ERR_MSG_MISSING_UPDATE_TODATE = "Please Enter Update To dates";
	public static final String ERR_MSG_MISSING_UPDATE_FROMDATE = "Please Enter Update From dates";
	public static final String ERR_MSG_NON_INTEGER_USERID = "User Id entered is not an integer";
	public static final String ERR_MSG_NON_INTEGER_AMOUNT = "Amount entered is not an integer";
	public static final String ERR_MSG_BLANK_ORDERID = "Order Id enetered is blank";
	public static final String ERR_MSG_ONLY_STATUS_SELECTION = "Only status cannot be selected";

	// Add Update Master Table Error Codes
	public static final String ERR_CODE_NAME_IS_NULL_OR_EMPTY = "PSE1401";
	public static final String ERR_CODE_INVALID_VALUE_OF_PAYMENTMASTERTYPE = "PSE1402";
	public static final String ERR_CODE_SQL_EXCEPTION_CONSTRAINT_VIOLATION = "PSE0201";
	public static final String ERR_CODE_OPERATION_TYPE_ADD_UPDATE_ONLY = "PSE1403";
	public static final String ERR_CODE_INVALID_VALUE_MASTERID = "PSE1403";

	// Add Update Master Table Error Messages
	public static final String ERR_MSG_NAME_IS_NULL_OR_EMPTY = "Name is either null or empty";
	public static final String ERR_MSG_INVALID_VALUE_OF_PAYMENTMASTERTYPE = "Invalid value for PaymentMasterType";
	public static final String ERR_MSG_SQL_EXCEPTION_CONSTRAINT_VIOLATION = "SQL statement not being able to execute due to constraint violation.";
	public static final String ERR_MSG_OPERATION_TYPE_ADD_UPDATE_ONLY = "OperationType can be add or update only";
	public static final String ERR_MSG_INVALID_VALUE_MASTERID = "Invalid value for MasterId";
	
	public static final String ERR_CODE_INCORRECT_RESPONSE_PAY_SERV="FSE0115";
	public static final String ERR_MSG_INCORRECT_RESPONSE_PAY_SERV="INCORRECT RESPONSE FROM PAYMENT_SERVICE";
	
	
	public static final String ERR_CODE_INVALID_OR_MISSING_USERID = "PSE0004";
	public static final String ERR_MSG_INVALID_OR_MISSING_USERID = "User id is invalid or missing";
}
