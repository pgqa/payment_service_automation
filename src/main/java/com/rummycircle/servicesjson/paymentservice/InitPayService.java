package com.rummycircle.servicesjson.paymentservice;

public class InitPayService {
	
	public InitRedisDataService orderId;
	public CardDetailsInit card;
	public String data;

	public InitRedisDataService getOrderId() {
		return orderId;
	}

	public void setOrderId(InitRedisDataService orderId) {
		this.orderId = orderId;
	}

	public CardDetailsInit getCard() {
		return card;
	}

	public String getData() {
		return data;
	}

	public void setCard(CardDetailsInit card) {
		this.card = card;
	}

	public void setData(String data) {
		this.data = data;
	}

}
