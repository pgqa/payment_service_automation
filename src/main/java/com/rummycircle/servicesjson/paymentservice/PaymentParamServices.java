package com.rummycircle.servicesjson.paymentservice;

import java.util.List;

public class PaymentParamServices {

	String ErrorCode;
	String ErrorMessage;
	private int channelId;
	private String paymentType;
	private String paymentOption;
	private String processorCode;
	private int percentage;
	private String gateway;
	private int active;
	private String paymentTypeLabel;
	private String paymentOptionLabel;
	private String id;
	private List<PayloadDetails> payload;
	private Request request;
	private String table;

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public String getProcessorCode() {
		return processorCode;
	}

	public void setProcessorCode(String processorCode) {
		this.processorCode = processorCode;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getPaymentTypeLabel() {
		return paymentTypeLabel;
	}

	public void setPaymentTypeLabel(String paymentTypeLabel) {
		this.paymentTypeLabel = paymentTypeLabel;
	}

	public String getPaymentOptionLabel() {
		return paymentOptionLabel;
	}

	public void setPaymentOptionLabel(String paymentOptionLabel) {
		this.paymentOptionLabel = paymentOptionLabel;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<PayloadDetails> getPayload() {
		return payload;
	}

	public void setPayload(List<PayloadDetails> payload) {
		this.payload = payload;
	}

}
