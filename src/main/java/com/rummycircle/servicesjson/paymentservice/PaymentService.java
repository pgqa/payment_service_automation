package com.rummycircle.servicesjson.paymentservice;

import java.util.List;

public class PaymentService {

	public List<SearchOrderService> SearchArray;
	public String ErrorCode;
	public String ErrorMessage;
	public String userId;
	public String amount;
	public String status;
	public List<InitResponseService> gateways;
	public ProcessParameter processParamters;
	public InitResponseService action;
	public List<InitResponseService> data;
	public String orderId;
	public String secretKey;
	public String defaultPaymentMode;
	private List<PaymentDataService> paymentData;
	private List<PaymentGatewaysService> paymentGateways;
	private List<PaymentModesService> paymentModes;
	private List<PaymentOptionsService> paymentOptions;
	private List<PaymentModeOptionMappingService> paymentModeOptionMapping;
	private List<PaymentAlternteGatewaysService> paymentAlternteGateways;
	public String paymentOption;
	public String paymentModeLabel;
	public String paymentOptionLabel;
	public long transactionId;

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public List<SearchOrderService> getSearchArray() {
		return SearchArray;
	}

	public String getUserId() {
		return userId;
	}

	public void setSearchArray(List<SearchOrderService> searchArray) {
		SearchArray = searchArray;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<InitResponseService> getGateways() {
		return gateways;
	}

	public InitResponseService getAction() {
		return action;
	}

	public void setGateways(List<InitResponseService> gateways) {
		this.gateways = gateways;
	}

	public void setAction(InitResponseService action) {
		this.action = action;
	}

	public List<InitResponseService> getData() {
		return data;
	}

	public void setData(List<InitResponseService> data) {
		this.data = data;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public ProcessParameter getProcessParamters() {
		return processParamters;
	}

	public void setProcessParamters(ProcessParameter processParamters) {
		this.processParamters = processParamters;
	}

	public String getDefaultPaymentMode() {
		return defaultPaymentMode;
	}

	public void setDefaultPaymentMode(String defaultPaymentMode) {
		this.defaultPaymentMode = defaultPaymentMode;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public List<PaymentDataService> getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(List<PaymentDataService> paymentData) {
		this.paymentData = paymentData;
	}

	public List<PaymentGatewaysService> getPaymentGateways() {
		return paymentGateways;
	}

	public void setPaymentGateways(List<PaymentGatewaysService> paymentGateways) {
		this.paymentGateways = paymentGateways;
	}

	public List<PaymentModesService> getPaymentModes() {
		return paymentModes;
	}

	public void setPaymentModes(List<PaymentModesService> paymentModes) {
		this.paymentModes = paymentModes;
	}

	public List<PaymentOptionsService> getPaymentOptions() {
		return paymentOptions;
	}

	public void setPaymentOptions(List<PaymentOptionsService> paymentOptions) {
		this.paymentOptions = paymentOptions;
	}

	public List<PaymentModeOptionMappingService> getPaymentModeOptionMapping() {
		return paymentModeOptionMapping;
	}

	public void setPaymentModeOptionMapping(
			List<PaymentModeOptionMappingService> paymentModeOptionMapping) {
		this.paymentModeOptionMapping = paymentModeOptionMapping;
	}

	public List<PaymentAlternteGatewaysService> getPaymentAlternteGateways() {
		return paymentAlternteGateways;
	}

	public void setPaymentAlternteGateways(
			List<PaymentAlternteGatewaysService> paymentAlternteGateways) {
		this.paymentAlternteGateways = paymentAlternteGateways;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public String getPaymentModeLabel() {
		return paymentModeLabel;
	}

	public String getPaymentOptionLabel() {
		return paymentOptionLabel;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public void setPaymentModeLabel(String paymentModeLabel) {
		this.paymentModeLabel = paymentModeLabel;
	}

	public void setPaymentOptionLabel(String paymentOptionLabel) {
		this.paymentOptionLabel = paymentOptionLabel;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "PaymentService [SearchArray=" + SearchArray + ", ErrorCode="
				+ ErrorCode + ", ErrorMessage=" + ErrorMessage + ", userId="
				+ userId + ", amount=" + amount + ", status=" + status
				+ ", gateways=" + gateways + ", processParamters="
				+ processParamters + ", action=" + action + ", data=" + data
				+ ", orderId=" + orderId + ", secretKey=" + secretKey
				+ ", defaultPaymentMode=" + defaultPaymentMode
				+ ", paymentData=" + paymentData + ", paymentGateways="
				+ paymentGateways + ", paymentModes=" + paymentModes
				+ ", paymentOptions=" + paymentOptions
				+ ", paymentModeOptionMapping=" + paymentModeOptionMapping
				+ ", paymentAlternteGateways=" + paymentAlternteGateways
				+ ", paymentOption=" + paymentOption + ", paymentModeLabel="
				+ paymentModeLabel + ", paymentOptionLabel="
				+ paymentOptionLabel + "]";
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

}
