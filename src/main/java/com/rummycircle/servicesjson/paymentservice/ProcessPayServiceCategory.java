package com.rummycircle.servicesjson.paymentservice;

public class ProcessPayServiceCategory {
	private String source;
	private int type_id;
	private String responseparams;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	public String getResponseparams() {
		return responseparams;
	}

	public void setResponseparams(String responseparams) {
		this.responseparams = responseparams;
	}

	@Override
	public String toString() {
		return "ProcessPayServiceCategory [source=" + source + ", type_id="
				+ type_id + ", responseparams=" + responseparams + "]";
	}
}
