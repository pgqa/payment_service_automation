package com.rummycircle.servicesjson.paymentservice;

public class SearchParamService {

	private String orderId;
	private int userId;
	private String loginId;
	private int amount;
	private String creationDateTo;
	private String creationDateFrom;
	private String status;
	private String updateDateTo;
	private String updateDateFrom;

	public String getOrderId() {
		return orderId;
	}

	public int getUserId() {
		return userId;
	}

	public String getLoginId() {
		return loginId;
	}

	public int getAmount() {
		return amount;
	}

	public String getCreationDateTo() {
		return creationDateTo;
	}

	public String getCreationDateFrom() {
		return creationDateFrom;
	}

	public String getStatus() {
		return status;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setCreationDateTo(String creationDateTo) {
		this.creationDateTo = creationDateTo;
	}

	public void setCreationDateFrom(String creationDateFrom) {
		this.creationDateFrom = creationDateFrom;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateDateTo() {
		return updateDateTo;
	}

	public void setUpdateDateTo(String updateDateTo) {
		this.updateDateTo = updateDateTo;
	}

	public String getUpdateDateFrom() {
		return updateDateFrom;
	}

	public void setUpdateDateFrom(String updateDateFrom) {
		this.updateDateFrom = updateDateFrom;
	}

}
