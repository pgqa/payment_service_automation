package com.rummycircle.servicesjson.paymentservice;

public class AuthorizationServices {
	
	public String role;
	public boolean auditRequired;
	public String apiKey;
	public String secretKey;
	public String clientName;
	public boolean edsRequired;
	public boolean active;
	public String merchantName;
	public String status;
	public String updateApiKey;
	public String newRole;
	public String success;
	public String schemaName;

		
	public String getRole() {
		return role;
	}
	public boolean isAuditRequired() {
		return auditRequired;
	}
	public String getApiKey() {
		return apiKey;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public String getClientName() {
		return clientName;
	}
	public boolean isEdsRequired() {
		return edsRequired;
	}
	public boolean isActive() {
		return active;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public String getStatus() {
		return status;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public void setAuditRequired(boolean auditRequired) {
		this.auditRequired = auditRequired;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public void setEdsRequired(boolean edsRequired) {
		this.edsRequired = edsRequired;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUpdateApiKey() {
		return updateApiKey;
	}
	public String getNewRole() {
		return newRole;
	}
	public void setUpdateApiKey(String updateApiKey) {
		this.updateApiKey = updateApiKey;
	}
	public void setNewRole(String newRole) {
		this.newRole = newRole;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public String getSchemaName() {
		return schemaName;
	}
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}
	@Override
	public String toString() {
		return "AuthorizationServices [role=" + role + ", auditRequired="
				+ auditRequired + ", apiKey=" + apiKey + ", secretKey="
				+ secretKey + ", clientName=" + clientName + ", edsRequired="
				+ edsRequired + ", active=" + active + ", merchantName="
				+ merchantName + ", status=" + status + ", updateApiKey="
				+ updateApiKey + ", newRole=" + newRole + ", success="
				+ success + ", schemaName=" + schemaName + "]";
	}
	
	

}
