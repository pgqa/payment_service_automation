package com.rummycircle.servicesjson.paymentservice;


public class ProcessParameter {

	private Data data;
	private InitResponseService action;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public InitResponseService getAction() {
		return action;
	}

	public void setAction(InitResponseService action) {
		this.action = action;
	}

	
}
