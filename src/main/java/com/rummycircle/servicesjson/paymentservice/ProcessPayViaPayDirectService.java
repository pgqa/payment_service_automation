package com.rummycircle.servicesjson.paymentservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProcessPayViaPayDirectService {
	private String ReferenceNo;
	private int Amount;
	private String Status;
	private String source;
	private int type_id;

	@JsonProperty("ReferenceNo")
	public String getReferenceNo() {
		return ReferenceNo;
	}

	@JsonProperty("ReferenceNo")
	public void setReferenceNo(String referenceNo) {
		ReferenceNo = referenceNo;
	}

	@JsonProperty("Amount")
	public int getAmount() {
		return Amount;
	}

	@JsonProperty("Amount")
	public void setAmount(int amount) {
		Amount = amount;
	}

	@JsonProperty("Status")
	public String getStatus() {
		return Status;
	}

	@JsonProperty("Status")
	public void setStatus(String status) {
		Status = status;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

}
