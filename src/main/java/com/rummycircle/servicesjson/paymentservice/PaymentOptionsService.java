package com.rummycircle.servicesjson.paymentservice;

public class PaymentOptionsService {
	private int id;
	private int paymentModeId;
	private String name;
	private String label;
	private long updateTime;
	private int paymentTypeOptionMappingId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPaymentModeId() {
		return paymentModeId;
	}

	public void setPaymentModeId(int paymentModeId) {
		this.paymentModeId = paymentModeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	public int getPaymentTypeOptionMappingId() {
		return paymentTypeOptionMappingId;
	}

	public void setPaymentTypeOptionMappingId(int paymentTypeOptionMappingId) {
		this.paymentTypeOptionMappingId = paymentTypeOptionMappingId;
	}

	@Override
	public String toString() {
		return "PaymentOptionsService [id=" + id + ", paymentModeId="
				+ paymentModeId + ", name=" + name + ", label=" + label
				+ ", updateTime=" + updateTime
				+ ", paymentTypeOptionMappingId=" + paymentTypeOptionMappingId
				+ "]";
	}
}
