package com.rummycircle.servicesjson.paymentservice;

public class PayloadDetails {

	private String gateway;
	private String payTypeOptId;
	private String channelId;
	private String processorCode;
	private String detailsRequired;
	private String merchantName;
	private int minUserId;
	private String status;
	private int percentage;
	private int priority;
	private int[] auditId;
	private String percent;
	private String active;
	private String id;
	private int maxUserid;
	
	public PayloadDetails(String gateway, String payTypeOptId,
			String channelId, String processorCode, String detailsRequired,
			int[] auditId) {

		this.gateway = gateway;
		this.payTypeOptId = payTypeOptId;
		this.channelId = channelId;
		this.processorCode = processorCode;
		this.detailsRequired = detailsRequired;
		this.auditId = auditId;

	}

	public int getMaxUserid() {
		return maxUserid;
	}

	public void setMaxUserid(int maxUserid) {
		this.maxUserid = maxUserid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public int[] getAuditId() {
		return auditId;
	}

	public void setAuditId(int[] auditId) {
		this.auditId = auditId;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public int getMinUserId() {
		return minUserId;
	}

	public void setMinUserId(int minUserId) {
		this.minUserId = minUserId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PayloadDetails() {

	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getPayTypeOptId() {
		return payTypeOptId;
	}

	public void setPayTypeOptId(String payTypeOptId) {
		this.payTypeOptId = payTypeOptId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getProcessorCode() {
		return processorCode;
	}

	public void setProcessorCode(String processorCode) {
		this.processorCode = processorCode;
	}

	public String getDetailsRequired() {
		return detailsRequired;
	}

	public void setDetailsRequired(String detailsRequired) {
		this.detailsRequired = detailsRequired;
	}

}
