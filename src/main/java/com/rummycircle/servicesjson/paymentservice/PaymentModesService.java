package com.rummycircle.servicesjson.paymentservice;

import java.util.List;

public class PaymentModesService {
	private int id;
	private String name;
	private long updateTime;

	private List<AlternatePayloadDetails> payload;
	private Request request;

	public List<AlternatePayloadDetails> getPayload() {
		return payload;
	}

	public void setPayload(List<AlternatePayloadDetails> payload) {
		this.payload = payload;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "PaymentModesService [id=" + id + ", name=" + name
				+ ", updateTime=" + updateTime + "]";
	}
}
