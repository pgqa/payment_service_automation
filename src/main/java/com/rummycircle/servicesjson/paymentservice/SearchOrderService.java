package com.rummycircle.servicesjson.paymentservice;

public class SearchOrderService {

	private String orderId;
	private int userId;
	private String loginId;
	private int amount;
	private String creationDate;
	private String status;
	private String gateway;
	private String updateDate;
	private String updatedBy;
	private String comments;
	private String txnId;

	public String getOrderId() {
		return orderId;
	}

	public int getUserId() {
		return userId;
	}

	public String getLoginId() {
		return loginId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public String getStatus() {
		return status;
	}

	public String getGateway() {
		return gateway;
	}
	
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}



	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;

	}

}
