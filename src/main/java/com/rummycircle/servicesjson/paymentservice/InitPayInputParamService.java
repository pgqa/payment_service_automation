package com.rummycircle.servicesjson.paymentservice;

public class InitPayInputParamService {

	public String card;
	public String data;
	public long rewardStoreItemId;
	public double bonusAmount;
	public boolean firstDepositor;
	public String addCashToken;
	
	public String getAddCashToken() {
		return addCashToken;
	}

	public void setAddCashToken(String addCashToken) {
		this.addCashToken = addCashToken;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public long getRewardStoreItemId() {
		return rewardStoreItemId;
	}

	public double getBonusAmount() {
		return bonusAmount;
	}

	public void setRewardStoreItemId(long rewardStoreItemId) {
		this.rewardStoreItemId = rewardStoreItemId;
	}

	public void setBonusAmount(double bonusAmount) {
		this.bonusAmount = bonusAmount;
	}

	@Override
	public String toString() {
		return "InitPayInputParamService [card=" + card + ", data=" + data + ", rewardStoreItemId=" + rewardStoreItemId
				+ ", bonusAmount=" + bonusAmount + ", firstDepositor=" + firstDepositor + ", addCashToken="
				+ addCashToken + "]";
	}

	public boolean isFirstDepositor() {
		return firstDepositor;
	}

	public void setFirstDepositor(boolean firstDepositor) {
		this.firstDepositor = firstDepositor;
	}

}
