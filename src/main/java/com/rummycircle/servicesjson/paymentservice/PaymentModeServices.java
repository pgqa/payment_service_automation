package com.rummycircle.servicesjson.paymentservice;

import java.util.List;

public class PaymentModeServices {

	private String status;
	private String ErrorCode;
	private String channelId;
	private String percentage;
	private String active;
	private String ErrorMessage;
	private List<UpdatePayloadDetails> payload;
	private Request request;

	public List<UpdatePayloadDetails> getPayload() {
		return payload;
	}

	public void setPayload(List<UpdatePayloadDetails> payload) {
		this.payload = payload;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
