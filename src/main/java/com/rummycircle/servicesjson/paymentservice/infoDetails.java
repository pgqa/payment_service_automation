package com.rummycircle.servicesjson.paymentservice;

public class infoDetails {

	String label;
	String gateway;
	String processorBankCode;
	int id;
	int percentage;
	String paymentOption;
	int gateway_id;
	String detailRequired;

	public String getDetailRequired() {
		return detailRequired;
	}

	public void setDetailRequired(String detailRequired) {
		this.detailRequired = detailRequired;
	}

	public int getGateway_id() {
		return gateway_id;
	}

	public void setGateway_id(int gateway_id) {
		this.gateway_id = gateway_id;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getProcessorBankCode() {
		return processorBankCode;
	}

	public void setProcessorBankCode(String processorBankCode) {
		this.processorBankCode = processorBankCode;
	}

}
