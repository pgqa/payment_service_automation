package com.rummycircle.servicesjson.paymentservice;

public class AddUpdateGMODBInsertResponse {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
