package com.rummycircle.servicesjson.paymentservice;

public class InitFreeChargeType8ResponseService {
	
	public double amount;
	public String metadata;
	public int type_id;
	public String checksum;
	public String merchantTxnId; // OrderID
	public String ErrorCode;
	public String source; // Freecharge
	public String ErrorMessage;
	public String status;
	public String txnId;
	public String orderId;
	
	
	public String getMetadata() {
		return metadata;
	}
	public int getType_id() {
		return type_id;
	}
	public String getChecksum() {
		return checksum;
	}
	public String getMerchantTxnId() {
		return merchantTxnId;
	}
	public String getErrorCode() {
		return ErrorCode;
	}
	public String getSource() {
		return source;
	}
	public String getErrorMessage() {
		return ErrorMessage;
	}
	public String getStatus() {
		return status;
	}
	public String getTxnId() {
		return txnId;
	}
	
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}
	public void setType_id(int type_id) {
		this.type_id = type_id;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public void setMerchantTxnId(String merchantTxnId) {
		this.merchantTxnId = merchantTxnId;
	}
	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
