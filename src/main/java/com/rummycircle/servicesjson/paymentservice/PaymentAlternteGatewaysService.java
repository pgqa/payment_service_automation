package com.rummycircle.servicesjson.paymentservice;

public class PaymentAlternteGatewaysService {
	private int id;
	private int gatewayId;
	private int payTypeOptMapping;
	private int priority;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(int gatewayId) {
		this.gatewayId = gatewayId;
	}

	public int getPayTypeOptMapping() {
		return payTypeOptMapping;
	}

	public void setPayTypeOptMapping(int payTypeOptMapping) {
		this.payTypeOptMapping = payTypeOptMapping;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "PaymentAlternteGatewaysService [id=" + id + ", gatewayId="
				+ gatewayId + ", payTypeOptMapping=" + payTypeOptMapping
				+ ", priority=" + priority + "]";
	}
}
