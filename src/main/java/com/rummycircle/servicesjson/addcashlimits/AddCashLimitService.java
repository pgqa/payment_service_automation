package com.rummycircle.servicesjson.addcashlimits;

public class AddCashLimitService {

	private int currentLimit;
	private String maxLimit;
	private String dailyLimit;
	private String idverificationStatus;
	private int monthlyDeposit;
	private boolean increaseLimitLink;
	private boolean dailyIncreaseLimitLink;
	private boolean paymentModeOptions;
	private boolean today;
	private boolean dailyLimitToday;
	private boolean currentLimitHit;
	private boolean HrsLimitHit;
	private String mobileno;
	private String newlimit;
	private boolean userExists;
	private String newmobileno;
	private String oldmobileno;
	private String usermobile;
	private String error;
	private int errorCode;
	private String errorString;
	private boolean success;
	private String path;
	private int channelId;
	private boolean isLocked;
	private String abCheck;
	private String abAddCashOverLayCheck;
	private String maxAmount;
	private String isUserForced;
	private String docStatus;
	private String kycstatus;
	private String cutoffValue;
	private String idVerificationStatus;
	private String userId;
	private String rewardStoreItemId;
	private String limitedMonthlyFlag;
	private String fromPage;
	private String whichLimitHit;
	private String dailyAddcashLimit;
	private String amtWithdrawPickedUpToday;
	private String amtWithdrawRequestedToday;
	private String accountStatus;
	private String maxLimitHit;
	private String amtWithdrawPickedUpThisMonth;
	private String firstDepositor;
	private String amountWithdrawnToday;
	private String monthlyLimitHit;
	private String amtWithdrawRequestedThisMonth;
	private String showPage;
	private String isMobileCheck;
	private String mobileNo;
	private String cashAddedToday;
	private String monthlyAddCashLimit;
	private String amountWithdrawnThisMonth;
	private String dailyLimitHit;
	private String cashAddedThisMonth;
	private String mobile;
	private String from;
	private String updateDepositLimit;
	private String ticketId;
	private boolean isInstallment;
	private int txnId;
	private String orderId;
	
	private String ErrorCode;
	private String ErrorMessage;
	
	public boolean getisInstallment() {
		return isInstallment;
	}

	public void setisInstallment(boolean isInstallment) {
		this.isInstallment = isInstallment;
	}

	String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public int getTxnId() {
		return txnId;
	}

	public void setTxnId(int txnId) {
		this.txnId = txnId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getUpdateDepositLimit() {
		return updateDepositLimit;
	}

	public void setUpdateDepositLimit(String string) {
		this.updateDepositLimit = string;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getCurrentLimit() {
		return currentLimit;
	}

	public void setCurrentLimit(int currentLimit) {
		this.currentLimit = currentLimit;
	}

	public String getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(String maxLimit) {
		this.maxLimit = maxLimit;
	}

	public String getDailyLimit() {
		return dailyLimit;
	}

	public void setDailyLimit(String dailyLimit) {
		this.dailyLimit = dailyLimit;
	}

	public String getIdverificationStatus() {
		return idverificationStatus;
	}

	public void setIdverificationStatus(String idverificationStatus) {
		this.idverificationStatus = idverificationStatus;
	}

	public int getMonthlyDeposit() {
		return monthlyDeposit;
	}

	public void setMonthlyDeposit(int monthlyDeposit) {
		this.monthlyDeposit = monthlyDeposit;
	}

	public boolean isIncreaseLimitLink() {
		return increaseLimitLink;
	}

	public void setIncreaseLimitLink(boolean increaseLimitLink) {
		this.increaseLimitLink = increaseLimitLink;
	}

	public boolean isDailyIncreaseLimitLink() {
		return dailyIncreaseLimitLink;
	}

	public void setDailyIncreaseLimitLink(boolean dailyIncreaseLimitLink) {
		this.dailyIncreaseLimitLink = dailyIncreaseLimitLink;
	}

	public boolean isPaymentModeOptions() {
		return paymentModeOptions;
	}

	public void setPaymentModeOptions(boolean paymentModeOptions) {
		this.paymentModeOptions = paymentModeOptions;
	}

	public boolean isToday() {
		return today;
	}

	public void setToday(boolean today) {
		this.today = today;
	}

	public boolean isDailyLimitToday() {
		return dailyLimitToday;
	}

	public void setDailyLimitToday(boolean dailyLimitToday) {
		this.dailyLimitToday = dailyLimitToday;
	}

	public boolean isCurrentLimitHit() {
		return currentLimitHit;
	}

	public void setCurrentLimitHit(boolean currentLimitHit) {
		this.currentLimitHit = currentLimitHit;
	}

	public boolean isHrsLimitHit() {
		return HrsLimitHit;
	}

	public void setHrsLimitHit(boolean hrsLimitHit) {
		HrsLimitHit = hrsLimitHit;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getNewlimit() {
		return newlimit;
	}

	public void setNewlimit(String newlimit) {
		this.newlimit = newlimit;
	}

	public boolean isUserExists() {
		return userExists;
	}

	public void setUserExists(boolean userExists) {
		this.userExists = userExists;
	}

	public String getNewmobileno() {
		return newmobileno;
	}

	public void setNewmobileno(String newmobileno) {
		this.newmobileno = newmobileno;
	}

	public String getOldmobileno() {
		return oldmobileno;
	}

	public void setOldmobileno(String oldmobileno) {
		this.oldmobileno = oldmobileno;
	}

	public String getUsermobile() {
		return usermobile;
	}

	public void setUsermobile(String usermobile) {
		this.usermobile = usermobile;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorString() {
		return errorString;
	}

	public void setErrorString(String errorString) {
		this.errorString = errorString;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public boolean isLocked() {
		return isLocked;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public String getAbCheck() {
		return abCheck;
	}

	public void setAbCheck(String abCheck) {
		this.abCheck = abCheck;
	}

	public String getAbAddCashOverLayCheck() {
		return abAddCashOverLayCheck;
	}

	public void setAbAddCashOverLayCheck(String abAddCashOverLayCheck) {
		this.abAddCashOverLayCheck = abAddCashOverLayCheck;
	}

	public String getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(String maxAmount) {
		this.maxAmount = maxAmount;
	}

	public String getIsUserForced() {
		return isUserForced;
	}

	public void setIsUserForced(String isUserForced) {
		this.isUserForced = isUserForced;
	}

	public String getDocStatus() {
		return docStatus;
	}

	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}

	public String getKycstatus() {
		return kycstatus;
	}

	public void setKycstatus(String kycstatus) {
		this.kycstatus = kycstatus;
	}

	public String getCutoffValue() {
		return cutoffValue;
	}

	public void setCutoffValue(String cutoffValue) {
		this.cutoffValue = cutoffValue;
	}

	public String getIdVerificationStatus() {
		return idVerificationStatus;
	}

	public void setIdVerificationStatus(String idVerificationStatus) {
		this.idVerificationStatus = idVerificationStatus;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRewardStoreItemId() {
		return rewardStoreItemId;
	}

	public void setRewardStoreItemId(String rewardStoreItemId) {
		this.rewardStoreItemId = rewardStoreItemId;
	}

	public String getLimitedMonthlyFlag() {
		return limitedMonthlyFlag;
	}

	public void setLimitedMonthlyFlag(String limitedMonthlyFlag) {
		this.limitedMonthlyFlag = limitedMonthlyFlag;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public String getWhichLimitHit() {
		return whichLimitHit;
	}

	public void setWhichLimitHit(String whichLimitHit) {
		this.whichLimitHit = whichLimitHit;
	}

	public String getDailyAddcashLimit() {
		return dailyAddcashLimit;
	}

	public void setDailyAddcashLimit(String dailyAddcashLimit) {
		this.dailyAddcashLimit = dailyAddcashLimit;
	}

	public String getAmtWithdrawPickedUpToday() {
		return amtWithdrawPickedUpToday;
	}

	public void setAmtWithdrawPickedUpToday(String amtWithdrawPickedUpToday) {
		this.amtWithdrawPickedUpToday = amtWithdrawPickedUpToday;
	}

	public String getAmtWithdrawRequestedToday() {
		return amtWithdrawRequestedToday;
	}

	public void setAmtWithdrawRequestedToday(String amtWithdrawRequestedToday) {
		this.amtWithdrawRequestedToday = amtWithdrawRequestedToday;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getMaxLimitHit() {
		return maxLimitHit;
	}

	public void setMaxLimitHit(String maxLimitHit) {
		this.maxLimitHit = maxLimitHit;
	}

	public String getAmtWithdrawPickedUpThisMonth() {
		return amtWithdrawPickedUpThisMonth;
	}

	public void setAmtWithdrawPickedUpThisMonth(String amtWithdrawPickedUpThisMonth) {
		this.amtWithdrawPickedUpThisMonth = amtWithdrawPickedUpThisMonth;
	}

	public String getFirstDepositor() {
		return firstDepositor;
	}

	public void setFirstDepositor(String firstDepositor) {
		this.firstDepositor = firstDepositor;
	}

	public String getAmountWithdrawnToday() {
		return amountWithdrawnToday;
	}

	public void setAmountWithdrawnToday(String amountWithdrawnToday) {
		this.amountWithdrawnToday = amountWithdrawnToday;
	}

	public String getMonthlyLimitHit() {
		return monthlyLimitHit;
	}

	public void setMonthlyLimitHit(String monthlyLimitHit) {
		this.monthlyLimitHit = monthlyLimitHit;
	}

	public String getAmtWithdrawRequestedThisMonth() {
		return amtWithdrawRequestedThisMonth;
	}

	public void setAmtWithdrawRequestedThisMonth(String amtWithdrawRequestedThisMonth) {
		this.amtWithdrawRequestedThisMonth = amtWithdrawRequestedThisMonth;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public String getIsMobileCheck() {
		return isMobileCheck;
	}

	public void setIsMobileCheck(String isMobileCheck) {
		this.isMobileCheck = isMobileCheck;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getCashAddedToday() {
		return cashAddedToday;
	}

	public void setCashAddedToday(String cashAddedToday) {
		this.cashAddedToday = cashAddedToday;
	}

	public String getMonthlyAddCashLimit() {
		return monthlyAddCashLimit;
	}

	public void setMonthlyAddCashLimit(String monthlyAddCashLimit) {
		this.monthlyAddCashLimit = monthlyAddCashLimit;
	}

	public String getAmountWithdrawnThisMonth() {
		return amountWithdrawnThisMonth;
	}

	public void setAmountWithdrawnThisMonth(String amountWithdrawnThisMonth) {
		this.amountWithdrawnThisMonth = amountWithdrawnThisMonth;
	}

	public String getDailyLimitHit() {
		return dailyLimitHit;
	}

	public void setDailyLimitHit(String dailyLimitHit) {
		this.dailyLimitHit = dailyLimitHit;
	}

	public String getCashAddedThisMonth() {
		return cashAddedThisMonth;
	}

	public void setCashAddedThisMonth(String cashAddedThisMonth) {
		this.cashAddedThisMonth = cashAddedThisMonth;
	}

	@Override
	public String toString() {
		return "AddCashLimitService [currentLimit=" + currentLimit + ", maxLimit=" + maxLimit + ", dailyLimit="
				+ dailyLimit + ", idverificationStatus=" + idverificationStatus + ", monthlyDeposit=" + monthlyDeposit
				+ ", increaseLimitLink=" + increaseLimitLink + ", dailyIncreaseLimitLink=" + dailyIncreaseLimitLink
				+ ", paymentModeOptions=" + paymentModeOptions + ", today=" + today + ", dailyLimitToday="
				+ dailyLimitToday + ", currentLimitHit=" + currentLimitHit + ", HrsLimitHit=" + HrsLimitHit
				+ ", mobileno=" + mobileno + ", newlimit=" + newlimit + ", userExists=" + userExists + ", newmobileno="
				+ newmobileno + ", oldmobileno=" + oldmobileno + ", usermobile=" + usermobile + ", error=" + error
				+ ", errorCode=" + errorCode + ", errorString=" + errorString + ", success=" + success + ", path="
				+ path + ", channelId=" + channelId + ", isLocked=" + isLocked + ", abCheck=" + abCheck
				+ ", abAddCashOverLayCheck=" + abAddCashOverLayCheck + ", maxAmount=" + maxAmount + ", isUserForced="
				+ isUserForced + ", docStatus=" + docStatus + ", kycstatus=" + kycstatus + ", cutoffValue="
				+ cutoffValue + ", idVerificationStatus=" + idVerificationStatus + ", userId=" + userId
				+ ", rewardStoreItemId=" + rewardStoreItemId + ", limitedMonthlyFlag=" + limitedMonthlyFlag
				+ ", fromPage=" + fromPage + ", whichLimitHit=" + whichLimitHit + ", dailyAddcashLimit="
				+ dailyAddcashLimit + ", amtWithdrawPickedUpToday=" + amtWithdrawPickedUpToday
				+ ", amtWithdrawRequestedToday=" + amtWithdrawRequestedToday + ", accountStatus=" + accountStatus
				+ ", maxLimitHit=" + maxLimitHit + ", amtWithdrawPickedUpThisMonth=" + amtWithdrawPickedUpThisMonth
				+ ", firstDepositor=" + firstDepositor + ", amountWithdrawnToday=" + amountWithdrawnToday
				+ ", monthlyLimitHit=" + monthlyLimitHit + ", amtWithdrawRequestedThisMonth="
				+ amtWithdrawRequestedThisMonth + ", showPage=" + showPage + ", isMobileCheck=" + isMobileCheck
				+ ", mobileNo=" + mobileNo + ", cashAddedToday=" + cashAddedToday + ", monthlyAddCashLimit="
				+ monthlyAddCashLimit + ", amountWithdrawnThisMonth=" + amountWithdrawnThisMonth + ", dailyLimitHit="
				+ dailyLimitHit + ", cashAddedThisMonth=" + cashAddedThisMonth + "]";
	}

	public boolean isInstallment() {
		return isInstallment;
	}

	public void setInstallment(boolean isInstallment) {
		this.isInstallment = isInstallment;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public void setErrorCode(String ErrorCode) {
		ErrorCode = ErrorCode;
	}
	
	public String getErrorCodeStr() {
		return ErrorCode;
	}

}
