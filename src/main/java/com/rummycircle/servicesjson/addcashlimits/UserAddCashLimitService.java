package com.rummycircle.servicesjson.addcashlimits;

public class UserAddCashLimitService {

	private String limitHit48hr;
	private String firstTxnDateInLast48Hours;
	private String last48HoursLimitChangedDate;
	private int idVerified;
	private int kycVerified;
	private boolean forced;
	private String maxLimit;
	private String dailyLimit;
	private String cashAddedThisMonth;
	private String monthlyAddCashLimit;
	private String linkEnableOrNot;
	private String whichLimit;
	private boolean mobileVerified;
	private String netCashAddedThisMonth;
	private String netCashAddedToday;
	private String grossDepositLimit;
	private String cumulativeAddedCash;
	private int idUploadStatus;
	private int kycUploadStatus;

	public String getLimitHit48hr() {
		return limitHit48hr;
	}

	public String getFirstTxnDateInLast48Hours() {
		return firstTxnDateInLast48Hours;
	}

	public String getLast48HoursLimitChangedDate() {
		return last48HoursLimitChangedDate;
	}

	public int isIdVerified() {
		return idVerified;
	}

	public int isKycVerified() {
		return kycVerified;
	}

	public boolean isForced() {
		return forced;
	}

	public String getMaxLimit() {
		return maxLimit;
	}

	public String getDailyLimit() {
		return dailyLimit;
	}

	public String getCashAddedThisMonth() {
		return cashAddedThisMonth;
	}

	public String getMonthlyAddCashLimit() {
		return monthlyAddCashLimit;
	}

	public String getLinkEnableOrNot() {
		return linkEnableOrNot;
	}

	public String getWhichLimit() {
		return whichLimit;
	}

	public boolean isMobileVerified() {
		return mobileVerified;
	}

	public String getNetCashAddedThisMonth() {
		return netCashAddedThisMonth;
	}

	public String getNetCashAddedToday() {
		return netCashAddedToday;
	}

	public void setLimitHit48hr(String limitHit48hr) {
		this.limitHit48hr = limitHit48hr;
	}

	public void setFirstTxnDateInLast48Hours(String firstTxnDateInLast48Hours) {
		this.firstTxnDateInLast48Hours = firstTxnDateInLast48Hours;
	}

	public void setLast48HoursLimitChangedDate(
			String last48HoursLimitChangedDate) {
		this.last48HoursLimitChangedDate = last48HoursLimitChangedDate;
	}

	public void setIdVerified(int idVerified) {
		this.idVerified = idVerified;
	}

	public void setKycVerified(int kycVerified) {
		this.kycVerified = kycVerified;
	}

	public void setForced(boolean forced) {
		this.forced = forced;
	}

	public void setMaxLimit(String maxLimit) {
		this.maxLimit = maxLimit;
	}

	public void setDailyLimit(String dailyLimit) {
		this.dailyLimit = dailyLimit;
	}

	public void setCashAddedThisMonth(String cashAddedThisMonth) {
		this.cashAddedThisMonth = cashAddedThisMonth;
	}

	public void setMonthlyAddCashLimit(String monthlyAddCashLimit) {
		this.monthlyAddCashLimit = monthlyAddCashLimit;
	}

	public void setLinkEnableOrNot(String linkEnableOrNot) {
		this.linkEnableOrNot = linkEnableOrNot;
	}

	public void setWhichLimit(String whichLimit) {
		this.whichLimit = whichLimit;
	}

	public void setMobileVerified(boolean mobileVerified) {
		this.mobileVerified = mobileVerified;
	}

	public void setNetCashAddedThisMonth(String netCashAddedThisMonth) {
		this.netCashAddedThisMonth = netCashAddedThisMonth;
	}

	public void setNetCashAddedToday(String netCashAddedToday) {
		this.netCashAddedToday = netCashAddedToday;
	}

	public String getGrossDepositLimit() {
		return grossDepositLimit;
	}

	public void setGrossDepositLimit(String grossDepositLimit) {
		this.grossDepositLimit = grossDepositLimit;
	}

	public String getCumulativeAddedCash() {
		return cumulativeAddedCash;
	}

	public void setCumulativeAddedCash(String cumulativeAddedCash) {
		this.cumulativeAddedCash = cumulativeAddedCash;
	}

	public int getIdUploadStatus() {
		return idUploadStatus;
	}

	public void setIdUploadStatus(int idUploadStatus) {
		this.idUploadStatus = idUploadStatus;
	}

	public int getKycUploadStatus() {
		return kycUploadStatus;
	}

	public void setKycUploadStatus(int kycUploadStatus) {
		this.kycUploadStatus = kycUploadStatus;
	}

}
