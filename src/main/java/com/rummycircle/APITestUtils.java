package com.rummycircle;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.FormEncodingType;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.rummycircle.utils.config.ConfigManager;
import com.rummycircle.utils.database.DatabaseManager;
import com.rummycircle.utils.testutils.BaseTest;

public class APITestUtils extends BaseTest {

	public static String sendGETRequestUsingWebClient(String requestURL,
			List<NameValuePair> queryParam) {

		WebClient webClient = new WebClient(BrowserVersion.CHROME);
		String value = "";

		webClient.getCache().clear();
		webClient.getCookieManager().clearCookies();
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setUseInsecureSSL(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		URL url;

		try {
			url = new URL(requestURL);
			WebRequest requestSettings = new WebRequest(url, HttpMethod.GET);

			if (queryParam != null) {
				requestSettings.setRequestParameters(queryParam);
				requestSettings.setEncodingType(FormEncodingType.URL_ENCODED);
			}

			Page redirectPage = webClient.getPage(requestSettings);
			Thread.sleep(5000);
			value = redirectPage.getWebResponse().getContentAsString();

		} catch (FailingHttpStatusCodeException | IOException
				| InterruptedException e) {
			e.printStackTrace();
		}
		webClient.close();
		return value;
	}

	public String createJSessionId(String userId) {
		return "ga24x7_jsessionid=" + getRandomString(6) + ":" + userId;
	}

	public String getTicketFormattedDate(String date) throws ParseException {
		SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date parsedDate = sdfSource.parse(date);
		SimpleDateFormat sdfDestination = new SimpleDateFormat(
				"MMM-dd, yyyy hh:mm aaa");
		String strDate = sdfDestination.format(parsedDate);
		return strDate;
	}

	public String getBonusFormattedDate(String date) throws ParseException {
		SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date parsedDate = sdfSource.parse(date);
		SimpleDateFormat sdfDestination = new SimpleDateFormat("dd MMM yyyy");
		String strDate = sdfDestination.format(parsedDate);
		return strDate;
	}

	public static String sendReqAndGetRespHeaderUsingWebClient(
			String requestURL, List<NameValuePair> queryParam,
			HttpMethod method, String headerKey) {

		WebClient webClient = new WebClient(BrowserVersion.CHROME);
		String value = "";

		webClient.getCache().clear();
		webClient.getCookieManager().clearCookies();
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setUseInsecureSSL(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		URL url;

		try {
			url = new URL(requestURL);
			WebRequest requestSettings = new WebRequest(url, method);

			if (queryParam != null) {
				requestSettings.setRequestParameters(queryParam);
				requestSettings.setEncodingType(FormEncodingType.URL_ENCODED);
			}

			Page redirectPage = webClient.getPage(requestSettings);

			Thread.sleep(5000);
			List<NameValuePair> temp = redirectPage.getWebResponse()
					.getResponseHeaders();
			value = extractSpecificHeaderValues(temp, headerKey);

		} catch (FailingHttpStatusCodeException | IOException
				| InterruptedException e) {
			e.printStackTrace();
		}

		webClient.close();
		return value;
	}

	private static String extractSpecificHeaderValues(
			List<NameValuePair> headers, String headerKey) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < headers.size(); i++) {
			if (headers.get(i).getName().equals(headerKey)) {
				sb.append(headers.get(i).getValue() + ";");
			}
		}

		return sb.toString();
	}

	public static String sendReqAndGetRespHeaderUsingWebClient(
			String requestURL, String json, HttpMethod method,
			Map<String, String> headers, String headerKey) {

		WebClient webClient = new WebClient(BrowserVersion.CHROME);
		String value = "";

		webClient.getCache().clear();
		webClient.getCookieManager().clearCookies();
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setUseInsecureSSL(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		URL url;

		try {
			url = new URL(requestURL);
			WebRequest requestSettings = new WebRequest(url, method);

			if (json != null) {
				requestSettings.setRequestBody(json);
				requestSettings.setEncodingType(FormEncodingType.URL_ENCODED);

			}
			requestSettings.setAdditionalHeaders(headers);

			Page redirectPage = webClient.getPage(requestSettings);
			Thread.sleep(5000);
			System.out.println(redirectPage.getWebResponse().getStatusCode());

			System.out
					.println(redirectPage.getWebResponse().getStatusMessage());
			System.out.println(redirectPage.getWebResponse()
					.getContentAsString());

			List<NameValuePair> temp = redirectPage.getWebResponse()
					.getResponseHeaders();
			value = extractSpecificHeaderValues(temp, headerKey);

		} catch (FailingHttpStatusCodeException | IOException
				| InterruptedException e) {
			e.printStackTrace();
		}
		webClient.close();
		return value;
	}

	/**
	 * Get the current year
	 * 
	 * @return int
	 */
	public int getCurrentYear() {
		int CURRENT_YEAR = Calendar.getInstance().get(Calendar.YEAR);
		return CURRENT_YEAR;
	}

	public static String getMRCFusionProxyURL() throws Exception {
		return !getFusionPort().isEmpty() ? getFusionProtocol() + "://"
				+ getMRCFusionHost() + ":" + getFusionPort()
				: getFusionProtocol() + "://" + getMRCFusionHost();
	}

	public static String getRCFusionProxyURL() throws Exception {
		return !getFusionPort().isEmpty() ? getFusionProtocol() + "://"
				+ getRCFusionHost() + ":" + getFusionPort()
				: getFusionProtocol() + "://" + getRCFusionHost();
	}

	public static String getFusionProtocol() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.FUSION_PROXY_PROTOCOL.getValue());
	}

	public static String getRCFusionHost() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.FUSION_PROXY_HOST.getValue());
	}

	public static String getMRCFusionHost() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.FUSION_PROXY_MOBILE_HOST.getValue());
	}

	public static String getFusionPort() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.FUSION_PROXY_PORT.getValue());
	}

	public static String getLoginProtocol() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.LOGIN_PROTOCOL.getValue());

	}

	public static String getLoginHost() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.LOGIN_HOST.getValue());

	}

	public static String getLoginPort() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.LOGIN_PORT.getValue());

	}

	public static String getFusionProxySetupHost() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.FUSIONPROXYSETUP_HOST.getValue());

	}

	public static String getFusionProxySetupProtocol() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.FUSIONPROXYSSETUP_PROTOCOL.getValue());

	}

	public static String getCreateCaptchaProtocol() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.CREATECAPTCHA_PROTOCOL.getValue());

	}

	public static String getCreateCaptchaHost() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.CREATECAPTCHA_HOST.getValue());

	}

	public static String getCreateCaptchPort() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.CREATECAPTCHA_PORT.getValue());

	}

	public static String getRedisIp() throws Exception {
		return ConfigManager.getInstance().getString(
				LocalConfigProperties.REDIS_IP.getValue());

	}

	/**
	 * Get UserId from Username
	 * 
	 * @param userName
	 * @return userId
	 */
	public static String getUserId(String userName) {
		String userIdQuery = "select * from users where loginid='" + userName
				+ "'";
		String userIdFromDB = DatabaseManager.executeQuery(userIdQuery)[0][0]
				.toString();
		return userIdFromDB;
	}

	public static double getMaxAmount(int userId) {
		// MaxAmount Logic :
		double maxAmount = 0D;
		String query = "select current_limit,max_limit,daily_limit,monthly_deposit,total_deposited from deposit_limit where user_id="
				+ userId;
		Object data[][] = DatabaseManager.executeQuery(query);
		double maxLimit = Double.parseDouble(data[0][1].toString());
		double monthlyDeposited = Double.parseDouble(data[0][3].toString());
		double totalDeposited = Double.parseDouble(data[0][4].toString());
		double AmountWithdrawnThisMonth = getAmountWithdrawnThisMonth(userId);
		double amtWithdrawRequestedThisMonth = getWithdrawAmountRequestedThisMonth(userId);
		double amtWithdrawPckdUpThisMonth = getWithdrawAmountPickedUpThisMonth(userId);
		maxAmount = maxLimit - monthlyDeposited + AmountWithdrawnThisMonth
				+ amtWithdrawRequestedThisMonth + amtWithdrawPckdUpThisMonth;
		double currentLimit = getMonthlyLimit(userId);

		if (maxAmount >= currentLimit && is48LimitHit(userId)) {
			maxAmount = currentLimit;
		} else if (maxAmount < 0) {
			maxAmount = 0;
		}
		int kycStatus = getKYCStatus(userId);

		if (kycStatus != 0 && kycStatus < 4) {
			double balCumulativeCash = getBalCUmulativeCash(totalDeposited);
			if (balCumulativeCash >= 0 && balCumulativeCash < maxAmount) {
				maxAmount = balCumulativeCash;
			}
		}

		double available24Limit = 0D;
		double dailyLimit = Double.parseDouble(data[0][2].toString());
		double Last24hoursdeposited = getDailyDeposited(userId);
		double AmountWithdrawnToday = getAmountWithdrawnToday(userId);
		double getAmtWithdrawRequestedToday = getWithdrawAmountRequestedToday(userId);
		double AmtWithdrawPickedUpToday = getWithdrawAmountPickedUpToday(userId);
		available24Limit = dailyLimit - Last24hoursdeposited
				+ AmountWithdrawnToday + getAmtWithdrawRequestedToday
				+ AmtWithdrawPickedUpToday;
		if (available24Limit >= dailyLimit) {
			available24Limit = dailyLimit;
		} else if (available24Limit < 0) {
			available24Limit = 0;
		}
		if (maxAmount >= available24Limit) {
			maxAmount = available24Limit;
		}
		return maxAmount;
	}

	public static boolean is48LimitHit(int userId) {
		String query = String
				.format("select if(count(*)=0,false,true) FROM deposit_limit_logs depositLogs WHERE depositLogs.user_id = %d AND depositLogs.limit_increased_user ='Y' and depositLogs.curr_date > update_date order by depositLogs.id desc limit 1;",
						userId);
		return Boolean.parseBoolean(DatabaseManager.executeQuery(query)[0][0]
				.toString());
	}

	public static double getAvailableAmount(int userId) {
		// availableAmount Logic :
		double availableLimit = 0D;
		String query = "select current_limit,max_limit,daily_limit,monthly_deposit,total_deposited from deposit_limit where user_id="
				+ userId;
		Object[][] data = DatabaseManager.executeQuery(query);
		double currentLimit = Double.parseDouble(data[0][0].toString());
		double monthlyDeposited = Double.parseDouble(data[0][3].toString());
		double AmountWithdrawnThisMonth = getAmountWithdrawnThisMonth(userId);
		double amtWithdrawRequestedThisMonth = getWithdrawAmountRequestedThisMonth(userId);
		double amtWithdrawPckdUpThisMonth = getWithdrawAmountPickedUpThisMonth(userId);

		availableLimit = currentLimit - monthlyDeposited
				+ AmountWithdrawnThisMonth + amtWithdrawRequestedThisMonth
				+ amtWithdrawPckdUpThisMonth;

		if (availableLimit >= currentLimit) {
			availableLimit = currentLimit;
		} else if (availableLimit < 0) {
			availableLimit = 0;
		}

		int kycStatus = getKYCStatus(userId);

		double available24Limit = 0D;
	
		double totalDeposit = Double.parseDouble(data[0][4].toString());
		double DailyLimit = Double.parseDouble(data[0][2].toString());

		if (kycStatus != 0 & kycStatus < 4) {
			double balCumulativeCash = getBalCUmulativeCash(totalDeposit);
			if (balCumulativeCash >= 0 & balCumulativeCash < availableLimit) {
				availableLimit = balCumulativeCash;
			}
		}
		double dailyLimit = Double.parseDouble(data[0][2].toString());
		double Last24hoursdeposited = getDailyDeposited(userId);
		double AmountWithdrawnToday = getAmountWithdrawnToday(userId);
		double getAmtWithdrawRequestedToday = getWithdrawAmountRequestedToday(userId);
		double AmtWithdrawPickedUpToday = getWithdrawAmountPickedUpToday(userId);
		available24Limit = dailyLimit - Last24hoursdeposited
				+ AmountWithdrawnToday + getAmtWithdrawRequestedToday
				+ AmtWithdrawPickedUpToday;
		
		if (available24Limit >= DailyLimit) {
			available24Limit = DailyLimit;
		} else if (available24Limit < 0) {
			available24Limit = 0;
		}
		if (availableLimit >= available24Limit) {
			availableLimit = available24Limit;
		}
		return availableLimit;
	}

	public static double getMaxLimit(int userId) {
		String query = "select if(count(max_limit)=0,0,max_limit) as max_limit from deposit_limit where user_id="
				+ userId;
		Object[][] data = DatabaseManager.executeQuery(query);
		return Double.parseDouble(data[0][0].toString());
	}

	public static double getMonthlyLimit(int userId) {
		String query = "select if(count(current_limit)=0,0,current_limit) as max_limit from deposit_limit where user_id="
				+ userId;
		Object[][] data = DatabaseManager.executeQuery(query);
		return Double.parseDouble(data[0][0].toString());
	}

	public static double getMonthlyDeposited(int userId) {
		// String query = String
		// .format("select if(count(amount)=0,0,sum(amount)) from gateway_transactions where user_id=%d and MONTH(update_date)=MONTH(CURDATE()) and YEAR(update_date)=YEAR(CURDATE());",
		// userId);
		String query = String.format(
				"select monthly_deposit from deposit_limit where user_id=%d",
				userId);
		return Double.parseDouble(DatabaseManager.executeQuery(query)[0][0]
				.toString());
	}

	public static double getAmountWithdrawnThisMonth(int userId) {
		String query = String
				.format("select if(count(amount)=0,0,sum(amount)) from withdraw_request where user_id=%d and status=3 and MONTH(date)=MONTH(CURDATE()) and YEAR(date)=YEAR(CURDATE());",
						userId);
		return Double.parseDouble(DatabaseManager.executeQuery(query)[0][0]
				.toString());
	}

	public static double getWithdrawAmountRequestedThisMonth(int userId) {
		String query = String
				.format("select if(count(amount)=0,0,sum(amount)) from withdraw_request where user_id=%d and status=1 and MONTH(date)=MONTH(CURDATE()) and YEAR(date)=YEAR(CURDATE());",
						userId);
		return Double.parseDouble(DatabaseManager.executeQuery(query)[0][0]
				.toString());
	}

	public static double getWithdrawAmountPickedUpThisMonth(int userId) {
		String query = String
				.format("select if(count(amount)=0,0,sum(amount)) from withdraw_request where user_id=%d and status=8 and MONTH(date)=MONTH(CURDATE()) and YEAR(date)=YEAR(CURDATE());",
						userId);
		return Double.parseDouble(DatabaseManager.executeQuery(query)[0][0]
				.toString());
	}

	public static double getDailyLimit(int userId) {
		String query = "select if(count(daily_limit)=0,0,daily_limit) as max_limit from deposit_limit where user_id="
				+ userId;
		Object[][] data = DatabaseManager.executeQuery(query);
		return Double.parseDouble(data[0][0].toString());
	}

	public static double getDailyDeposited(int userId) {
		String query = String
				.format("select if(count(amount)=0,0,sum(amount)) from gateway_transactions where user_id=%d and DATE(update_date)=CURDATE();",
						userId);
		return Double.parseDouble(DatabaseManager.executeQuery(query)[0][0]
				.toString());
	}

	public static double getAmountWithdrawnToday(int userId) {
		String query = String
				.format("select if(count(amount)=0,0,sum(amount)) from withdraw_request where user_id=%d and status=3 and DATE(date)=CURDATE();",
						userId);
		return Double.parseDouble(DatabaseManager.executeQuery(query)[0][0]
				.toString());
	}

	public static double getWithdrawAmountRequestedToday(int userId) {
		String query = String
				.format("select if(count(amount)=0,0,sum(amount)) from withdraw_request where user_id=%d and status=1 and DATE(date)=CURDATE();",
						userId);
		return Double.parseDouble(DatabaseManager.executeQuery(query)[0][0]
				.toString());
	}

	public static double getWithdrawAmountPickedUpToday(int userId) {
		String query = String
				.format("select if(count(amount)=0,0,sum(amount)) from withdraw_request where user_id=%d and status=8 and DATE(date)=CURDATE();",
						userId);
		return Double.parseDouble(DatabaseManager.executeQuery(query)[0][0]
				.toString());
	}

	public static int getKYCStatus(int userId) {
		String query = String.format(
				"select kyc_status from user_preference where user_id=%d",
				userId);
		return Integer.parseInt(DatabaseManager.executeQuery(query)[0][0]
				.toString());
	}

	public static double getBalCUmulativeCash(double totalDeposit) {
		String query = "select cumulative_deposit_amount from cumulative_limits_details where mod_value=0;";
		double balCum = Double
				.parseDouble(DatabaseManager.executeQuery(query)[0][0]
						.toString());
		if (totalDeposit >= balCum)
			balCum = 0;
		else
			balCum = balCum - totalDeposit;
		return balCum;

	}
public static void main(String[] args) {
	System.out.println(getAvailableAmount(343109));
}
}
