package com.rummycircle;

import com.rummycircle.utils.database.DatabaseManager;
import com.rummycircle.utils.reporting.extent.Logger;

public class LimitsUtil {

	private static Logger Log = Logger.getLogger();

	public static String getUserMonthlyDepositForThisMonth(String userId) {

		String query = String.format(
				"select monthly_deposit from deposit_limit where user_id = %s",
				userId);
		Log.info(" Query >>>> " + query);
		String ML = DatabaseManager.executeQuery(query)[0][0].toString();

		return ML;
	}

	public static String getUserMonthlyLimit(String userId) {

		String query = String.format(
				"select current_limit from deposit_limit where user_id = %s",
				userId);
		Log.info(" Query >>>> " + query);
		String ML = DatabaseManager.executeQuery(query)[0][0].toString();

		return ML;
	}

	public static String getUserDailyLimit(String userId) {

		String query = String.format(
				"select daily_limit from deposit_limit where user_id = %s",
				userId);
		Log.info(" Query >>>> " + query);
		String DL = DatabaseManager.executeQuery(query)[0][0].toString();

		return DL;
	}

	public static String getUserMaxMonthlyLimit(String userId) {

		String query = String.format(
				"select max_limit from deposit_limit where user_id = %s",
				userId);
		Log.info(" Query >>>> " + query);
		String MML = DatabaseManager.executeQuery(query)[0][0].toString();

		return MML;
	}

	public static Object[][] getKycUserDailyLimit() {

		String query = "select d.daily_limit, d.user_id ,u.loginid from deposit_limit d, users u, user_preference up where up.kyc_status = 4 and u.passwd = 'ebaa5e41bfc12f9f329cfcbc66639f68' and u.user_id = d.user_id  and u.user_id = up.user_id order by d.user_id desc limit 1;";
		Log.info(" Query >>>> " + query);
		Object[][] dbValue = DatabaseManager.executeQuery(query);

		return dbValue;

	}

	public static Object[][] getKycUserMaxMonthlyLimit() {

		String query = "select d.max_limit, d.user_id ,u.loginid from deposit_limit d, users u, user_preference up where up.kyc_status = 4 and u.passwd = 'ebaa5e41bfc12f9f329cfcbc66639f68' and u.user_id = d.user_id  and u.user_id = up.user_id order by d.user_id desc limit 1;";
		Log.info(" Query >>>> " + query);
		Object[][] dbValue = DatabaseManager.executeQuery(query);

		return dbValue;

	}

	public static Object[][] getUserLimitDetails(String userID) {

		String query = String
				.format("select current_limit, max_limit, daily_limit, total_deposited from deposit_limit where user_id = %s;",
						userID);
		Log.info(" Query >>>> " + query);
		Object[][] dbValue = DatabaseManager.executeQuery(query);

		return dbValue;
	}

}
