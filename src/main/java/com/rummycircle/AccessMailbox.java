package com.rummycircle;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;

import org.eclipse.jetty.util.log.Log;

/**
 * This class would access any mail and extract the body of the mail which in intended to be extracted
 * on the basis of SUBJECT or FROM
 * 
 * @author viral.singh@games24x7.com
 */
public class AccessMailbox {
	
	private String hostName = "smtp.gmail.com";
	private int hostPort = 465;
    private String userName = null;
    private String password  = null;
    private String folder = "inbox";

	public AccessMailbox (String username, String password) {
    	setUserName(username);
    	setPassword(password);
    }
    
    public AccessMailbox (String host, String username, String password) {
    	setHostName(host);
    	setUserName(username);
    	setPassword(password);
    }
    
    public AccessMailbox (String host, int port, 
    		String username, String password) {
    	setHostName(host);
    	setHostPort(port);
    	setUserName(username);
    	setPassword(password);
    }

    public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public int getHostPort() {
		return hostPort;
	}

	public void setHostPort(int hostPort) {
		this.hostPort = hostPort;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    
    public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	/**
	 * This method will extract the mail body if the Subject of mail matches
	 * 
	 * @param expSubject - expected Subject of the mail
	 * @return
	 */
	public String getMailWithSubject (String expSubject) {
		String mailBody = null;		
		mailBody = getLatestMailWithSubjectOrFrom(expSubject, null);		
		return mailBody;
	}
	
	/**
	 * This method will extract the mail body if the 'From' field of the mail contains expFrom.
	 * 
	 * @param expFrom - expected 'From' string (can be email address or display name)
	 * @return
	 */
	public String getLatestMailFrom (String expFrom) {
		String mailBody = null;		
		mailBody = getLatestMailWithSubjectOrFrom(null, expFrom);		
		return mailBody;
	}
	
	/**
	 * This method will return the latest mail that either matches the Subject or
	 * the 'From' field of the mail.
	 * 
	 * @param expSubject - expected Subject of the mail
	 * @param expFrom - expected 'From' string (can be email address or display name)
	 * @return
	 */
	public String getLatestMailWithSubjectOrFrom (String expSubject, String expFrom) {
		String mailBody = null;		
		mailBody = getMailWithSubjectOrFrom(expSubject, expFrom);		
		return mailBody;
	}
    
	/**
	 * This method will return the latest mail that either matches the Subject or
	 * the 'From' field of the mail.
	 * 
	 * @param expSubject - expected Subject of the mail
	 * @param expFrom - expected 'From' string (can be email address or display name)
	 * @return
	 */
	private String getMailWithSubjectOrFrom (String expSubject, String expFrom) {
		Properties props = new Properties();        
        String messageBody = null;

        props.put("mail.transport.protocol", "smtps");
        props.put("mail.smtps.host", hostName);
        props.put("mail.smtps.auth", "true");
        
		try {			
			Session session = Session.getDefaultInstance(props);

			Store store = session.getStore("imaps");
			store.connect(hostName, userName, password);
	
			Folder inbox = store.getFolder(folder);
			inbox.open(Folder.READ_ONLY);
			int messageCount = inbox.getMessageCount();
	
			System.out.println("Total Messages:- " + messageCount);
	
			Message[] messages = inbox.getMessages();	
			
			for (int i = messageCount -1; i > messageCount - 20; i--) {
			    
			    if ((expSubject != null 
			    		&& messages[i].getSubject().equalsIgnoreCase(expSubject))
			    		|| (expFrom != null 
			    		&& messages[i].getFrom()[0].toString().contains(expFrom))) {
			    	
			    	Object content = messages[i].getContent();  
                    
                    if (content instanceof String) {  
                        messageBody = org.jsoup.Jsoup.parse((String)content).text();                        
                    } else if (content instanceof MimeMultipart) {  
                        messageBody = getTextFromMimeMultipart(
                                (MimeMultipart)content); 
                    }
			    	break ;
			    }
			}
			
			inbox.close(true);
			store.close();
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		return messageBody;
	}
	
	/**
	 * This method extract text from the mail body which is in multipart and adds it to the
	 * mail body.
	 * 
	 * @param mimeMultipart - content of the mail
	 * @return
	 * @throws Exception
	 */
	private String getTextFromMimeMultipart(
	        MimeMultipart mimeMultipart) throws Exception{
	    String result = "";
	    int count = mimeMultipart.getCount();
	    
	    for (int i = 0; i < count; i++) {
	        BodyPart bodyPart = mimeMultipart.getBodyPart(i);
	        if (bodyPart.isMimeType("text/plain")) {
	            result = result + "\n" + bodyPart.getContent();
	            break; // without break same text appears twice in my tests
	        } else if (bodyPart.isMimeType("text/html")) {
	            String html = (String) bodyPart.getContent();
	            result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
	        } else if (bodyPart.getContent() instanceof MimeMultipart){
	            result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
	        }
	    }
	    
	    return result;
	}
}