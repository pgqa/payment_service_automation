package com.rummycircle;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.testng.Assert;

import com.jayway.jsonpath.JsonPath;
import com.rummycircle.TournamentUtility.TournamentDetails;
import com.rummycircle.restclient.HTTPHeaders;
import com.rummycircle.restclient.HTTPMethod;
import com.rummycircle.restclient.HTTPRequest;
import com.rummycircle.restclient.HTTPResponse;
import com.rummycircle.restclient.RESTAssuredClient;
import com.rummycircle.utils.testutils.NewBaseTest;
import com.rummycircle.utils.testutils.PropertyReader;

public class TournamentUtility extends NewBaseTest {

	public enum TournamentDetails {
		TOURNAMENTID, TOURNAMENTNAME, ENTRYFEE, TOURNAMENTTYPE, PRIZETYPE, TOURNAMENTSTATUS, TOURNAMENTSTARTTIME, TOURNAMENTENDTIME
	};

	public static Properties customProp = PropertyReader
			.loadCustomProperties("custom.properties");
	
	public Map<TournamentDetails, String> CreateTournamentUsingPostAPI(
			int entryfee, int regCondId, String tournamentType,
			int prizeStructureId, int tableSize, int rewardPoints,
			int maxPlayers, int ticketId, String pType, String entryType,
			int regStartTimeDays, int regEndTimeDays, int trnmntStartTimeDays) {
		Properties customProp = PropertyReader
				.loadCustomProperties("custom.properties");
		DateTimeFormatter dtf = DateTimeFormatter
				.ofPattern("dd-MMM-yyyy HH:mm:ss");

		Random rand = new Random();
		// int entryFee = rand.nextInt(50)+1;
		String parentTrnFlag = getParentTrnFlag(tournamentType);
		String tournamentName = getRandomString(2).concat("_automation_trn");

		String timeOfRelease = dtf.format(LocalDateTime.now());
		String regStartTime = dtf.format(LocalDateTime.now().plusDays(
				regStartTimeDays));
		String regEndTime = dtf.format(LocalDateTime.now().plusDays(
				regEndTimeDays));
		String tournamentStartTime = dtf.format(LocalDateTime.now().plusDays(
				trnmntStartTimeDays));

		String requestBody = "{\"context\":{\"loginId\":\"qa.automation3\",\"source\":\"WebAdminPortal\",\"time\":1495092778095,\"channelId\":1\n },\n \"value\":{\"entryFee\":\""
				+ entryfee
				+ "\",\"parent\":\""
				+ parentTrnFlag
				+ "\",\"timeOfRelease\":\""
				+ timeOfRelease
				+ "\",\"regCondId\":\""
				+ regCondId
				+ "\",\"ptype\":\""
				+ pType
				+ "\",\"tournamentType\":\""
				+ tournamentType
				+ "\",\"cardsTobeDelt\":13,\"gamePlay\":2,\"prizeStuctureID\":\""
				+ prizeStructureId
				+ "\",\"jokers\":2,\"tournamentName\":\""
				+ tournamentName
				+ "\",\"tableSize\":\""
				+ tableSize
				+ "\",\"cardPerSequence\":3,\"tournamentDesc\":\"This tournament is created by automation script\",\"rewardPoints\":\""
				+ rewardPoints
				+ "\",\"showOfflineDetails\":false,\"parenttournname\":\"N/A\",\"autoEmailLosers\":false,\"serviceFeeType\":\"Percentage\",\"entryType\":\""
				+ entryType
				+ "\", \"brandName\":\"N/A\",\"maxPlayers\":\""
				+ maxPlayers
				+ "\",\"languages\":[   {      \"language\":\"English\",      \"id\":1,      \"templateID\":1500062232   }],\"minPlayers\":2,\"deckToUse\":2,\"timeBetweenDeals\":10,\"offlineTournament\":false,\"displayTabID\":33,\"regEndTime\":\""
				+ regEndTime
				+ "\",\"prizeDescription\":\"p1\",\"regConditionDescription\":\"R1\",\"regStartTime\":\""
				+ regStartTime
				+ "\",\"breakID\":1,\"breakName\":\"System Default\",\"tournamentStartTime\":\""
				+ tournamentStartTime
				+ "\",\"moveTime\":10000,\"tournamentNameType\":\"Manual\",\"customVisibility\":\"false\",\"channelVisibility\":[   1,   2,   3,   4],\"testTournamentFlag\":\"false\",\"timeBetweenRound\":10,\"autoEmail\":true,\"ticketId\":\""
				+ ticketId + "\", \"showOnTop\":false\n }\n}";

		System.out.println(requestBody);

		String path = customProp.getProperty(
				"createTournament.postreq.protocol").concat("://")
				+ customProp.getProperty("createTournament.postreq.host")
				+ ":"
				+ customProp.getProperty("createTournament.postreq.port")
				+ "/admin/api/tournament/create";
		System.out.println(path);
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json;charset=UTF-8");

		HTTPRequest request = new HTTPRequest(headers, requestBody);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc.sendRequest(HTTPMethod.POST, path, request);
		System.out
				.println("response--------------------------------------->>>response"
						+ response.getBody().getBodyText());
		Assert.assertTrue(response.getStatusCode() == 200,
				"status Code expected is 200. However Status Code received:"
						+ response.getStatusCode());

		Map<TournamentDetails, String> tournamentData = new HashMap<TournamentDetails, String>();

		tournamentData.put(TournamentDetails.TOURNAMENTID, (JsonPath.read(
				response.getBody().getBodyText(), "$.value.trnmntId"))
				.toString());
		tournamentData.put(TournamentDetails.TOURNAMENTNAME, (JsonPath.read(
				response.getBody().getBodyText(), "$.value.tournamentName"))
				.toString());
		tournamentData.put(TournamentDetails.ENTRYFEE, (JsonPath.read(response
				.getBody().getBodyText(), "$.value.entryFee")).toString());
		tournamentData.put(TournamentDetails.TOURNAMENTTYPE,
				(getTrnType(JsonPath.read(response.getBody().getBodyText(),
						"$.value.tournamentType"))).toString());
		tournamentData.put(TournamentDetails.PRIZETYPE, (getPrizeType(JsonPath
				.read(response.getBody().getBodyText(), "$.value.ptype")))
				.toString());
		tournamentData.put(TournamentDetails.TOURNAMENTSTATUS,
				(getTrnStatus(JsonPath.read(response.getBody().getBodyText(),
						"$.value.status"))).toString());
		tournamentData.put(TournamentDetails.TOURNAMENTSTARTTIME, (JsonPath
				.read(response.getBody().getBodyText(),
						"$.value.tournamentStartTime")).toString());

		return tournamentData;
	}

	public Map<TournamentDetails, String> createBackEndTournament(int entryfee,
			String prizeStructureId, boolean isBackEnd, boolean isInstallment,
			String trnName, String parentId, int serviceFee, int parentPercent) {
		Properties customProp = PropertyReader
				.loadCustomProperties("custom.properties");
		DateTimeFormatter dtf = DateTimeFormatter
				.ofPattern("dd-MMM-yyyy HH:mm:ss");
		String tournamentName = getRandomString(2).concat("_automation_trn");

		String timeOfRelease = dtf.format(LocalDateTime.now());
		String regStartTime = dtf.format(LocalDateTime.now());
		String regEndTime = dtf.format(LocalDateTime.now().plusDays(1));
		String tournamentStartTime = dtf
				.format(LocalDateTime.now().plusDays(2));

		String requestBody = "{	\"context\": {		\"loginId\": \"ashutosh\",		\"source\": \"WebAdminPortal\",		\"time\": 1495092778095,		\"channelId\": 1	},	\"value\": {		\"entryFee\": "
				+ entryfee
				+ ",		\"parent\": \"false\",		\"isBackend\": "
				+ isBackEnd
				+ ",		\"installment\": "
				+ isInstallment
				+ ",		\"tcUrl\": \"http://url/\",		\"timeOfRelease\": \""
				+ timeOfRelease
				+ "\",		\"regCondId\": 0,		\"ptype\": 2,		\"tournamentType\": 2,		\"cardsTobeDelt\": 13,		\"gamePlay\": 2,		\"prizeStuctureID\": "
				+ prizeStructureId
				+ ",		\"jokers\": 2,		\"tournamentName\": \""
				+ trnName
				+ "\",		\"tableSize\": 6,		\"cardPerSequence\": 3,		\"tournamentDesc\": \"this is Rest Trn\",		\"rewardPoints\": 0,		\"showOfflineDetails\": false,		\"offlineDetails\": \"\",		\"parenttournid\": "
				+ parentId
				+ ",		\"parenttournname\": \"N/A\",		\"autoEmailLosers\": false,		\"serviceFeeType\": \"Percentage\",		\"entryType\": 1,		\"brandName\": \"N/A\",		\"maxPlayers\": 6,		\"languages\": [{			\"language\": \"English\",			\"id\": 1,			\"templateID\": 1500062232		}],		\"minPlayers\": 2,		\"deckToUse\": 2,		\"timeBetweenDeals\": 10,		\"offlineTournament\": false,		\"serviceFeePercentage\": "
				+ serviceFee
				+ ",		\"parentPrizePercentage\": "
				+ parentPercent
				+ ",		\"displayTabID\": 31,		\"regEndTime\": \""
				+ regEndTime
				+ "\",		\"prizeDescription\": \"p1\",		\"regConditionDescription\": \"R1\",		\"regStartTime\": \""
				+ regStartTime
				+ "\",		\"breakID\": 1,		\"breakName\": \"System Default\",		\"tournamentStartTime\": \""
				+ tournamentStartTime
				+ "\",		\"moveTime\": 10000,		\"tournamentNameType\": \"Manual\",		\"customVisibility\": \"false\",		\"channelVisibility\": [			1,			2,			3,			4		],		\"testTournamentFlag\": \"false\",		\"timeBetweenRound\": 10,		\"autoEmail\": true,		\"ticketId\": 0,		\"showOnTop\": false	}}";

		String path = "http://try-gpc.rummycircle.com/gpc/api/tournaments";
		System.out.println(path);
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json;charset=UTF-8");

		HTTPRequest request = new HTTPRequest(headers, requestBody);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc.sendRequest(HTTPMethod.POST, path, request);
		System.out
				.println("response--------------------------------------->>>response"
						+ response.getBody().getBodyText());
		Assert.assertTrue(response.getStatusCode() == 200,
				"status Code expected is 200. However Status Code received:"
						+ response.getStatusCode());

		Map<TournamentDetails, String> tournamentData = new HashMap<TournamentDetails, String>();

		tournamentData.put(TournamentDetails.TOURNAMENTID, (JsonPath.read(
				response.getBody().getBodyText(), "$.value.trnmntId"))
				.toString());
		tournamentData.put(TournamentDetails.TOURNAMENTNAME, (JsonPath.read(
				response.getBody().getBodyText(), "$.value.tournamentName"))
				.toString());
		tournamentData.put(TournamentDetails.ENTRYFEE, (JsonPath.read(response
				.getBody().getBodyText(), "$.value.entryFee")).toString());
		tournamentData.put(TournamentDetails.TOURNAMENTTYPE,
				(getTrnType(JsonPath.read(response.getBody().getBodyText(),
						"$.value.tournamentType"))).toString());
		tournamentData.put(TournamentDetails.PRIZETYPE, (getPrizeType(JsonPath
				.read(response.getBody().getBodyText(), "$.value.ptype")))
				.toString());
		tournamentData.put(TournamentDetails.TOURNAMENTSTATUS,
				(getTrnStatus(JsonPath.read(response.getBody().getBodyText(),
						"$.value.status"))).toString());
		tournamentData.put(TournamentDetails.TOURNAMENTSTARTTIME, (JsonPath
				.read(response.getBody().getBodyText(),
						"$.value.tournamentStartTime")).toString());

		return tournamentData;
	}

	public Map<String, String> createBackEndPrizStructure(String name,
			boolean minimumGuaranteed, int guaranteedAmount,
			int tournamentSize, boolean isBackend, boolean collapsible,
			int collapseRound, String ticketId) {
		// Trntype values 0-standalone,1-parent,2-qualifier
		// prizePoolType values 0-normal,1-guaranteed, 2-xup
		String requestJson = String
				.format("{\"value\": {\"prizeStructure\": {\"id\": 0,\"structureName\": \""
						+ name
						+ "\",\"guaranteed\": "
						+ minimumGuaranteed
						+ ",\"guaranteedAmount\": "
						+ guaranteedAmount
						+ ",\"xup\": false,\"xupMultiplier\": 0,\"tableSize\": 6,\"tournSize\": "
						+ tournamentSize
						+ ",\"cash\": false,\"ticket\": true,\"percentOfPrizeRound\": 100,\"isBackend\": "
						+ isBackend
						+ ",\"collapsible\": "
						+ collapsible
						+ ",\"collapseRound\": "
						+ collapseRound
						+ ",\"minPlayers\": [2,7,19,55,163,487,1459,4375,13123,39367],\"maxPlayers\": [6,18,54,162,486,1458,4374,13122,39366,118098],\"dealMultipliers\": [1,1,1,1,1,1,1,1,1,1],\"qualifications\": [2,2,1,1,1,1,1,1,1,1],\"ranges\": [1,2,3,4,5,6,7,8,9,10],\"prizePoolType\": 0,\"totalPrizePool\": 1000,\"tournamentType\": 2,\"fromPage\": 0},\"roundCreationDetails\": {\"1\": [\"6,1\"]},\"roundPrizeDetails\": [{\"roundNo\": 1,\"tranche\": 1,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": %s,\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 2,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": %s,\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 3,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": %s,\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 4,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": %s,\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 5,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": %s,\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 6,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": %s,\"ticketValue\": 0}]},\"context\": {\"source\": \"WebadminPortal\",\"time\": 12345633,\"channelId\": 1,\"loginId\": \"akansha.gaonkar\"}\r\n}",
						ticketId, ticketId, ticketId, ticketId, ticketId,
						ticketId);
		System.out.println("----------------\n" + requestJson);
		String path = "http://try-gpc.rummycircle.com/gpc/api/prizestructures";
		System.out.println(path);
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json;charset=UTF-8");

		HTTPRequest request = new HTTPRequest(headers, requestJson);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc.sendRequest(HTTPMethod.POST, path, request);
		System.out.println("---------->" + response.getBody().getBodyText());
		HashMap<String, String> prizeStrustureData = new HashMap<String, String>();
		prizeStrustureData
				.put("id",
						JsonPath.read(response.getBody().getBodyText(),
								"$.value[0].id").toString());
		prizeStrustureData.put(
				"name",
				JsonPath.read(response.getBody().getBodyText(),
						"$.value[0].name").toString());
		return prizeStrustureData;
	}

	public Map<String, String> createPrizStructure(String name,
			boolean minimumGuaranteed, int guaranteedAmount,
			int tournamentSize, int trntype, boolean isBackend,
			boolean collapsible, int collapseRound, int prizePoolType) {
		// Trntype values 0-standalone,1-parent,2-qualifier
		// prizePoolType values 0-normal,1-guaranteed, 2-xup
		String requestJson = "{\"value\": {\"prizeStructure\": {\"structureName\":\""
				+ name
				+ "\",\"guaranteed\": "
				+ minimumGuaranteed
				+ ",\"guaranteedAmount\": "
				+ guaranteedAmount
				+ ",\"xup\": false,\"xupMultiplier\": 0,\"tableSize\": 6,\"tournSize\": "
				+ tournamentSize
				+ ",\"cash\":true,\"ticket\":false,\"percentOfPrizeRound\":100,\"totalPrizePool\": "
				+ guaranteedAmount
				+ ",\"fromPage\":1,\"tournamentType\":"
				+ trntype
				+ ",\"isBackend\":"
				+ isBackend
				+ ",\"collapsible\":"
				+ collapsible
				+ ",\"collapseRound\": "
				+ collapseRound
				+ ",\"minPlayers\":[2, 7, 19, 55, 163, 487, 1459, 4375, 13123, 39367], \"maxPlayers\":[6, 18, 54, 162, 486, 1458, 4374, 13122, 39366, 118098], \"dealMultipliers\":[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],  \"qualifications\": [1,1,1,1,1,1,1,1,1,1], \"ranges\":[1,2,3,4,5,6,7,8,9,10],\"prizePoolType\": "
				+ prizePoolType
				+ ",\"tournamentType\": "
				+ trntype
				+ "},\"roundPrizeDetails\": [{\"roundNo\": 2,\"tranche\": 1,\"noOfPrizes\": 1,\"cashAmount\": "
				+ guaranteedAmount
				+ ",\"cashPercent\": 100,\"ticketId\": 0,\"ticketValue\": 0},{\"roundNo\": 2,\"tranche\": 2,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": 0,\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 3,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": 0,\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 4,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": 0,\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 5,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": 0,\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 6,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": 0,\"ticketValue\": 0}]}, \"context\":{\"source\":\"WebadminPortal\",\"time\":12345633,\"channelId\":1,\"loginId\":\"sanket\" }}";
		String path = "http://try-gpc.rummycircle.com/gpc/api/prizestructures";
		System.out.println(path);
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json;charset=UTF-8");

		HTTPRequest request = new HTTPRequest(headers, requestJson);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc.sendRequest(HTTPMethod.POST, path, request);
		System.out.println("---------->" + response.getBody().getBodyText());
		HashMap<String, String> prizeStrustureData = new HashMap<String, String>();
		prizeStrustureData
				.put("id",
						JsonPath.read(response.getBody().getBodyText(),
								"$.value[0].id").toString());
		prizeStrustureData.put(
				"name",
				JsonPath.read(response.getBody().getBodyText(),
						"$.value[0].name").toString());
		return prizeStrustureData;
	}

	public String getPrizeType(int prize) {
		String prizeType = "Promotional";
		if (prize == 1) {
			prizeType = "Practice";
		} else if (prize == 2) {
			prizeType = "Cash";
		}
		return prizeType;
	}

	public String getParentTrnFlag(String tournamentType) {
		String parentTrnFlag = "false";
		if (Integer.parseInt(tournamentType) == 1) {
			parentTrnFlag = "true";
		}
		return parentTrnFlag;
	}

	public String getTrnType(int trnType) {
		String tournamentType = "Standalone";
		if (trnType == 1) {
			tournamentType = "Parent";
		} else if (trnType == 2) {
			tournamentType = "Qualifier";
		}
		return tournamentType;
	}

	public String getTrnStatus(int trnStatus) {
		String tournamentStatus = "To Be Released";
		if (trnStatus == 2) {
			tournamentStatus = "Coming Up";
		} else if (trnStatus == 3) {
			tournamentStatus = "Open";
		} else if (trnStatus == 4) {
			tournamentStatus = "Registration Closed";
		} else if (trnStatus == 5) {
			tournamentStatus = "In Progress";
		} else if (trnStatus == 6) {
			tournamentStatus = "Completed";
		} else if (trnStatus == 7) {
			tournamentStatus = "Crash";
		} else if (trnStatus == 8) {
			tournamentStatus = "Crash Refund";
		} else if (trnStatus == 9) {
			tournamentStatus = "Abandoned";
		} else if (trnStatus == 10) {
			tournamentStatus = "PAbandoned";
		} else if (trnStatus == 11) {
			tournamentStatus = "Expired";
		} else if (trnStatus == 12) {
			tournamentStatus = "Cancelled";
		}
		return tournamentStatus;
	}

	public void deleteTournament(String trnId) {
		String requestJson = String
				.format("{\"context\":{\"loginId\":\"testUser1\",\"source\":\"WebadminPortal\",\"time\":12345633,\"channelId\":1},\"value\":{\"trnmntId\":%s}}",
						trnId);
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json;charset=UTF-8");
		String path = "http://try-gpc.rummycircle.com/gpc/api/tournaments";
		HTTPRequest request = new HTTPRequest(headers, requestJson);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc
				.sendRequest(HTTPMethod.DELETE, path, request);
	}

	public void deletePrizeStructure(String prizeStructureId) {
		String requestJson = "{\"context\":{\"loginId\":\"ritesh\",\"source\":\"WebadminPortal\",\"time\":12345633}\n}";
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json;charset=UTF-8");
		String path = String.format(
				"http://try-gpc.rummycircle.com/gpc/api/prizestructures/%s",
				prizeStructureId);
		HTTPRequest request = new HTTPRequest(headers, requestJson);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc
				.sendRequest(HTTPMethod.DELETE, path, request);
		System.out.println("Dlete ps: " + response.getBody().getBodyText());
	}
	
	public Map<String, String> createBackendTicket(int days) {
		DateTimeFormatter dtf = DateTimeFormatter
				.ofPattern("yyyy-MM-dd HH:mm:ss");
		String expiryDate = 
		dtf.format(LocalDateTime.now().plusDays(
				 days));
		String ticketName = getRandomString(2).concat("_automation_ticket");

		String requestBody = "{\"name\":\""+ticketName+"\",\"ticketValueType\":1,\"ticketValue\":0,\"ticketType\":1,\"expiryType\":1,\"expiryDate\":\""+expiryDate+"\",\"validity\":0,\"displayToUsers\":true,\"createdBy\":\"niic_01\",\"updatedBy\":null}";
		Properties customProp = PropertyReader
				.loadCustomProperties("custom.properties");
		String path = customProp.getProperty(
				"createTicket.nfs.protocol").concat("://")
				+ customProp.getProperty("createTicket.nfs.host")
				+ ":"
				+ customProp.getProperty("createTicket.nfs.port")
				+ "/nfs/api/ticket";
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json;charset=UTF-8");

		HTTPRequest request = new HTTPRequest(headers, requestBody);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc.sendRequest(HTTPMethod.POST, path, request);
		HashMap<String, String> ticketData = new HashMap<String, String>();
		ticketData
				.put("id",
						JsonPath.read(response.getBody().getBodyText(),
								"$.id").toString());
		ticketData.put(
				"name",
				JsonPath.read(response.getBody().getBodyText(),
						"$.name").toString());
		return ticketData;
	}
	
	public String createBackendTicket() {
		return new TournamentUtility().createBackendTicket(5).get("id");
		}
	
	public Map<String, String> createPrizStructure(String name,
			boolean minimumGuaranteed, int guaranteedAmount,
			int tournamentSize, int trntype, boolean isBackend,
			boolean collapsible, int collapseRound, int prizePoolType,
			String ticketid,int cashPercent,boolean cash,boolean ticket) {
		// Trntype values 0-standalone,1-parent,2-qualifier
		// prizePoolType values 0-normal,1-guaranteed, 2-xup

		String requestJson = "{\"value\": {\"prizeStructure\": {\"structureName\":\""
				+ name
				+ "\",\"guaranteed\": "
				+ minimumGuaranteed
				+ ",\"guaranteedAmount\": "
				+ guaranteedAmount
				+ ",\"xup\": false,\"xupMultiplier\": 0,\"tableSize\": 6,\"tournSize\": "
				+ tournamentSize
				+ ",\"cash\":"+cash+",\"ticket\":"+ticket+",\"percentOfPrizeRound\":100,\"totalPrizePool\": "
				+ guaranteedAmount
				+ ",\"fromPage\":0,\"tournamentType\":"
				+ trntype
				+ ",\"isBackend\":"
				+ isBackend
				+ ",\"collapsible\":"
				+ collapsible
				+ ",\"collapseRound\": "
				+ collapseRound
				+ ",\"minPlayers\":[2, 7, 19, 55, 163, 487, 1459, 4375, 13123, 39367], \"maxPlayers\":[6, 18, 54, 162, 486, 1458, 4374, 13122, 39366, 118098], \"dealMultipliers\":[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],  \"qualifications\": [1,1,1,1,1,1,1,1,1,1], \"ranges\":[1,2,3,4,5,6,7,8,9,10],\"prizePoolType\": "
				+ prizePoolType
				+ "},\"roundPrizeDetails\": [{\"roundNo\": 1,\"tranche\": 1,\"noOfPrizes\": 1,\"cashAmount\": "+cashPercent+",\"cashPercent\": "+cashPercent+",\"ticketId\": "+ticketid+",\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 2,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": "+ticketid+",\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 3,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": "+ticketid+",\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 4,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": "+ticketid+",\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 5,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": "+ticketid+",\"ticketValue\": 0},{\"roundNo\": 1,\"tranche\": 6,\"noOfPrizes\": 1,\"cashAmount\": 0,\"cashPercent\": 0,\"ticketId\": "+ticketid+",\"ticketValue\": 0}]}, \"context\":{\"source\":\"WebadminPortal\",\"time\":12345633,\"channelId\":1,\"loginId\":\"sanket\" }}";

		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json;charset=UTF-8");

		HTTPRequest request = new HTTPRequest(headers, requestJson);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc.sendRequest(HTTPMethod.POST, customProp.getProperty("drt.create.prize.structure"), request);
		HashMap<String, String> prizeStrustureData = new HashMap<String, String>();
		prizeStrustureData
				.put("id",
						JsonPath.read(response.getBody().getBodyText(),
								"$.value[0].id").toString());
		prizeStrustureData.put(
				"name",
				JsonPath.read(response.getBody().getBodyText(),
						"$.value[0].name").toString());
		return prizeStrustureData;
	}
	
	public String createParentPrizeStructure(int winningAmount) {
		String name="parent_".concat(getRandomString(6));
		boolean isBackend=false;
		boolean minimumGuaranteed=true;
		int guaranteedAmount=winningAmount;
		int tournamentSize=6;
		boolean collapsible=false;
		int collapseRound=0;
		int prizePoolType=1;
		int trntype=2;
		String ticketid="0";
		int cashPercent=100;
		boolean cash=true;
		boolean ticket=false;
		return createPrizStructure(name, minimumGuaranteed, guaranteedAmount, tournamentSize, trntype, isBackend, collapsible, collapseRound, prizePoolType,ticketid,cashPercent,cash,ticket).get("id");
	}
	
	public Map<String, String> createRegCond(String ticketId) {
		
		
		String regCondName = getRandomString(2).concat("_automation_regCond");

		String requestBody = "{\"value\":{\"regName\":\""+regCondName+"\",\"clubType\":\"\",\"pWithTicket\":\""+ticketId+"\",\"specificPlayers\":0,\"disallowedTicket\":0,\"joinMessage\":\"Sorry, you are not qualified for this tournaments. Please join another tournament.\"},\"context\":{\"source\":\"AdminPortal\",\"time\":1540194062235,\"channelId\":1,\"loginId\":\"niic_01\"},\"context\":{\"source\":\"AdminPortal\",\"time\":1540194062235,\"channelId\":1,\"loginId\":\"niic_01\"}}";
		
		String path = customProp.getProperty("drt.reg.condition");

		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json;charset=UTF-8");

		HTTPRequest request = new HTTPRequest(headers, requestBody);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc.sendRequest(HTTPMethod.POST, path, request);
		HashMap<String, String> ticketData = new HashMap<String, String>();
		ticketData
				.put("id",
						JsonPath.read(response.getBody().getBodyText(),
								"$.value[0].regId").toString());
		ticketData.put(
				"name",
				JsonPath.read(response.getBody().getBodyText(),
						"$.value[0].regName").toString());
		return ticketData;
	}
	
	public String createRegCondition(String ticketid) {
		return new TournamentUtility().createRegCond(ticketid).get("id");
	}
	
	public Map<TournamentDetails, String> createParentTournament(String prizeStructureId,String regCondId) {
		int entryfee=0;
		String tournamentType="1";//parent tournament
		String pType="3";
		int tableSize=6;
		int maxPlayers=6;
		int rewardPoints=0;
		int regStartTimeDays=1;
		int regEndTimeDays=2;
		int trnmntStartTimeDays=3;
		String tcURL="";
		return CreateTournamentUsingPostAPI(entryfee, regCondId, tournamentType, prizeStructureId, tableSize, rewardPoints, maxPlayers, pType, regStartTimeDays, regEndTimeDays, trnmntStartTimeDays, tcURL);
		}
	
	public Map<TournamentDetails, String> CreateTournamentUsingPostAPI(
			int entryfee, String regCondId, String tournamentType,
			String prizeStructureId, int tableSize, int rewardPoints,
			int maxPlayers, String pType,
			int regStartTimeDays, int regEndTimeDays, int trnmntStartTimeDays,String tcURL) {

		DateTimeFormatter dtf = DateTimeFormatter
				.ofPattern("dd-MMM-yyyy HH:mm:ss");

		String parentTrnFlag = getParentTrnFlag(tournamentType);
		String tournamentName = getRandomString(2).concat("_automation_trn");

		String timeOfRelease = dtf.format(LocalDateTime.now());
		String regStartTime = dtf.format(LocalDateTime.now().plusDays(
				regStartTimeDays));
		String regEndTime = dtf.format(LocalDateTime.now().plusDays(
				regEndTimeDays));
		String tournamentStartTime = dtf.format(LocalDateTime.now().plusDays(
				trnmntStartTimeDays));
		
		String requestBody = "{\"value\":{\"testTournamentFlag\":\"false\",\"isBackend\":false,\"installment\":false,\"tcUrl\":\""
				+ tcURL + "\",\"ptype\":" + pType + ",\"tournamentType\":" + tournamentType
				+ ",\"gamePlay\":2,\"displayTabID\":\"32,33,34,36,37,38,39,310,311,312,313,314,315,31\",\"brandID\":0,\"brandName\":\"N/A\",\"tournamentNameType\":\"Manual\",\"tournamentName\":\""
				+ tournamentName + "\",\"tableSize\":" + tableSize + ",\"entryFee\":\"" + entryfee
				+ "\",\"serviceFeeType\":\"Percentage\",\"serviceFeePercentage\":0,\"minPlayers\":2,\"maxPlayers\":"
				+ maxPlayers + ",\"prizeStuctureID\":" + prizeStructureId
				+ ",\"channelVisibility\":[1,2,3,4],\"customVisibility\":\"false\",\"regCondId\":" + regCondId
				+ ",\"tournamentDesc\":\"\",\"timeBetweenDeals\":20,\"timeBetweenRound\":10,\"breakID\":1,\"breakName\":\"System Default\",\"cardsTobeDelt\":13,\"deckToUse\":2,\"cardPerSequence\":3,\"jokers\":2,\"moveTime\":25000,\"timeOfRelease\":\""+timeOfRelease+"\",\"regStartTime\":\""+regStartTime+"\",\"regEndTime\":\""+regEndTime+"\",\"tournamentStartTime\":\""+tournamentStartTime+"\",\"autoEmail\":false,\"autoEmailLosers\":false,\"prizeDescription\":\"\",\"regConditionDescription\":\"test automation\",\"parent\":\""
				+ parentTrnFlag
				+ "\",\"entryType\":0,\"offlineTournament\":false,\"offlineDetails\":\"\",\"showOfflineDetails\":false},\"context\":{\"source\":\"WebAdminPortal\",\"time\":1540197879719,\"channelId\":1,\"loginId\":\"niic_01\"}}";		
		
		
		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", customProp.getProperty("content.type"));

		HTTPRequest request = new HTTPRequest(headers, requestBody);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc.sendRequest(HTTPMethod.POST, customProp.getProperty("drt.create.tournament"), request);
	
		Assert.assertTrue(response.getStatusCode() == 200,
				"status Code expected is 200. However Status Code received:"
						+ response.getStatusCode());

		Map<TournamentDetails, String> tournamentData = new HashMap<TournamentDetails, String>();

		tournamentData.put(TournamentDetails.TOURNAMENTID, (JsonPath.read(
				response.getBody().getBodyText(), "$.value.trnmntId"))
				.toString());
		tournamentData.put(TournamentDetails.TOURNAMENTNAME, (JsonPath.read(
				response.getBody().getBodyText(), "$.value.tournamentName"))
				.toString());
		tournamentData.put(TournamentDetails.ENTRYFEE, (JsonPath.read(response
				.getBody().getBodyText(), "$.value.entryFee")).toString());
		tournamentData.put(TournamentDetails.TOURNAMENTTYPE,
				(getTrnType(JsonPath.read(response.getBody().getBodyText(),
						"$.value.tournamentType"))).toString());
		tournamentData.put(TournamentDetails.PRIZETYPE, (getPrizeType(JsonPath
				.read(response.getBody().getBodyText(), "$.value.ptype")))
				.toString());
		tournamentData.put(TournamentDetails.TOURNAMENTSTATUS,
				(getTrnStatus(JsonPath.read(response.getBody().getBodyText(),
						"$.value.status"))).toString());
		tournamentData.put(TournamentDetails.TOURNAMENTSTARTTIME, (JsonPath
				.read(response.getBody().getBodyText(),
						"$.value.tournamentStartTime")).toString());

		return tournamentData;
	}
	
	public String createBackEndPrizeStructure(String ticketid) {
		String name="backend_".concat(getRandomString(6));
		boolean isBackend=true;
		boolean minimumGuaranteed=false;
		int guaranteedAmount=0;
		int tournamentSize=6;
		boolean collapsible=true;
		int collapseRound=1;
		int prizePoolType=0;
		int trntype=2;
		int cashPercent=0;
		boolean cash=false;
		boolean ticket=true;
		return createPrizStructure(name, minimumGuaranteed, guaranteedAmount, tournamentSize, trntype, isBackend, collapsible, collapseRound, prizePoolType,ticketid,cashPercent,cash,ticket).get("id");
	
	}
	
	public Map<TournamentDetails, String> createBackEndTournament(String entryfee,
			String prizeStructureId, boolean isBackEnd, boolean isInstallment,
			String trnName, String parentId, int serviceFee, int parentPercent) {

		DateTimeFormatter dtf = DateTimeFormatter
				.ofPattern("dd-MMM-yyyy HH:mm:ss");

		String timeOfRelease = dtf.format(LocalDateTime.now().plusMinutes(1));
		String regStartTime = dtf.format(LocalDateTime.now().plusMinutes(1));
		String regEndTime = dtf.format(LocalDateTime.now().plusHours(5));
		String tournamentStartTime = dtf
				.format(LocalDateTime.now().plusDays(1));

		String requestBody = "{	\"context\": {		\"loginId\": \"ashutosh\",		\"source\": \"WebAdminPortal\",		\"time\": 1495092778095,		\"channelId\": 1	},	\"value\": {		\"entryFee\": "
				+ entryfee
				+ ",		\"parent\": \"false\",		\"isBackend\": "
				+ isBackEnd
				+ ",		\"installment\": "
				+ isInstallment
				+ ",		\"tcUrl\": \"http://url/\",		\"timeOfRelease\": \""
				+ timeOfRelease
				+ "\",		\"regCondId\": 0,		\"ptype\": 2,		\"tournamentType\": 2,		\"cardsTobeDelt\": 13,		\"gamePlay\": 2,		\"prizeStuctureID\": "
				+ prizeStructureId
				+ ",		\"jokers\": 2,		\"tournamentName\": \""
				+ trnName
				+ "\",		\"tableSize\": 6,		\"cardPerSequence\": 3,		\"tournamentDesc\": \"this is Rest Trn\",		\"rewardPoints\": 0,		\"showOfflineDetails\": false,		\"offlineDetails\": \"\",		\"parenttournid\": "
				+ parentId
				+ ",		\"parenttournname\": \"N/A\",		\"autoEmailLosers\": false,		\"serviceFeeType\": \"Percentage\",		\"entryType\": 1,		\"brandName\": \"N/A\",		\"maxPlayers\": 6,		\"languages\": [{			\"language\": \"English\",			\"id\": 1,			\"templateID\": 1500062232		}],		\"minPlayers\": 2,		\"deckToUse\": 2,		\"timeBetweenDeals\": 10,		\"offlineTournament\": false,		\"serviceFeePercentage\": "
				+ serviceFee
				+ ",		\"parentPrizePercentage\": "
				+ parentPercent
				+ ",		\"displayTabID\": 31,		\"regEndTime\": \""
				+ regEndTime
				+ "\",		\"prizeDescription\": \"p1\",		\"regConditionDescription\": \"R1\",		\"regStartTime\": \""
				+ regStartTime
				+ "\",		\"breakID\": 1,		\"breakName\": \"System Default\",		\"tournamentStartTime\": \""
				+ tournamentStartTime
				+ "\",		\"moveTime\": 10000,		\"tournamentNameType\": \"Manual\",		\"customVisibility\": \"false\",		\"channelVisibility\": [			1,			2,			3,			4		],		\"testTournamentFlag\": \"false\",		\"timeBetweenRound\": 10,		\"autoEmail\": true,		\"ticketId\": 0,		\"showOnTop\": false	}}";

		HTTPHeaders headers = new HTTPHeaders();
		headers.addHeader("Content-Type", "application/json;charset=UTF-8");

		HTTPRequest request = new HTTPRequest(headers, requestBody);
		RESTAssuredClient rc = RESTAssuredClient.getRESTAssuredClient();
		HTTPResponse response = rc.sendRequest(HTTPMethod.POST, customProp.getProperty("drt.create.backend.tournament"), request);
		
		Assert.assertTrue(response.getStatusCode() == 200,
				"status Code expected is 200. However Status Code received:"
						+ response.getStatusCode());

		Map<TournamentDetails, String> tournamentData = new HashMap<TournamentDetails, String>();

		tournamentData.put(TournamentDetails.TOURNAMENTID, (JsonPath.read(
				response.getBody().getBodyText(), "$.value.trnmntId"))
				.toString());
		tournamentData.put(TournamentDetails.TOURNAMENTNAME, (JsonPath.read(
				response.getBody().getBodyText(), "$.value.tournamentName"))
				.toString());
		tournamentData.put(TournamentDetails.ENTRYFEE, (JsonPath.read(response
				.getBody().getBodyText(), "$.value.entryFee")).toString());
		tournamentData.put(TournamentDetails.TOURNAMENTTYPE,
				(getTrnType(JsonPath.read(response.getBody().getBodyText(),
						"$.value.tournamentType"))).toString());
		tournamentData.put(TournamentDetails.PRIZETYPE, (getPrizeType(JsonPath
				.read(response.getBody().getBodyText(), "$.value.ptype")))
				.toString());
		tournamentData.put(TournamentDetails.TOURNAMENTSTATUS,
				(getTrnStatus(JsonPath.read(response.getBody().getBodyText(),
						"$.value.status"))).toString());
		tournamentData.put(TournamentDetails.TOURNAMENTSTARTTIME, (JsonPath
				.read(response.getBody().getBodyText(),
						"$.value.tournamentStartTime")).toString());
		tournamentData.put(TournamentDetails.TOURNAMENTENDTIME, (JsonPath
				.read(response.getBody().getBodyText(),
						"$.value.regEndTime")).toString());
		

		return tournamentData;
	}
	
	/**
	 * Create Back End Tournament using API
	 * 
	 * @param entryfee
	 */
	public Map<TournamentDetails, String> createBackEndTournament(String entryfee, String prizeStrId,
			String parentTournId) {
		return createBackEndTournament(entryfee, prizeStrId, true, true, getRandomString(7), parentTournId, 10,
				90);
	}
	
	public Map<TournamentDetails, String> createBackEndTournament(String entryfee, String prizeStrId,
			String parentTournId, boolean isInstallmentAllowed) {
		return createBackEndTournament(entryfee, prizeStrId, true, isInstallmentAllowed, getRandomString(7), parentTournId, 10,
				90);
	}
}