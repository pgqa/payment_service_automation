package com.rummycircle;

import java.util.ArrayList;
import java.util.Queue;

import org.apache.log4j.Logger;

import com.rummycircle.wsclient.WebSocketClient;

public class WebSocketUtils {
	private static Logger Log = Logger.getLogger(WebSocketUtils.class);

	public static final String URL = "wss://stage-tlist.rummycircle.com/";

	public String getMidMsg(String mid, String jSessionId) {
		String message = "{\"mid\": " + mid + ",\"jSessionId\": \""
				+ jSessionId + "\" }";
		Log.info("Message based on MID and JSESSIONID :: " + message);
		return message;
	}

	public String getReadyStateString(String jSessionId, int channelId,
			boolean isMobile) {
		String message = "{\"mid\": 1,\"jSessionId\": \"" + jSessionId
				+ "\", \"channelId\":" + channelId + ",\"isMobile\":"
				+ isMobile + " }";
		Log.info("Ready State message :: " + message);
		return message;
	}

	public String getTournamentData(String userId, String jSessionId) {
		String message = "{\"mid\": 14,\"tournamentId\": 10084002,\"userId\": \""
				+ userId + "\",\"jSessionId\": \"" + jSessionId + "\" }";
		Log.info("Tournament message :: " + message);
		return message;
	}

	public String getSaveSortMessage(String tabId, String type, String column,
			int order) {
		String message = "{\"mid\":10,\"params\": {\"" + tabId
				+ "\" : {\"type\" : \"" + type + "\",\"" + column + "\" : "
				+ order + "}}}";
		Log.info("Sort message :: " + message);
		return message;
	}

	public String getSaveFilterMessage(String tabId, String column, String value) {
		String message = "{\"mid\":12,\"params\":{\"" + tabId + "\":{\"1\":{\""
				+ column + "\":[\"" + value + "\"]}}}}";
		Log.info("Filter message :: " + message);
		return message;
	}

	public String getFavoriteMessage(String mId, int pt, int f, int s,
			String ef, String pv, String id) {
		String message = "{\"mid\":" + mId + ",\"template\":{\"pt\":" + pt
				+ ",\"f\":" + f + ",\"s\":" + s + ",\"ef\":\"" + ef
				+ "\",\"pv\":\"" + pv + "\",\"id\":\"" + id + "\"}}";
		Log.info("Favorite message :: " + message);
		return message;
	}

	public String getMTTMessage(String mId, String trnId) {
		String message = "{\"mid\":" + mId + ",\"tournamentId\":" + trnId + "}";
		Log.info("Tournament message for tournament Id: " + trnId + " is :: "
				+ message);
		return message;
	}

	public String getMTTRoundMessage(String mId, String trnId, String rno) {
		String message = "{\"mid\":" + mId + ",\"tournamentId\":" + trnId
				+ ",\"round\":" + rno + "}";
		Log.info("Round message for specific tournament Id: " + trnId
				+ " is :: " + message);
		return message;
	}

	public String getMTTTableMessage(String mId, String trnId, String rno,
			String tableId) {
		String message = "{\"mid\":" + mId + ",\"tournamentId\":" + trnId
				+ ",\"round\":" + rno + ",\"tableId\":" + tableId + "}";
		Log.info("Table message for specific tournament Id: " + trnId
				+ " is :: " + message);
		return message;
	}

	public String getMTTQualifierMessage(String mId, String parentId,
			String userId) {
		String message = "{\"mid\":" + mId + ",\"parentId\":" + parentId
				+ ",\"userId\":\"" + userId + "\"}";
		Log.info("MTT Parent Qualifier message" + message);
		return message;
	}

	public String getMTTQualifierMessage(String mId, String parentId) {
		String message = "{\"mid\":" + mId + ",\"parentId\":" + parentId + "}";
		Log.info("MTT Parent Qualifier message" + message);
		return message;
	}

	public String purchaseTicket(String mId, String geoLoc, Integer ticketId,
			boolean isInstall) {
		String message = "{\"mid\":" + mId + ",\"geoLocState\":\"" + geoLoc
				+ "\",\"ticketId\":" + ticketId + ",\"isInstallment\":"
				+ isInstall + "}";
		Log.info("Purchasing ticket" + ticketId + " with message: \n" + message);
		return message;
	}

	public String reserveTicket(String mId, String geoLoc, Integer ticketId) {
		String message = "{\"mid\":" + mId + ",\"geoLocState\":\"" + geoLoc
				+ "\",\"ticketId\":" + ticketId + "}";
		Log.info("Reserving ticket" + ticketId + " with message: \n" + message);
		return message;
	}
	
	public String unreserveTicket(String mId,Integer ticketId){
		String message="{\"mid\":"+mId+",\"ticketId\":"+ticketId+"}";
		Log.info("Unreserving ticket" + ticketId + " with message: \n" + message);
		return message;
	}

	public String getMTTMessage(String mId, String trnId, int startIndex) {
		String message = "{\"mid\":" + mId + ",\"tournamentId\":" + trnId
				+ ",\"startIndex\":" + startIndex + "}";
		Log.info("Tournament message :: " + message);
		return message;
	}

	/*
	 * public String getMTTMessage(String mId, String trnId, String userId,
	 * String jSessionId, int action, boolean rpJoined, boolean isMob, boolean
	 * isCashPlayer, String loginId) { String message = "{\"mid\":" + mId +
	 * ",\"tournamentId\":" + trnId + ",\"userId\": " + userId +
	 * ",\"jSessionId\": \"" + jSessionId + "\",\"action\": " + action +
	 * ",\"rpJoined\": " + rpJoined + ",\"isMob\": " + isMob +
	 * ",\"isCashPlayer\": " + isCashPlayer + ",\"loginId\": \"" + loginId +
	 * "\"}"; Log.info("MTT message based on tournament details :: " + message);
	 * return message; }
	 */

	public String getMTTMessage(String mId, String trnId, String jSessionId,
			int action, int newEntryType, boolean isMobile,
			boolean isCashPlayer, String loginId) {
		String joinBy = "0";
		if (newEntryType == 0)
			joinBy = "0";
		else if (newEntryType == 1)
			joinBy = "1";
		else if (newEntryType != 6)
			joinBy = "2";
		String message = "{\"mid\":" + mId + ",\"tournamentId\":" + trnId
				+ ",\"jSessionId\": \"" + jSessionId + "\",\"action\": "
				+ action + ",\"joinBy\": " + joinBy + ",\"isMob\": " + isMobile
				+ ",\"isCashPlayer\": " + isCashPlayer + ",\"loginId\": \""
				+ loginId + "\"}";
		Log.info("MTT message based on tournament details :: " + message);
		return message;
	}

	public String getMessageBasedOnMid(String mId) {
		String message = "{\"mid\":" + mId + "}";
		Log.info("Message based on specified mid :: " + message);
		return message;
	}

	public String getGamePlayedCount(String templateId, int pt, int f, int s,
			String ef, String pv, String id, int count) {
		String message = "{\"mid\":29,\"templateId\":\"" + templateId
				+ "\",\"template\":{\"pt\":" + pt + ",\"f\":" + f + ",\"s\":"
				+ s + ",\"ef\":\"" + ef + "\",\"pv\":\"" + pv + "\",\"id\":\""
				+ id + "\"},\"count\":" + count + "}";
		Log.info("Game Played Count message :: " + message);
		return message;
	}

	public ArrayList<String> getMessage(WebSocketClient client, int mid) {
		String expMsg = "\"mid\":" + mid;
		String msg = null;
		ArrayList<String> msgList = new ArrayList<String>();
		Queue<String> msgQueue = client.getMessages();
		while (msgQueue.size() != 0) {
			msg = msgQueue.poll();
			if (msg.contains(expMsg)) {
				msgList.add(msg);
			}
		}
		return msgList;
	}
}