package com.rummycircle;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

/*
 * ankit
 */
public class JSONComparator {
	public static boolean comparator(String json1, String json2) throws JSONException {
		JSONObject inputjson = new JSONObject(json1);
		JSONObject outputjson = new JSONObject(json2);
		Map<String, Object> map1 = new HashMap<>();
		Map<String, Object> map2 = new HashMap<>();
		parse(inputjson, map1);
		parse(outputjson, map2);
		boolean isEqual = true;
		Set<Entry<String, Object>> set1 = map1.entrySet();
		Iterator<Entry<String, Object>> it = set1.iterator();
		while (it.hasNext()) {
			Entry<String, Object> entry = it.next();
			if (map2.containsKey(entry.getKey())) {
				if (!map2.get(entry.getKey()).equals(map1.get(entry.getKey())))
					isEqual = false;
			}
		}
		return isEqual;
	}

	public static Map<String, Object> parse(JSONObject json, Map<String, Object> out) throws JSONException {
		Iterator<String> keys = json.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			Object val = null;
			try {
				JSONObject value = json.getJSONObject(key);
				parse(value, out);
			} catch (Exception e) {
				val = json.get(key);
			}

			if (val != null) {
				out.put(key, val + "");
			}
		}
		return out;
	}

}