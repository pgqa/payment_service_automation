echo "Configuring mongoDB..."

enabled=$1
dbname=$2
host=$3
port=$4

sed -i.bak "$ a\mongo.db.enabled=${enabled}" src/test/resources/config.properties
sed -i.bak "$ a\mongo.db.dbname=${dbname}" src/test/resources/config.properties
sed -i.bak "$ a\mongo.db.host=${host}" src/test/resources/config.properties
sed -i.bak "$ a\mongo.db.port=${port}" src/test/resources/config.properties

echo "Mongo database set to : ${enabled}"
echo "dbname set to : ${dbname}"
echo "host is set to: ${host}"
echo "port is set to : ${port}"