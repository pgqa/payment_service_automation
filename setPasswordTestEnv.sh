login_protocol=$1
login_host=$2
login_port=$3
createcaptcha_protocol=$4
createcaptcha_host=$5
createcaptcha_port=$6
fusionProxySetup_protocol=$7
fusionProxySetup_host=$8
redis_ip=$9

echo "login protocol set to : ${login_protocol}"
echo "login host is set to: ${login_host}"
echo "login port is set to : ${login_port}"
echo "create captcha protocol is set to : ${createcaptcha_protocol}"
echo "create captcha port is set to : ${createcaptcha_port}"
echo "create captcha host is set to :${createcaptcha_host}"
echo "Fusion proxy setup protocol is set to :"${fusionProxySetup_protocol}
echo "Fusion proxy setup host is set to :"${fusionProxySetup_host}
echo "redis ip is set to :"${redis_ip}




sed -i -e "s/login.protocol=/login.protocol=${login_protocol}/g" src/test/resources/config.properties
sed -i -e "s/login.host=/login.host=${login_host}/g" src/test/resources/config.properties
sed -i -e "s/login.port=/login.port=${login_port}/g" src/test/resources/config.properties
sed -i -e "s/createCaptcha.protocol=/createCaptcha.protocol=${createcaptcha_protocol}/g" src/test/resources/config.properties
sed -i -e "s/createCaptcha.host=/createCaptcha.host=${createcaptcha_host}/g" src/test/resources/config.properties
sed -i -e "s/createCaptcha.port=/createCaptcha.port=${createcaptcha_port}/g" src/test/resources/config.properties
sed -i -e "s/fusionProxySetup.protocol=/fusionProxySetup.protocol=${fusionProxySetup_protocol}/g" src/test/resources/config.properties
sed -i -e "s/fusionProxySetup.host=/fusionProxySetup.host=${fusionProxySetup_host}/g" src/test/resources/config.properties
sed -i -e "s/redis.ip=/redis.ip=${redis_ip}/g" src/test/resources/config.properties




